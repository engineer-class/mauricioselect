/*
Script de configuração do banco de dados do projeto `MauricioSelect`
*/

DROP DATABASE IF EXISTS mauricioselect;
CREATE DATABASE mauricioselect;
USE mauricioselect;

/*
    Tabela 1 - categrias
*/
CREATE TABLE categoria (
    cat_id INT NOT NULL AUTO_INCREMENT,
    cat_nome VARCHAR(20),
    cat_valor_por_hora DOUBLE,
    UNIQUE(cat_nome),
    CONSTRAINT pk_cat_id PRIMARY KEY (cat_id)
);

/*
    Tabela 2 - fabricantes
*/
CREATE TABLE fabricante (
    fab_id INT NOT NULL AUTO_INCREMENT,
    fab_nome VARCHAR(25),
    UNIQUE(fab_nome),
    CONSTRAINT pk_fab_id PRIMARY KEY (fab_id)
);

/*
    Tabela 3 - usuários
*/
CREATE TABLE usuario (
    usr_id INT NOT NULL AUTO_INCREMENT,
    usr_cpf VARCHAR(14), 
    usr_email VARCHAR (50),
    usr_apelido VARCHAR(50), 
    usr_rg VARCHAR(12),
    usr_nome VARCHAR(150),
    usr_senha VARCHAR (50),
    usr_sexo ENUM('M','F'),
    CONSTRAINT pk_usr_id PRIMARY KEY (usr_id)
);

/*
    Tabela 4 - operadores 
*/
CREATE TABLE operador (
    ope_usr_id INT NOT NULL,
    ope_cargo VARCHAR(20) NOT NULL,
    CONSTRAINT fk_ope_usr_id FOREIGN KEY (ope_usr_id) REFERENCES usuario(usr_id)
);

/*
    Tabela 5 - Carros cadastrados
*/
CREATE TABLE carro (
    car_id INT NOT NULL AUTO_INCREMENT,
    car_cat_id INT,
    car_fab_id INT,
    car_ano INT,
    car_conservacao ENUM('BOM', 'MEDIO', 'BAIXO'),
    car_cor ENUM('PRETO', 'BRANCO', 'CINZA'),
    car_placa VARCHAR(30),
    car_quilometragem DOUBLE,
    car_modelo VARCHAR(30),
    car_tanque INT,
    car_quebrado TINYINT DEFAULT 0,
    UNIQUE(car_placa),
    CONSTRAINT pk_car_id PRIMARY KEY (car_id),
    CONSTRAINT fk_car_cat_id FOREIGN KEY (car_cat_id) REFERENCES categoria(cat_id),
    CONSTRAINT fk_car_fab_id FOREIGN KEY (car_fab_id) REFERENCES fabricante(fab_id)
);

/* 
    Tabela 6 - Reservas 
*/
CREATE TABLE reserva (
    res_id INT NOT NULL AUTO_INCREMENT,
    res_usr_id INT, 
    res_cat_id INT,
    res_car_id INT,
    res_dia_final DATE,
    res_dia_inicio DATE,
    res_status ENUM('PENDENTE','APROVADA', 'REPROVADA'),
    res_valor_total DECIMAL(19, 4),
    CONSTRAINT pk_res_id PRIMARY KEY (res_id),
    CONSTRAINT fk_res_usr_id FOREIGN KEY (res_usr_id) REFERENCES usuario(usr_id),
    CONSTRAINT fk_res_cat_id FOREIGN KEY (res_cat_id) REFERENCES categoria(cat_id),
    CONSTRAINT fk_res_car_id FOREIGN KEY (res_car_id) REFERENCES carro (car_id)
);

/*
    Tabela 7 - alugueis
*/
CREATE TABLE aluguel (
    alu_id INT NOT NULL AUTO_INCREMENT,
    alu_res_id INT,
    alu_usr_id INT,
    alu_ope_id INT,
    alu_car_id INT,
    alu_status ENUM('ALUGADO', 'FINALIZADO'),
    alu_data_devolucao DATE,
    alu_data_retirada DATE,
    alu_valor_total DECIMAL(19, 4),
    alu_valor_pago_antecipado DECIMAL(19, 4),
    CONSTRAINT pk_alu_id PRIMARY KEY (alu_id),
    CONSTRAINT fk_alu_res_id FOREIGN KEY (alu_res_id) REFERENCES reserva(res_id),
    CONSTRAINT fk_alu_usr_id FOREIGN KEY (alu_usr_id) REFERENCES usuario(usr_id),
    CONSTRAINT fk_alu_ope_id FOREIGN KEY (alu_ope_id) REFERENCES operador(ope_usr_id)
);

-- Registrando o primeiro Operador
INSERT INTO usuario VALUES (999, 999, 'root@mauricioselect.com', 'root', 999, 'root', 'root', 'M');
INSERT INTO operador VALUES (999, 'root');

-- Registrando as primeiras categorias
INSERT INTO categoria (cat_nome, cat_valor_por_hora) VALUES ('Econômico', 20.00);
INSERT INTO categoria (cat_nome, cat_valor_por_hora) VALUES ('Familiar', 23.5);
INSERT INTO categoria (cat_nome, cat_valor_por_hora) VALUES ('Empresarial', 25);

-- Registrando os primeiros fabricantes 
INSERT INTO fabricante (fab_nome) VALUES ('BMW');
INSERT INTO fabricante (fab_nome) VALUES ('Ford');
INSERT INTO fabricante (fab_nome) VALUES ('Ferrari');

-- Registrando os primeiros carros (Um para cada categória)
INSERT INTO carro (car_cat_id, car_fab_id, car_ano, car_conservacao, car_cor, car_placa, car_quilometragem, car_modelo, car_tanque) 
VALUES (1, 1, 2002, 'BOM', 'BRANCO', 'ABCD432', 1500, 'BMW Z4', 100);

INSERT INTO carro (car_cat_id, car_fab_id, car_ano, car_conservacao, car_cor, car_placa, car_quilometragem, car_modelo, car_tanque) 
VALUES (2, 2, 2019, 'MEDIO', 'CINZA', 'DCBA432', 1500, 'Ford Mustang', 100);

INSERT INTO carro (car_cat_id, car_fab_id, car_ano, car_conservacao, car_cor, car_placa, car_quilometragem, car_modelo, car_tanque) 
VALUES (3, 3, 2013, 'BOM', 'PRETO', 'ABCD123', 500, 'Ferrari 458 Italia', 100);
