[![pipeline status](https://gitlab.com/engineer-class/mauricioselect/badges/master/pipeline.svg)](https://gitlab.com/engineer-class/mauricioselect/commits/master)

## Mauricio Select :red_car:

Este projeto tem como objetivo o aluguel e o gerenciamento de carros. Possui visualização de acordo com as permissões do usuário. Existem dois tipos de visualização, do usuário que utiliza o sistema para solicitar carros para aluguel e funcionários chamados de "operadores" que gerenciam o sistema, cadastrando dados e autorizando alugueis.

A versão final do projeto está disponível [aqui](https://mauricio-select.herokuapp.com)

## Objetivos do projeto

O objetivo deste projeto foi a utilização das tecnologias:

1. [Java](https://docs.oracle.com/javase/8/docs/) (Versão 8 ou superior) no back end;
2. [Servlets](https://javaee.github.io/javaee-spec/javadocs/javax/servlet/http/package-summary.html) anotados ou com descritor de implantação;
3. [Java Server Pages](https://javaee.github.io/javaee-spec/javadocs/) (JSP) para criar páginas dinâmicas;
4. [JavaScript](https://www.ecma-international.org/ecma-262/7.0/) (JS) no front end;
5. JS Closures;
6. JS DOM Handlers;
7. JS Event Listeners;
8. JS para criar elementos dinamicamente no DOM.
9. [JQuery](https://jquery.com/);
10. [HTML 5](https://www.w3.org/TR/html52/);
11. [CSS 3](https://www.w3.org/Style/CSS/specs.en.html);
12. CSS 3 Media Queries;
13. Bases de dados [MySQL](https://www.mysql.com/) ou [MariaDB](https://mariadb.org/);
14. [Apache TomCat](http://tomcat.apache.org/).

## Detalhes da aplicação

Tela inicial do projeto com apresentação do tema proposto.

![Tela inicial do projeto](/imagens/1_index.png)

Descrição do projeto.

![Sobre o projeto](/imagens/2_sobre.png)

Visualização inicial dos usuários que utilizam o sistema para alugar carros.

![Tela inicial do usuario](/imagens/3_tela_inicial_usuario.png)

Visualização do perfil do usuário (A mesma tela é exibida para operadores e usuários por isto será omitida na visualização dos operadores)

![Perfil do usuario](/imagens/4_perfil_usuario.png)

Reservas criadas pelo usuário podem ser editadas nesta tela.

![Reservas do usuario](/imagens/5_reservas_usuario.png)

Reservas podem ser criadas nesta tela.

![Criação de reservas](/imagens/6_nova_reserva_usuario.png)

Tela inicial do operador, usuário do sistema que tem autorização para gerenciamentos gerais, desde a criação de carros até a liberação destes para alugueis.

![Tela inicial do operador](/imagens/7_tela_inicial_operador.png)

Adição e remoção de operadores.

![Gerenciamento de operadores](/imagens/8_gerenciamento_operador.png)

Gerenciamento de reservas.

![Gerenciamento de reserva](/imagens/9_gerenciamento_reserva.png)

Gerenciamento de algueis.

![Gerenciamento de algueis](/imagens/10_gerenciamento_aluguel.png)

Visualização da frota atual, isto é, informações gerais sobre todos os carros.

![Visualizacao da frota atual](/imagens/11_visualizacao_frota_atual.png)

Cadastro de carros.

![Cadastro de carros](/imagens/12_cadastro_novo_carro.png)

Gerenciamento de fabricantes.

![Gerenciamento de fabricantes](/imagens/13_gerenciar_fabricante.png)

Gerenciamento de categorias.

![Gerenciamento de categorias](/imagens/14_gerenciar_categoria.png)

## Instruções gerais - Para auxílio na utilização

Os tópicos abaixo tratam de questões que podem facilitar a utilização do projeto.

### Deploy no heroku

Para realizar o deploy de aplicações web java no heroku é necessário realizar os passos abaixo:

1. Autenticar no [heroku CLI](https://devcenter.heroku.com/articles/heroku-cli):
~~~
	heroku login
~~~
2. Caso nenhuma aplicação tenha sido criada no site é possível criar via CLI:
~~~
	heroku create
~~~
3. Após isto é possível visualizar o status de sua aplicação:

Sintaxe:
~~~
	heroku apps:info -a [ app ]
~~~

Exemplo:

~~~
	heroku apps:info -a mauricio-select
~~~
4. O plugin [jawsdb](https://devcenter.heroku.com/articles/jawsdb-maria#provisioning-the-add-on) foi utilizado para o banco relacional [MariaDB](https://mariadb.com)
~~~
	heroku addons:create jawsdb-maria
~~~
OBS.: É possível que seja solicitado o cadastro de formas de pagamento, porém este add-on é gratuito.

5. Antes de realizar o deploy é necessário configurar o [Buildpack](https://devcenter.heroku.com/articles/buildpacks) que sera utilizado para o deploy:

Sintaxe:
~~~
	heroku buildpacks:set [ buildpack ]
~~~
Exemplo:
~~~
	heroku buildpacks:set heroku/jvm
~~~
6. Para realizar o deploy o seguinte comando foi utilizado:

Sintaxe:
~~~	
	heroku war:deploy [ caminho para o arquivo.war ] --app [ nome da aplicação heroku ]
~~~	
Exemplo:
~~~
	heroku war:deploy build/libs/MauricioSelect.war --app mauricio-select
~~~
### Processo para iniciar o tomcat

É possível baixar o tomcat para linux aqui [aqui](http://mirror.nbtelecom.com.br/apache/tomcat/tomcat-8/v8.0.53/bin/apache-tomcat-8.0.53.tar.gz).

Após baixar basta descompactar o arquivo **apache-tomcat-8.0.53.tar.gz**, exemplo:
~~~
	tar -xf apache-tomcat-8.0.53.tar.gz -C /opt/
~~~
O arquivo de saída será **apache-tomcat-8.0.53** e estará localizado em /opt/apache-tomcat-8.0.53.

Os exemplos abaixo são feitos com esta forma padrão de extração.

Para o script funcionar precisa estar no diretório do tomcat, exemplo: **/opt/apache-tomcat-8.0.53/bin/**
É preciso copiar o arquivo **.war** para a pasta webapps do tomcat, exemplo: 
~~~
	cp /home/$USER/mauricioselect/MauricioSelect/dist/MauricioSelect.war /opt/apache-tomcat-8.0.53/webapps/
~~~
Após isto já é possível executar o script abaixo e o projeto ficará disponível em http://localhost:8080


Exemplo de script shell:

~~~ shell
	echo "Iniciando tomcat"

	sh shutdown.sh
	unset CATALINA_HOME
	sh startup.sh
~~~