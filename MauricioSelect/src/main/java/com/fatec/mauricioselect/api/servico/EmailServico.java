package com.fatec.mauricioselect.api.servico;

import com.fatec.mauricioselect.api.modelo.Usuario;

public interface EmailServico {
    public void enviaEmailDeRecuperacao(Usuario usuario);
}
