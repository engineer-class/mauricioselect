package com.fatec.mauricioselect.api.servico;

import com.fatec.mauricioselect.api.modelo.Aluguel;
import com.fatec.mauricioselect.api.modelo.categorico.AluguelCategorico;
import java.util.List;

public interface AluguelServico {
    AluguelCategorico inserirAluguel(Aluguel aluguel);
    AluguelCategorico encontrarAluguelPorId(Integer idAluguel);
    AluguelCategorico encontrarAluguel(Aluguel aluguel);
    List<AluguelCategorico> encontrarTudo();
    List<AluguelCategorico> encontraPorEstado(String estado);
    boolean atualizar(Aluguel aluguelAtual);
    boolean finalizaAluguel(AluguelCategorico aluguelCategorico);
    boolean deletar(Integer idAluguel);
}
