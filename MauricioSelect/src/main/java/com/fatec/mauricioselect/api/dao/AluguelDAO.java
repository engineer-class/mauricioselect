package com.fatec.mauricioselect.api.dao;

import com.fatec.mauricioselect.api.modelo.Aluguel;
import java.util.List;


public interface AluguelDAO {
    Aluguel inserir(Aluguel aluguel);
    Aluguel encontrarPorId(Integer idAluguel);
    Aluguel encontrarAluguel(Aluguel aluguel);
    List<Aluguel> encontrarPorEstado(String estado);
    List<Aluguel> encontrarTudo();
    Aluguel atualizar(Aluguel aluguelAntigo, Aluguel aluguelAtual);
    boolean deletar(Integer idAluguel);
}
