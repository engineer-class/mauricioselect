package com.fatec.mauricioselect.api.servico;

import com.fatec.mauricioselect.api.modelo.Fabricante;
import java.util.List;

public interface FabricanteServico {
    
    public Fabricante inserir(Fabricante fabricante);
    
    public Fabricante encontrarPorId(Integer id);
    
    public Fabricante encontrarPorNome(String nome);
    
    public List<Fabricante> encontrarTudo();
    
    public Fabricante atualizar(Fabricante fabricanteNovo);
    
    public boolean deletar(Integer idFabricante);
    
}
