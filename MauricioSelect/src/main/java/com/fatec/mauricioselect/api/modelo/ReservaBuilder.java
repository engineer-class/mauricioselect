package com.fatec.mauricioselect.api.modelo;

import java.sql.Date;

public class ReservaBuilder {
    
    protected Integer idCarro;
    protected Integer idReserva;
    protected Integer idUsuario;
    protected Integer idCategoria;
    protected Date reservaDiaFinal;
    protected Date reservaDiaInicio;
    protected String status;
    protected Double reservaValorTotal;
    
    public static ReservaBuilder getBuilder(){
        return new ReservaBuilder();
    }

    public ReservaBuilder adicionaIdReserva(Integer idReserva) {
        this.idReserva = idReserva;
        return this;
    }

    public ReservaBuilder adicionaIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
        return this;
    }

    public ReservaBuilder adicionaIdCategoria(Integer idCategoria) {
        this.idCategoria = idCategoria;
        return this;
    }

    public ReservaBuilder adicionaDiaFinal(Date reservaDiaFinal) {
        this.reservaDiaFinal = reservaDiaFinal;
        return this;
    }

    public ReservaBuilder adicionaDiaInicio(Date reservaDiaInicio) {
        this.reservaDiaInicio = reservaDiaInicio;
        return this;
    }

    public ReservaBuilder adicionaStatus(String status) {
        this.status = status.toUpperCase();
        return this;
    }

    public ReservaBuilder adicionaValorTotal(Double valorTotal) {
        this.reservaValorTotal = valorTotal;
        return this;
    }
    
    public ReservaBuilder adicionaIdCarro(Integer idCarro) {
        this.idCarro = idCarro;
        return this;
    }
    
    public Reserva build(){
        if(idReserva == null){
            return new Reserva(idUsuario, idCarro, idCategoria, 
                    reservaDiaFinal, reservaDiaInicio, status, reservaValorTotal);
        }
        return new Reserva(idReserva, idCarro, idUsuario, idCategoria, 
                reservaDiaFinal, reservaDiaInicio, status, reservaValorTotal);
    }
}
