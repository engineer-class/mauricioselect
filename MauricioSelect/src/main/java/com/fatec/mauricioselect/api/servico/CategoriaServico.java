package com.fatec.mauricioselect.api.servico;

import com.fatec.mauricioselect.api.modelo.Categoria;
import java.util.List;

public interface CategoriaServico {

    public List<Categoria> recuperaTodasCategorias();

    public boolean deleta(Categoria categoria);

    public Categoria inserir(Categoria categoria);

    public Categoria encontrarPorNome(String nomeCategoria);
    
    public Categoria encontrarPorId(Integer idCategria);
    
    public Categoria atualizarCategoria(Categoria novaCategoria);
}
