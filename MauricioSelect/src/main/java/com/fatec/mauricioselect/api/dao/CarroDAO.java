package com.fatec.mauricioselect.api.dao;

import com.fatec.mauricioselect.api.modelo.Carro;
import java.util.List;

public interface CarroDAO {

    public Carro inserir(Carro carro);
    public Carro encontrarPorId(Integer idCarro);
    public Carro encontrarPorPlaca(String placa);
    public List<Carro> encontrarPorAno(Integer ano);
    public List<Carro> encontrarPorFabricante(Integer idFabricante);
    public List<Carro> encontrarPorCategoria(Integer idCategoria);
    public List<Carro> encontrarPorModelo(String modelo);
    public List<Carro> encontrarTudo();
    public Carro atualizar(Carro carroAntigo, Carro carroAtual);
    public boolean deletar(Integer idCarro);
    
}
