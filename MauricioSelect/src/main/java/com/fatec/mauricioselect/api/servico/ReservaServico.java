package com.fatec.mauricioselect.api.servico;

import com.fatec.mauricioselect.api.modelo.Reserva;
import com.fatec.mauricioselect.api.modelo.categorico.ReservaCategorica;
import java.util.List;

public interface ReservaServico {

    public boolean deleta(Reserva reserva);

    public ReservaCategorica inserir(Reserva reserva);

    public List<ReservaCategorica> encontrarPorIdUsuario(Integer idUsuario);
    
    public List<ReservaCategorica> encontrarReservasDisponiveis();

    public ReservaCategorica encontrarPorIdReserva(Integer idReserva);
    
    public ReservaCategorica atualizaReserva(Reserva reservaNova);

    public List<ReservaCategorica> encontrarTudo();
}
