/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fatec.mauricioselect.api.dao;

import com.fatec.mauricioselect.api.modelo.Reserva;
import java.util.List;

public interface ReservaDAO {
    public Reserva inserir(Reserva reserva);
    public Reserva encontrarPorId(Integer id);
    public List<Reserva> encontrarPorIdUsuario(Integer reserva);
    public List<Reserva> encontrarTudo();
    List<Reserva> encontrarReservasDisponiveis();
    public Reserva atualizar(Reserva reservaAntiga, Reserva reservaAtual);
    public boolean deletar(Integer idReserva);       
}
