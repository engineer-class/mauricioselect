package com.fatec.mauricioselect.api.dao;

import com.fatec.mauricioselect.api.modelo.Operador;
import java.util.List;

/**
 *
 * @author felipe
 */
public interface OperadorDAO {
    public Operador inserir(Operador usuario);
    public Operador encontrarPorId(Integer id);
    public List<Operador> encontrarTudo();
    public boolean deletar(Integer idUsuario);
}
