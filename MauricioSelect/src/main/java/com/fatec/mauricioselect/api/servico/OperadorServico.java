package com.fatec.mauricioselect.api.servico;

import com.fatec.mauricioselect.api.modelo.Operador;

public interface OperadorServico {
    public boolean deleta(Operador operador);
    
    public Operador inserir(Operador operador);
    
    public Operador encontrarPorIdUsuario(Integer idUsuario);   
    
    public Operador encontrarPorApelido(String apelido);
    
    public boolean usuarioEhOperador(String nomeUsuario);
}
