package com.fatec.mauricioselect.api.modelo;

public class CarroBuilder {
    
    private Integer idCarro;
    private Integer idCategoria;
    private Integer idFabricante;
    private Integer ano;
    private String estadoConserva;
    private String cor;
    private String placa;
    private Double quilometragem;
    private Integer tanque;
    private String modelo;
    private Integer estaQuebrado;
    
    private CarroBuilder() {
    }
    
    public static CarroBuilder getBuilder() {
        return new CarroBuilder();
    }
    
    public CarroBuilder adicionaModelo(String modelo) {
        this.modelo = modelo;
        return this;
    }
    
    public CarroBuilder adicionaIDCarro(Integer idCarro) {
        this.idCarro = idCarro;
        return this; 
    }
    
    public CarroBuilder adicionaCategoria(Integer idCategoria) {
        this.idCategoria = idCategoria;
        return this;
    }
    
    public CarroBuilder adicionaIDFabricante(Integer idFabricante) {
        this.idFabricante = idFabricante;
        return this;
    }
    
    public CarroBuilder adicionaAno(Integer ano) {
        this.ano = ano;
        return this;
    }
    
    public CarroBuilder adicionaCor(String cor) {  
        this.cor = cor;
        return this;
    }  
    
    public CarroBuilder adicionaEstadoConserva(String estadoConserva) { 
        this.estadoConserva = estadoConserva;
        return this;
    }
    
    public CarroBuilder adicionaPlaca(String placa) {
        this.placa = placa;
        return this;
    }
    
    public CarroBuilder adicionaQuilometragem(Double quilometragem) {
        this.quilometragem = quilometragem;
        return this;
    }
    
    public CarroBuilder adicionaTanque(Integer tanque) {
        this.tanque = tanque;
        return this;
    }
    
    public CarroBuilder adicionaEstadoDoCarro(Integer estaQuebrado) {
        this.estaQuebrado = estaQuebrado;
        return this;
    }
    
    public Carro build() {
        
        Carro carro;
        
        if (this.idCarro == null) {
            carro = new Carro(
                this.idCategoria, this.idFabricante,
                this.ano, this.estadoConserva, this.cor,
                this.placa, this.quilometragem, this.tanque, this.modelo, this.estaQuebrado
            );
        } else {
            carro = new Carro(
                this.idCarro, this.idCategoria, this.idFabricante,
                this.ano, this.estadoConserva, this.cor,
                this.placa, this.quilometragem, this.tanque, this.modelo, 
                this.estaQuebrado
            );
        }
        return carro;
    }
}
