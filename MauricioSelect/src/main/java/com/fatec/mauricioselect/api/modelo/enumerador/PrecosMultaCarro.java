package com.fatec.mauricioselect.api.modelo.enumerador;

/**
 * Enumerador com os preços de cada multa que pode ser aplicada no valor total
 * do aluguel por conta do carro
 */
public enum PrecosMultaCarro {
    BOM(0), MEDIO(15), BAIXO(30), // Multas por estados 
    ESTA_QUEBRADO(3000), TANQUE_SEM_MINIMO(300); // Multas por dano no carro
    
    public int valorDaMulta;
    PrecosMultaCarro(int valor) {
        valorDaMulta = valor;
    }
    
    public int getValor() {
        return valorDaMulta;
    }
}
