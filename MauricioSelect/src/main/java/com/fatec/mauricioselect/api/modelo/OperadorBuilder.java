/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fatec.mauricioselect.api.modelo;

/**
 *
 * @author felipe
 */
public class OperadorBuilder {
    
    protected Integer idUsuario;    
    protected String nome;
    protected String senha;
    protected String apelido;
    protected String cpf;
    protected String email;
    protected String rg;
    protected String sexo;
    private String cargo;
    private boolean foiConsumido;
    private Usuario usuario;
   
    public static OperadorBuilder getBuilder() {
        return new OperadorBuilder();
    }
    
    public OperadorBuilder adicionaNome(String nome) {
        this.nome = nome;
        return this;
    }
    
    public OperadorBuilder adicionaApelido(String apelido) {
        this.apelido = apelido;
        return this;
    }
    
    public OperadorBuilder adicionaID(Integer idUsuario) {
        this.idUsuario = idUsuario;
        return this;
    }
        
    public OperadorBuilder adicionaSenha(String senha) {
        this.senha = senha;
        return this;
    }
    
    public OperadorBuilder adicionaCPF(String cpf) {
        this.cpf = cpf;
        return this;
    }
    
    public OperadorBuilder adicionaEmail(String email) {
        this.email = email;
        return this;
    }
    
    public OperadorBuilder adicionaRG(String rg) {
        this.rg = rg;
        return this;
    }
    
    public OperadorBuilder adicionaSexo(String sexo) {
        this.sexo = sexo;
        return this;
    }
        
    /**
     * Método para adicionar ao builder um objeto do tipo usuário
     * @param usuario
     * @return 
     */
    public OperadorBuilder consomeObjetoUsuario(Usuario usuario) {
        this.usuario = usuario;
        this.foiConsumido = true;
        return this;
    }
    
    public OperadorBuilder adicionaCargo(String cargo) {
        this.cargo = cargo;
        return this;
    }
   
    public Operador build() {
        if (!foiConsumido) {
            return new Operador(apelido, cargo, senha, email, nome, cpf, rg, sexo);
        } else {
            Operador operador;
            
            
            if (usuario.getIdUsuario() == null) {
                operador = new Operador(usuario.getApelido(), cargo,
                    usuario.getSenha(), usuario.getEmail(),
                    usuario.getNome(), usuario.getCpf(), usuario.getRg(),
                    usuario.getSexo());
            } else {
                operador = new Operador(usuario.getIdUsuario(), 
                        usuario.getApelido(), cargo,
                        usuario.getSenha(), usuario.getEmail(),
                        usuario.getNome(), usuario.getCpf(), usuario.getRg(),
                        usuario.getSexo());
            }
            return operador;
        }
    }
}
