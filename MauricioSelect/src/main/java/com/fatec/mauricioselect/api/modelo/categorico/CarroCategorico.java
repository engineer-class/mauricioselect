package com.fatec.mauricioselect.api.modelo.categorico;

import com.fatec.mauricioselect.api.modelo.Categoria;
import com.fatec.mauricioselect.api.modelo.Fabricante;
import com.fatec.mauricioselect.api.modelo.abstrato.CarroAbstrato;

public class CarroCategorico extends CarroAbstrato {
    
    private Categoria categoria;
    private Fabricante fabricante;
    private String status;
    
    public CarroCategorico(){};
    
    public CarroCategorico(Integer idCarro) {
        super(idCarro);
    }
    
    public CarroCategorico(Integer idCarro, Categoria categoria, Fabricante fabricante,
            Integer ano, String estadoConserva, String cor, String placa,
            Double quilometragem, Integer tanque, String modelo, Integer estaQuebrado) {
        super(idCarro,ano,estadoConserva, cor, placa, quilometragem, tanque, modelo, estaQuebrado);
        this.categoria = categoria;
        this.fabricante = fabricante;
        
    }
    
    public CarroCategorico(Categoria categoria, Fabricante fabricante,
            Integer ano, String estadoConserva, String cor, String placa,
            Double quilometragem, Integer tanque, String modelo, Integer estaQuebrado) {
        
        super(ano,estadoConserva, cor, placa, quilometragem, tanque, modelo, estaQuebrado);
        
        this.categoria = categoria;
        this.fabricante = fabricante;

    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public Fabricante getFabricante() {
        return fabricante;
    }

    public void setFabricante(Fabricante fabricante) {
        this.fabricante = fabricante;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
