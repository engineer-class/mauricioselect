package com.fatec.mauricioselect.api.dao;

import com.fatec.mauricioselect.api.modelo.Categoria;
import java.util.List;

/**
 *
 * @author felipe
 */
public interface CategoriaDAO {

    public Categoria inserir(Categoria categoria);
    public Categoria encontrarPorId(Integer id);
    public Categoria encontrarPorNome(String categoriaNome);
    public List<Categoria> encontrarTudo();
    public Categoria atualizar(Categoria categoriaAntiga, Categoria categoriaAtual);
    public boolean deletar(Integer idCategoria);
    
}
