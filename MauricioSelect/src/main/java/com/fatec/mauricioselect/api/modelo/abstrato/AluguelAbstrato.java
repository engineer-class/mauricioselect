package com.fatec.mauricioselect.api.modelo.abstrato;

import java.sql.Date;

public abstract class AluguelAbstrato {
    
    private Integer idAluguel;
    private Integer idReserva;
    private String status;
    private Date dataRetirada;
    private Date dataDevolucao;
    private Double valorTotal;
    private Double valorPagoAntecipado;
        
    public AluguelAbstrato(Integer idAluguel, Integer idReserva, Date dataDevolucao, 
            Double valorTotal, String status) {
        this.idAluguel = idAluguel;
        this.idReserva = idReserva;
        this.dataDevolucao = dataDevolucao;
        this.valorTotal = valorTotal;
        this.status = status;        
    }
    
    public AluguelAbstrato(Integer idAluguel, Date dataDevolucao, 
            Double valorTotal, String status) {
        this.idAluguel = idAluguel;
        this.dataDevolucao = dataDevolucao;
        this.valorTotal = valorTotal;
        this.status = status;
    }
    
    public AluguelAbstrato(Integer idAluguel, Integer idReserva, String status, 
            Date dataRetirada, Date dataDevolucao, Double valorTotal, Double valorPagoAntecipado) {
        this.idAluguel = idAluguel;
        this.idReserva = idReserva;
        this.status = status;
        this.dataRetirada = dataRetirada;
        this.dataDevolucao = dataDevolucao;
        this.valorTotal = valorTotal;
        this.valorPagoAntecipado = valorPagoAntecipado;
    }

    public AluguelAbstrato(Integer idReserva, String status, 
            Date dataRetirada, Date dataDevolucao, Double valorTotal, Double valorPagoAntecipado) {
        this.idReserva = idReserva;
        this.status = status;
        this.dataRetirada = dataRetirada;
        this.dataDevolucao = dataDevolucao;
        this.valorTotal = valorTotal;
        this.valorPagoAntecipado = valorPagoAntecipado;
    }
    
    public Integer getIdAluguel() {
        return idAluguel;
    }

    public void setIdAluguel(Integer idAluguel) {
        this.idAluguel = idAluguel;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDataRetirada() {
        return dataRetirada;
    }

    public void setDataRetirada(Date dataRetirada) {
        this.dataRetirada = dataRetirada;
    }

    public Date getDataDevolucao() {
        return dataDevolucao;
    }

    public void setDataDevolucao(Date dataDevolucao) {
        this.dataDevolucao = dataDevolucao;
    }

    public Double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public Double getValorPagoAntecipado() {
        return valorPagoAntecipado;
    }

    public void setValorPagoAntecipado(Double valorPagoAntecipado) {
        this.valorPagoAntecipado = valorPagoAntecipado;
    }    

    public Integer getIdReserva() {
        return idReserva;
    }

    public void setIdReserva(Integer idReserva) {
        this.idReserva = idReserva;
    }    
}
