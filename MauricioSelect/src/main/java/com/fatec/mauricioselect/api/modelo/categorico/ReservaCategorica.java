package com.fatec.mauricioselect.api.modelo.categorico;

import com.fatec.mauricioselect.api.modelo.Carro;
import com.fatec.mauricioselect.api.modelo.Usuario;
import com.fatec.mauricioselect.api.modelo.Categoria;
import com.fatec.mauricioselect.api.modelo.abstrato.ReservaAbstrata;
import java.sql.Date;

/*
    Representação da tabela ponte criada para trabalhar com os dados
reais armazenados em cada coluna da tabela, e não apenas os IDs
*/
public class ReservaCategorica extends ReservaAbstrata {
    
    private Usuario usuario;
    private Categoria categoria;
    private Carro carro;

    public ReservaCategorica(Usuario usuario, Categoria categoria, Carro carro, 
            Integer idReserva, Date diaInicio, Date diaFinal, String status, Double valorTotal) {
        super(idReserva, diaInicio, diaFinal, status, valorTotal);
        this.usuario = usuario;
        this.categoria = categoria;
        this.carro = carro;
    }

    public ReservaCategorica(Usuario usuario, Categoria categoria, Carro carro,
            Date diaInicio, Date diaFinal, String status, Double valorTotal) {
        super(diaInicio, diaFinal, status, valorTotal);
        this.usuario = usuario;
        this.categoria = categoria;
        this.carro = carro;
    }  

    public Usuario getUsuario() {       
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public Carro getCarro() {
        return carro;
    }

    public void setCarro(Carro carro) {
        this.carro = carro;
    }   
}
