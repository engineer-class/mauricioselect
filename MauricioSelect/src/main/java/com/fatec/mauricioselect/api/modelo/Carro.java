package com.fatec.mauricioselect.api.modelo;

import com.fatec.mauricioselect.api.modelo.abstrato.CarroAbstrato;
import java.util.Objects;

public class Carro extends CarroAbstrato implements Comparable<Carro> {

    private Integer idCategoria;
    private Integer idFabricante;
    
    public Carro(Integer idCarro) {
        super(idCarro);
    }
    
    public Carro(Integer idCarro, Integer idCategoria, Integer idFabricante,
            Integer ano, String estadoConserva, String cor, String placa,
            Double quilometragem, Integer tanque, String modelo, Integer estaQuebrado) {
        super(idCarro,ano,estadoConserva, cor, placa, quilometragem, tanque, modelo, estaQuebrado);
        this.idCategoria = idCategoria;
        this.idFabricante = idFabricante;
        
    }
    
    public Carro(Integer idCategoria, Integer idFabricante,
            Integer ano, String estadoConserva, String cor, String placa,
            Double quilometragem, Integer tanque, String modelo, Integer estaQuebrado) {
        
        super(ano,estadoConserva, cor, placa, quilometragem, tanque, modelo, estaQuebrado);
        
        this.idCategoria = idCategoria;
        this.idFabricante = idFabricante;

    }

    public Integer getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Integer idCategoria) {
        this.idCategoria = idCategoria;
    }

    public Integer getIdFabricante() {
        return idFabricante;
    }

    public void setIdFabricante(Integer idFabricante) {
        this.idFabricante = idFabricante;
    }
       
    @Override
    public int compareTo(Carro t) {
        if (Objects.equals(t.getIdCarro(), this.getIdCarro())) {
            return 1;
        } 
        return -1;
    } 
}
