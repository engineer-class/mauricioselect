package com.fatec.mauricioselect.api.modelo;

import com.fatec.mauricioselect.api.modelo.abstrato.AluguelAbstrato;
import java.sql.Date;

public class Aluguel extends AluguelAbstrato {

    private Integer idUsuario;
    private Integer idOperador;
    private Integer idCarro;
    private Double valorTotalAPagar;
   
    public Aluguel(Integer idAluguel, Integer idReserva, Date dataDevolucao, 
            Double valorTotal, String status) {
        super(idAluguel, idReserva, dataDevolucao, valorTotal, status);
    }
    
    public Aluguel(Integer idAluguel, Date dataDevolucao, 
            Double valorTotal, String status) {
        super(idAluguel, dataDevolucao, valorTotal, status);
    }
    
    public Aluguel(Integer idAluguel, Date dataDevolucao, 
            Double valorTotal, Double valorTotalAPagar, String status) {
        super(idAluguel, dataDevolucao, valorTotal, status);
        this.valorTotalAPagar = valorTotalAPagar;
    }
    
    public Aluguel(Integer idReserva, String status,
            Date dataRetirada, Date dataDevolucao, Double valorTotal, 
            Double valorPagoAntecipado, Integer idUsuario, 
            Integer idOperador, Integer idCarro) {
        
        super(idReserva, status, dataRetirada, 
                dataDevolucao, valorTotal, valorPagoAntecipado);
        this.idOperador = idOperador;
        this.idUsuario = idUsuario;
        this.idCarro = idCarro;
    }
    
    public Aluguel(Integer idAluguel, Integer idReserva, String status,
            Date dataRetirada, Date dataDevolucao, Double valorTotal, 
            Double valorPagoAntecipado, Integer idUsuario, 
            Integer idOperador, Integer idCarro) {
        
        super(idAluguel, idReserva, status, dataRetirada, 
                dataDevolucao, valorTotal, valorPagoAntecipado);
        this.idOperador = idOperador;
        this.idUsuario = idUsuario;
        this.idCarro = idCarro;
    }
    
    public Integer getIdUsuario() {
        return idUsuario;
    }

    public Integer getIdOperador() {
        return idOperador;
    }

    public Integer getIdCarro() {
        return idCarro;
    }

    public Double getValorTotalAPagar() {
        return valorTotalAPagar;
    }
}
