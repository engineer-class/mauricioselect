package com.fatec.mauricioselect.api.dao;

import com.fatec.mauricioselect.api.modelo.Fabricante;
import java.util.List;

public interface FabricanteDAO {

    public Fabricante inserir(Fabricante fabricante);
    public Fabricante encontrarPorId(Integer id);
    public Fabricante encontrarPorNome(String nome);
    public List<Fabricante> encontrarTudo();
    public Fabricante atualizar(Fabricante fabricanteAntiga, Fabricante fabricanteAtual);
    public boolean deletar(Integer idFabricante);
        
}
