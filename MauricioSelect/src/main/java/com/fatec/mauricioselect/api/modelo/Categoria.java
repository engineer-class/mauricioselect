package com.fatec.mauricioselect.api.modelo;

public class Categoria {
    
    private Integer idCategoria;
    private String nome;
    private Double valorPorHora;

    public Categoria(Integer idCategoria) {
        this.idCategoria = idCategoria;
    }
    
    public Categoria(String nome, Integer idCategoria, Double valorPorHora) { 
        this.nome = nome;
        this.idCategoria = idCategoria;
        this.valorPorHora = valorPorHora;
    }
    
    public Categoria(String nome, Double valorPorHora) {
        this.nome = nome.toUpperCase();
        this.valorPorHora = valorPorHora;
    }

    public Categoria(Integer idCategoria, String nome, Double valorPorHora) {
        this.idCategoria = idCategoria;
        this.nome = nome;
        this.valorPorHora = valorPorHora;
    }
    
    public Integer getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Integer idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getValorPorHora() {
        return valorPorHora;
    }

    public void setValorPorHora(Double valorPorHora) {
        this.valorPorHora = valorPorHora;
    }  
}
