package com.fatec.mauricioselect.api.modelo;

import com.fatec.mauricioselect.api.modelo.abstrato.ReservaAbstrata;
import java.sql.Date;

public class Reserva extends ReservaAbstrata {
    
    private Integer idUsuario;
    private Integer idCategoria;
    private Integer idCarro;
    
    public Reserva(Integer idReserva) {
        super(idReserva);
    }
        
    public Reserva(Integer idReserva, Integer idCarro, Integer idUsuario, 
            Integer idCategoria, Date reservaDiaFinal, Date reservaDiaInicio, 
            String status, Double reservaValorTotal) {

        super(idReserva, reservaDiaInicio, reservaDiaFinal, status, reservaValorTotal);

        this.idCarro = idCarro;
        this.idUsuario = idUsuario;
        this.idCategoria = idCategoria;
    }

    public Reserva(Integer idUsuario, Integer idCarro, Integer idCategoria, Date reservaDiaFinal, 
            Date reservaDiaInicio, String status, Double reservaValorTotal) {
        
        super(reservaDiaInicio, reservaDiaFinal, status, reservaValorTotal);
        
        this.idUsuario = idUsuario;
        this.idCarro = idCarro;
        this.idCategoria = idCategoria;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Integer idCategoria) {
        this.idCategoria = idCategoria;
    }

    public Integer getIdCarro() {
        return idCarro;
    }

    public void setIdCarro(Integer idCarro) {
        this.idCarro = idCarro;
    }  
}
