package com.fatec.mauricioselect.api.modelo.abstrato;

import java.sql.Date;

public abstract class ReservaAbstrata {
    private Integer idReserva;
    private Date diaInicio;
    private Date diaFinal;
    private String status;
    private Double valorTotal;
    
    public ReservaAbstrata() {
        
    }
    
    public ReservaAbstrata(Integer idReserva) {
        this.idReserva = idReserva;
    }
    
    public ReservaAbstrata(Integer idReserva, Date diaInicio, 
            Date diaFinal, String status, Double valorTotal) {
      
        this.idReserva = idReserva;
        this.diaInicio = diaInicio;
        this.diaFinal = diaFinal;
        this.status = status;
        this.valorTotal = valorTotal;
    }
    
    public ReservaAbstrata(Date diaInicio, Date diaFinal, String status, 
            Double valorTotal) {
      
        this.diaInicio = diaInicio;
        this.diaFinal = diaFinal;
        this.status = status;
        this.valorTotal = valorTotal;
    }    

    public Integer getIdReserva() {
        return idReserva;
    }

    public void setIdReserva(Integer idReserva) {
        this.idReserva = idReserva;
    }

    public Date getDiaInicio() {
        return diaInicio;
    }

    public void setDiaInicio(Date diaInicio) {
        this.diaInicio = diaInicio;
    }

    public Date getDiaFinal() {
        return diaFinal;
    }

    public void setDiaFinal(Date diaFinal) {
        this.diaFinal = diaFinal;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
    }  
}
