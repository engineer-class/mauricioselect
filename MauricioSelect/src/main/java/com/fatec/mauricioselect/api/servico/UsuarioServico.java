package com.fatec.mauricioselect.api.servico;

import com.fatec.mauricioselect.api.modelo.Usuario;
import java.util.List;

public interface UsuarioServico {
    public boolean deleta(Usuario usuario);
    
    public Usuario inserir(Usuario usuario);
    
    public Usuario encontrarPorApelido(String apelidoUsuario); 
    
    public Usuario encontrarPorEmail(String email);
    
    public boolean validaUsuario(String nomeUsuario, String senha);
    
    public Usuario atualizaUsuario(Usuario usuarioNovo);
    
    public Usuario encontrarUsuarioPorId(Integer idUsuario);
    
    public List<Usuario> encontrarTodosOsUsuarios();
    
}
