package com.fatec.mauricioselect.api.modelo;

import java.util.Objects;

public class Usuario {
    
    private Integer idUsuario;
    private String apelido;
    private String senha;
    private String email;
    private String nome;
    private String cpf;
    private String rg;
    private String sexo;
    
    public Usuario() {
    }

    public Usuario(String apelido) { 
        this.apelido = apelido;
    }
    
    public Usuario(String apelido, String senha, 
            String email, String nome, String cpf, String rg, String sexo) {
        this.apelido = apelido;
        this.senha = senha;
        this.email = email;
        this.nome = nome;
        this.cpf = cpf;
        this.rg = rg;
        this.sexo = sexo;
    }

    public Usuario(Integer idUsuario, String apelido, String senha,
            String email, String nome, String cpf, String rg, String sexo) {
        this.idUsuario = idUsuario;
        this.apelido = apelido;
        this.senha = senha;
        this.email = email;
        this.nome = nome;
        this.cpf = cpf;
        this.rg = rg;
        this.sexo = sexo;
    }

    public Usuario(String apelido, String email, String rg,
            String cpf, String nome) {
        this.apelido = apelido;
        this.email = email;
        this.rg = rg;
        this.cpf = cpf;
        this.nome = nome;
    }
    
    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getApelido() {
        return apelido;
    }

    public void setApelido(String apelido) {
        this.apelido = apelido;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    @Override
    public boolean equals(Object o) {
        Usuario usuarioComparado = (Usuario) o;
        
        return usuarioComparado.getApelido().equals(this.apelido) ||
                usuarioComparado.getEmail().equals(this.email) ||
                usuarioComparado.getRg().equals(this.rg);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + Objects.hashCode(this.apelido);
        hash = 23 * hash + Objects.hashCode(this.email);
        hash = 23 * hash + Objects.hashCode(this.rg);
        return hash;
    }
}
