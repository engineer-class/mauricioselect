package com.fatec.mauricioselect.api.modelo;

public class Operador extends Usuario {
    private String cargo;
    
    public Operador() {
    }
    
    public Operador(String usuario, String cargo) {
        super(usuario);
        this.cargo = cargo;
    }
    
    public Operador(String usuario, String cargo, String senha, 
            String email, String nome, String cpf, String rg, String sexo) {
        
        super(usuario, senha, email, nome, cpf, rg, sexo);
        this.cargo = cargo;     
    }
    
    public Operador(Integer idUsuario, String usuario, String cargo, String senha, 
        String email, String nome, String cpf, String rg, String sexo) {
        
        super(idUsuario, usuario, senha, email, nome, cpf, rg, sexo);
        this.cargo = cargo;     
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }
}
