package com.fatec.mauricioselect.api.modelo.excecao;

/**
 * Exceção para a representação de pontos que apresentam erros com o conteúdo
 * dos dados
 */
public class ExcecaoDeDadosInvalidos extends RuntimeException {
    
    private String mensagemDeErro;
    
    public ExcecaoDeDadosInvalidos(String mensagemDeErro) {
        super();
        this.mensagemDeErro = mensagemDeErro;
    }

    @Override
    public String getMessage() {
        return mensagemDeErro;
    } 

    @Override
    public String getLocalizedMessage() {
        return mensagemDeErro;
    }
}
