package com.fatec.mauricioselect.api.dao;

import com.fatec.mauricioselect.api.modelo.Usuario;
import java.util.List;

/**
 *
 * @author felipe
 */
public interface UsuarioDAO {
    public Usuario inserir(Usuario usuario);
    public Usuario encontrarPorId(Integer id);
    public Usuario encontrarPorApelido(String apelidoUsuario);
    public Usuario encontrarPorEmail(String email);
    public List<Usuario> encontrarTudo();
    public Usuario atualizar(Usuario usuarioAntigo, Usuario usuarioAtual);
    public boolean deletar(Integer idUsuario);
}
