package com.fatec.mauricioselect.api.modelo;

import java.sql.Date;

public class AluguelBuilder {
    protected String status;
    protected Integer idCarro;
    protected Integer idAluguel;
    protected Integer idUsuario;
    protected Integer idOperador;
    protected Integer idReserva;
    protected Date dataDevolucao;
    protected Date dataRetirada;
    protected Double valorTotal;
    protected Double valorPagoAntecipado;
    
    public static AluguelBuilder getBuilder() {
        return new AluguelBuilder();
    }

    public AluguelBuilder adicionaIdAluguel(Integer idAluguel) {
        this.idAluguel = idAluguel;
        return this;
    }

    public AluguelBuilder adicionaIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
        return this;
    }

    public AluguelBuilder adicionaIdOperador(Integer idOperador) {
        this.idOperador = idOperador;
        return this;
    }

    public AluguelBuilder adicionaIdReserva(Integer idReserva) {
        this.idReserva = idReserva;
        return this;
    }

    public AluguelBuilder adicionaDataDevolucao(Date dataDevolucao) {
        this.dataDevolucao = dataDevolucao;
        return this;
    }

    public AluguelBuilder adicionaDataRetirada(Date dataRetirada) {
        this.dataRetirada = dataRetirada;
        return this;
    }
    
    public AluguelBuilder adicionaIdCarro(Integer idCarro) {
        this.idCarro = idCarro;
        return this;
    }
    
    public AluguelBuilder adicionaValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
        return this;
    }
    
    public AluguelBuilder adicionaValorPagoAntecipado(Double valorPagoAntecipado) {
        this.valorPagoAntecipado = valorPagoAntecipado;
        return this;
    }
    
    public AluguelBuilder adicionaStatus(String status) {
        this.status = status;
        return this;
    }

    public Aluguel build() {
        if(idAluguel == null) 
            return new Aluguel(idReserva, status, dataRetirada, dataDevolucao, 
                    valorTotal, valorPagoAntecipado, idUsuario, idOperador, idCarro);        
        return new Aluguel(idAluguel, idReserva, status, dataRetirada, dataDevolucao, 
                    valorTotal, valorPagoAntecipado, idUsuario, idOperador, idCarro);
    }
}
