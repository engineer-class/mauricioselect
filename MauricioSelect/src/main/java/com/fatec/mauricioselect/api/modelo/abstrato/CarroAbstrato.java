package com.fatec.mauricioselect.api.modelo.abstrato;

public abstract class CarroAbstrato {
    
    private Integer idCarro;
    private String modelo;
    private Integer ano;
    private String estadoConserva;
    private String cor;
    private String placa;
    private Double quilometragem;
    private Integer tanque;
    private Integer estaQuebrado;

    public CarroAbstrato(){}
    
    public CarroAbstrato(Integer idCarro) {
        this.idCarro = idCarro;
    }
    
    public CarroAbstrato(Integer idCarro, Integer ano, String estadoConserva, String cor, String placa,
            Double quilometragem, Integer tanque, String modelo, Integer estaQuebrado) {
        this.idCarro = idCarro;
        this.modelo = modelo;
        this.estadoConserva = estadoConserva;
        this.ano = ano;
        this.cor = cor;
        this.placa = placa;
        this.quilometragem = quilometragem;
        this.tanque = tanque;
        this.estaQuebrado = estaQuebrado;
    }
    
    public CarroAbstrato(Integer ano, String estadoConserva, String cor, String placa, 
            Double quilometragem, Integer tanque, String modelo, Integer estaQuebrado) {
        this.modelo = modelo;
        this.ano = ano;
        this.estadoConserva = estadoConserva;
        this.cor = cor;
        this.placa = placa;
        this.quilometragem = quilometragem;
        this.tanque = tanque;
        this.estaQuebrado = estaQuebrado;
    }

    public Integer getEstaQuebrado() {
        return estaQuebrado;
    }

    public void setEstaQuebrado(Integer estaQuebrado) {
        this.estaQuebrado = estaQuebrado;
    }

    public Integer getIdCarro() {
        return idCarro;
    }

    public void setIdCarro(Integer idCarro) {
        this.idCarro = idCarro;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public Integer getAno() {
        return ano;
    }

    public void setAno(Integer ano) {
        this.ano = ano;
    }

    public String getEstadoConserva() {
        return estadoConserva;
    }

    public void setEstadoConserva(String estadoConserva) {
        this.estadoConserva = estadoConserva;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public Double getQuilometragem() {
        return quilometragem;
    }

    public void setQuilometragem(Double quilometragem) {
        this.quilometragem = quilometragem;
    }

    public Integer getTanque() {
        return tanque;
    }

    public void setTanque(Integer tanque) {
        this.tanque = tanque;
    }

}
