package com.fatec.mauricioselect.api.modelo;

/**
 *
 * @author felipe
 */
public class UsuarioBuilder {

    protected Integer idUsuario = null;    
    protected String nome = null;
    protected String senha = null;
    protected String apelido = null;
    protected String cpf = null;
    protected String email = null;
    protected String rg = null;
    protected String sexo = null;
    
    public static UsuarioBuilder getBuilder() {
        return new UsuarioBuilder();
    }
    
    public UsuarioBuilder adicionaNome(String nome) {
        this.nome = nome;
        return this;
    }
    
    public UsuarioBuilder adicionaApelido(String apelido) {
        this.apelido = apelido;
        return this;
    }
    
    public UsuarioBuilder adicionaID(Integer idUsuario) {
        this.idUsuario = idUsuario;
        return this;
    }
        
    public UsuarioBuilder adicionaSenha(String senha) {
        this.senha = senha;
        return this;
    }
    
    public UsuarioBuilder adicionaCPF(String cpf) {
        this.cpf = cpf;
        return this;
    }
    
    public UsuarioBuilder adicionaEmail(String email) {
        this.email = email;
        return this;
    }
    
    public UsuarioBuilder adicionaRG(String rg) {
        this.rg = rg;
        return this;
    }
    
    public UsuarioBuilder adicionaSexo(String sexo) {
        this.sexo = sexo;
        return this;
    }
    
    public Usuario build() {
        if (null == idUsuario) {
            return new Usuario(apelido, senha, email, nome, cpf, rg, sexo);
        } else {            
            return new Usuario(idUsuario, apelido, senha, email, nome, cpf, rg, sexo);
        }
    }
}
