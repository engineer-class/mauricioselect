package com.fatec.mauricioselect.api.modelo;

public class Fabricante {

    private Integer idFabricante;
    private String nome; 

    public Fabricante(String nome) {
        this.nome = nome.toUpperCase();
    }

    public Fabricante(Integer idFabricante, String nome) {
        this.idFabricante = idFabricante;
        this.nome = nome;
    }
    
    public Integer getIdFabricante() {
        return idFabricante;
    }

    public void setIdFabricante(Integer idFabricante) {
        this.idFabricante = idFabricante;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
