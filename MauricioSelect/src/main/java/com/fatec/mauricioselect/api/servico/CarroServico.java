package com.fatec.mauricioselect.api.servico;

import com.fatec.mauricioselect.api.modelo.Carro;
import com.fatec.mauricioselect.api.modelo.categorico.CarroCategorico;
import java.util.List;

public interface CarroServico {
    
    public boolean deleta(Carro carro);
    
    public CarroCategorico inserir(Carro carro);
    
    public CarroCategorico encontrarPorId(Integer idCarro);
    
    public CarroCategorico atualizar(Carro carroNovo);
    
    public List<CarroCategorico> encontrarPorCategoria(String nomeCategoria);
    
    public List<CarroCategorico> encontrarPorModelo(String modelo);
    
    public List<CarroCategorico> encontrarTodosOsCarros();
    
    public List<CarroCategorico> encontrarCarrosDisponiveis();
    
    public List<CarroCategorico> encontrarCarrosDisponiveisPorCategoria(String categoria);
    
    public List<CarroCategorico> encontrarCarrosIndisponiveis();
    
}
