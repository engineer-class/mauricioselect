package com.fatec.mauricioselect.api.modelo.categorico;

import com.fatec.mauricioselect.api.modelo.abstrato.AluguelAbstrato;
import com.fatec.mauricioselect.api.modelo.Carro;
import com.fatec.mauricioselect.api.modelo.Categoria;
import com.fatec.mauricioselect.api.modelo.Operador;
import com.fatec.mauricioselect.api.modelo.Reserva;
import com.fatec.mauricioselect.api.modelo.Usuario;
import java.sql.Date;

/*
    Representação da tabela ponte criada para trabalhar com os dados
reais armazenados em cada coluna da tabela, e não apenas os IDs
*/
public class AluguelCategorico extends AluguelAbstrato {
    
    private Usuario usuario;
    private Operador operador;
    private Carro carro;
    private Categoria categoria;
    private Reserva reserva;

    public AluguelCategorico(Integer idReserva, 
            String status, Date dataRetirada, Date dataDevolucao, 
            Double valorTotal, Double valorPagoAntecipado, Usuario usuario,
            Operador operador, Carro carro, Categoria categoria, Reserva reserva) {
        super(idReserva, status, dataRetirada, dataDevolucao, valorTotal, valorPagoAntecipado);
        
        this.usuario = usuario;
        this.operador = operador;
        this.carro = carro;
        this.categoria = categoria;
        this.reserva = reserva;
    }

    public AluguelCategorico(Integer idAluguel, Integer idReserva, 
            String status, Date dataRetirada, Date dataDevolucao, 
            Double valorTotal, Double valorPagoAntecipado, Usuario usuario,
            Operador operador, Carro carro, Categoria categoria, Reserva reserva) {
        super(idAluguel, idReserva, status, dataRetirada, dataDevolucao, valorTotal, valorPagoAntecipado);
        
        this.usuario = usuario;
        this.operador = operador;
        this.carro = carro;
        this.categoria = categoria;
        this.reserva = reserva;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Operador getOperador() {
        return operador;
    }

    public void setOperador(Operador operador) {
        this.operador = operador;
    }

    public Carro getCarro() {
        return carro;
    }

    public void setCarro(Carro carro) {
        this.carro = carro;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }  

    public Reserva getReserva() {
        return reserva;
    }

    public void setReserva(Reserva reserva) {
        this.reserva = reserva;
    }    
}
