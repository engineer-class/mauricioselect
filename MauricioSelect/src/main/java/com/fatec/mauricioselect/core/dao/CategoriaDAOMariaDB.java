package com.fatec.mauricioselect.core.dao;

import com.fatec.mauricioselect.api.dao.CategoriaDAO;
import com.fatec.mauricioselect.api.modelo.Categoria;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author felipe
 */
public class CategoriaDAOMariaDB implements CategoriaDAO {

    private Conexao conexao;
    
    public CategoriaDAOMariaDB() {
        conexao = new Conexao();
    }
    
    @Override
    public Categoria inserir(Categoria categoria) {
        
        Connection conn = conexao.novaConexao();
        PreparedStatement sql;
        
        try {
            sql = conn.prepareStatement("INSERT INTO categoria "
                    + "(cat_nome, cat_valor_por_hora) VALUES(?, ?)");
        
            sql.setString(1, categoria.getNome());
            sql.setDouble(2, categoria.getValorPorHora());
            sql.execute();
            sql.close();
        } catch (SQLException ex) {
            Logger.getLogger(CategoriaDAOMariaDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        // Insere a categoria, depois retorna resultado da busca
        // caso não seja nulo, está correto
        return encontrarPorNome(categoria.getNome());
    }
    
    @Override
    public Categoria encontrarPorNome(String categoriaNome) {
        Connection conn = conexao.novaConexao();
        PreparedStatement sql;
        ResultSet rs = null;
        Categoria categoria = null;
        
        try {
            sql = conn.prepareStatement("SELECT cat_id, cat_nome, cat_valor_por_hora FROM "
                    + "categoria WHERE cat_nome = ?");
           
            sql.setString(1, categoriaNome.toUpperCase());
            rs = sql.executeQuery();
            sql.close();
            
            if (rs.next()){                 
                categoria = new Categoria(rs.getInt("cat_id"),
                                            rs.getString("cat_nome"), 
                                            rs.getDouble("cat_valor_por_hora"));
            } 
        
        } catch (SQLException ex) {
            Logger.getLogger(CategoriaDAOMariaDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return categoria;
    }

    @Override
    public Categoria encontrarPorId(Integer id) {
        Connection conn = conexao.novaConexao();
        PreparedStatement sql;
        ResultSet rs = null;
        Categoria categoria = null;
        
        try {
            sql = conn.prepareStatement("SELECT cat_id, cat_nome, "
                    + "cat_valor_por_hora FROM categoria WHERE cat_id = ?");
           
            sql.setInt(1, id);
            rs = sql.executeQuery();
            sql.close();
            
            if (rs.next()){                 
                categoria = new Categoria(rs.getInt("cat_id"),
                                            rs.getString("cat_nome"), 
                                            rs.getDouble("cat_valor_por_hora"));
            } 
        
        } catch (SQLException ex) {
            Logger.getLogger(CategoriaDAOMariaDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return categoria;
    }

    @Override
    public List<Categoria> encontrarTudo() {
        Connection conn = conexao.novaConexao();
        PreparedStatement sql;
        ResultSet rs = null;
        Categoria categoria = null;
        List<Categoria> categorias = new LinkedList<>();
        
        try {
            sql = conn.prepareStatement("SELECT cat_id, cat_nome, "
                    + "cat_valor_por_hora FROM categoria");
           
            rs = sql.executeQuery();
            sql.close();  
            
            while (rs.next()) {
                categorias.add(new Categoria(rs.getInt("cat_id"),
                                            rs.getString("cat_nome"), 
                                            rs.getDouble("cat_valor_por_hora")));
            }
        } catch (SQLException ex) {
            Logger.getLogger(CategoriaDAOMariaDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return categorias;        
    }

    @Override
    public Categoria atualizar(Categoria categoriaAntiga, Categoria categoriaAtual) {
        Connection conn = conexao.novaConexao();
        PreparedStatement sql;
        ResultSet rs = null;
        Categoria categoria = null;
        
        try {   
            sql = conn.prepareStatement("UPDATE categoria "
                    + "SET cat_nome = ?, cat_valor_por_hora = ?"
                    + " WHERE cat_id = ?");
            sql.setString(1, categoriaAtual.getNome());
            sql.setDouble(2, categoriaAtual.getValorPorHora());
            sql.setInt(3, categoriaAtual.getIdCategoria());
            sql.execute();
            
        } catch (SQLException ex) {
            Logger.getLogger(CategoriaDAOMariaDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return encontrarPorNome(categoriaAtual.getNome());
    }

    @Override
    public boolean deletar(Integer idCategoria) {
        Connection conn = conexao.novaConexao();
        PreparedStatement sql;
        boolean resultado = false;
        
        try {
            sql = conn.prepareStatement("DELETE FROM categoria WHERE "
                    + "cat_id = ?");
            sql.setInt(1, idCategoria);
            resultado = !sql.execute();
            sql.close();   
            
        } catch (SQLException ex) {
            Logger.getLogger(CategoriaDAOMariaDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultado;
    }    
}
