package com.fatec.mauricioselect.core.dao;

import com.fatec.mauricioselect.api.dao.OperadorDAO;
import com.fatec.mauricioselect.api.modelo.Operador;
import com.fatec.mauricioselect.api.modelo.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class OperadorDAOMariaDB implements OperadorDAO {

    private Conexao conexao;
    
    public OperadorDAOMariaDB() {
        conexao = new Conexao();
    }
    
    @Override
    public Operador inserir(Operador operador) {
        Connection conn = conexao.novaConexao();
        PreparedStatement sql;

        try {
            sql = conn.prepareStatement("INSERT INTO operador "
                    + "(ope_usr_id, ope_cargo) VALUES(?, ?)");

            sql.setInt(1, operador.getIdUsuario());
            sql.setString(2, operador.getCargo());
            sql.execute();
            sql.close();
        } catch (SQLException ex) {
            Logger.getLogger(OperadorDAOMariaDB.class.getName()).log(Level.INFO, null, ex);
        }

        Operador operadorEncontrado = encontrarPorId(operador.getIdUsuario());
        operador.setIdUsuario(operadorEncontrado.getIdUsuario());
        operador.setCargo(operadorEncontrado.getCargo());
       
        return operador;
    }

    @Override
    public Operador encontrarPorId(Integer id) {
        Connection con = conexao.novaConexao();
        PreparedStatement sql;
        ResultSet rs = null;
        Operador operador = null;
        
        try {
            sql = con.prepareStatement("SELECT ope_usr_id, ope_cargo "
                    + "FROM operador where ope_usr_id = ?");
            sql.setInt(1, id);
            rs = sql.executeQuery();
            sql.close();
            
            if (rs.next()){                
                operador = new Operador();
                operador.setIdUsuario(rs.getInt("ope_usr_id"));
                operador.setCargo(rs.getString("ope_cargo"));
            } 
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAOMariaDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return operador;
    }

    @Override
    public List<Operador> encontrarTudo() {
        Connection con = conexao.novaConexao();
        PreparedStatement sql;
        ResultSet rs = null;
        Operador operador = null;
        List<Operador> operadores = null;
        try {
            sql = con.prepareStatement("SELECT ope_usr_id, ope_cargo "
                    + "FROM operador");
            rs = sql.executeQuery();
            sql.close();
            
            if(rs != null){
                operadores = new ArrayList<Operador>();
                while (rs.next()){               
                    operador = new Operador();
                    operador.setIdUsuario(rs.getInt("ope_usr_id"));
                    operador.setCargo(rs.getString("ope_cargo"));
                    operadores.add(operador);
                }
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAOMariaDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return operadores;
    }

    @Override
    public boolean deletar(Integer idUsuario) {
    
        Connection con = conexao.novaConexao();
        PreparedStatement sql;
        boolean resultado = false;
        
        try {
            sql = con.prepareStatement("DELETE FROM operador WHERE ope_usr_id = ?");
            sql.setInt(1, idUsuario);
            resultado = sql.execute();
            sql.close();
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAOMariaDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultado;
    }
}
