package com.fatec.mauricioselect.core.servico;

import com.fatec.mauricioselect.api.dao.CarroDAO;
import com.fatec.mauricioselect.api.dao.CategoriaDAO;
import com.fatec.mauricioselect.api.dao.ReservaDAO;
import com.fatec.mauricioselect.api.dao.UsuarioDAO;
import com.fatec.mauricioselect.api.modelo.Carro;
import com.fatec.mauricioselect.api.modelo.Categoria;
import com.fatec.mauricioselect.api.modelo.Reserva;
import com.fatec.mauricioselect.api.modelo.Usuario;
import com.fatec.mauricioselect.api.modelo.categorico.ReservaCategorica;
import com.fatec.mauricioselect.api.servico.CategoriaServico;
import com.fatec.mauricioselect.api.servico.ReservaServico;
import com.fatec.mauricioselect.core.dao.CarroDAOMariaDB;
import com.fatec.mauricioselect.core.dao.CategoriaDAOMariaDB;
import com.fatec.mauricioselect.core.dao.ReservaDAOMariaDB;
import com.fatec.mauricioselect.core.dao.UsuarioDAOMariaDB;
import com.fatec.mauricioselect.web.servlet.utilitarios.ManipuladorDeData;
import com.fatec.mauricioselect.web.servlet.utilitarios.ValidadorDeCampos;
import java.lang.reflect.InvocationTargetException;
import java.sql.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ImplReservaServico implements ReservaServico {

    private ReservaDAO reservaDAO;

    public ImplReservaServico() {
        reservaDAO = new ReservaDAOMariaDB();
    }

    @Override
    public boolean deleta(Reserva reserva) {
        return reservaDAO.deletar(reserva.getIdReserva());
    }

    @Override
    public ReservaCategorica inserir(Reserva reserva) {
        reserva.setIdReserva(0);
        // Validando os dados
        validaAReserva(reserva);

        Reserva reservaInserida = reservaDAO.inserir(reserva);

        if (reservaInserida != null) {
            return criaReservaCategorica(reservaInserida);
        }
        return null;
    }

    @Override
    public List<ReservaCategorica> encontrarPorIdUsuario(Integer idUsuario) {

        if (idUsuario == null) {
            throw new RuntimeException("ID de usuário inválido");
        }

        return criaListaDeReservaCategorica(reservaDAO.encontrarPorIdUsuario(idUsuario));
    }

    @Override
    public ReservaCategorica encontrarPorIdReserva(Integer idReserva) {

        if (idReserva == null) {
            throw new RuntimeException("ID da reserva é inválido!");
        }

        return criaReservaCategorica(reservaDAO.encontrarPorId(idReserva));
    }

    @Override
    public ReservaCategorica atualizaReserva(Reserva reservaNova) {

        Reserva reserva = null;
        validaAReserva(reservaNova);

        if ("APROVADA".equals(reservaDAO.encontrarPorId(reservaNova.getIdReserva()).getStatus())) {
            throw new RuntimeException("Reservas aprovadas não podem ser modificadas!");
        }

        try {
            Reserva reservaAntiga = reservaDAO.encontrarPorId(reservaNova.getIdReserva());
            reserva = reservaDAO.atualizar(reservaAntiga, reservaNova);
        } catch (Exception ex) {
            Logger.getLogger(ImplReservaServico.class.getName()).log(Level.SEVERE, null, ex);
        }
        return criaReservaCategorica(reserva);
    }

    @Override
    public List<ReservaCategorica> encontrarTudo() {
        return criaListaDeReservaCategorica(reservaDAO.encontrarTudo());
    }

    @Override
    public List<ReservaCategorica> encontrarReservasDisponiveis() {

        List<Reserva> reservas = null;

        try {
            reservas = reservaDAO.encontrarReservasDisponiveis();
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName(), Level.SEVERE + "Erro ao buscar todos as reservas disponíveis: " + e.getMessage());
        }

        return criaListaDeReservaCategorica(reservas);
    }

    /**
     * Método para validar a reserva que está sendo inserida pelo usuário
     *
     * @param reserva
     */
    private void validaAReserva(Reserva reserva) {

        try {
            ValidadorDeCampos.validaCamposDoObjeto(reserva);
        } catch (InvocationTargetException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }

        if (reserva.getDiaInicio().after(reserva.getDiaFinal())) {
            throw new RuntimeException("O dia inicial deve vir antes do final!");
        }

        CategoriaServico categoriaServico = new ImplCategoriaServico();
        // Recuperando as informações da categoria
        Categoria categoria = categoriaServico.encontrarPorId(reserva.getIdCategoria());

        // Recuperando o número de dias entre as datas
        Date diaInicial = reserva.getDiaInicio();
        Date diaFinal = reserva.getDiaFinal();
        Integer distancia = ManipuladorDeData.distanciaEntreAsDatas(diaInicial, diaFinal);

        // Verifica a consistência do valor total
        double valorTotalCalculado = (distancia * 24) * categoria.getValorPorHora();
        if (valorTotalCalculado != reserva.getValorTotal()) {
            throw new RuntimeException("Inconsistência no valor total");
        }
    }

    /**
     * Método para criar as reservas categoricas
     *
     * @param reserva
     * @return
     */
    private ReservaCategorica criaReservaCategorica(Reserva reserva) {

        if (Objects.isNull(reserva)) {
            return null;
        }

        UsuarioDAO usuarioDAO = new UsuarioDAOMariaDB();
        CarroDAO carroDAO = new CarroDAOMariaDB();
        CategoriaDAO categoriaDAO = new CategoriaDAOMariaDB();

        List<Usuario> usuarios = usuarioDAO.encontrarTudo();
        List<Carro> carros = carroDAO.encontrarTudo();
        List<Categoria> categorias = categoriaDAO.encontrarTudo();

        Optional<Carro> carroOpcional = carros.stream().filter(c
                -> c.getIdCarro()
                        .equals(reserva.getIdCarro()))
                .findFirst();

        Optional<Usuario> usuarioOpcional = usuarios.stream().filter(u
                -> u.getIdUsuario()
                        .equals(reserva.getIdUsuario()))
                .findFirst();

        Optional<Categoria> categoriaOpcional = categorias.stream().filter(c
                -> c.getIdCategoria()
                        .equals(reserva.getIdCategoria()))
                .findFirst();

        if (carroOpcional.isPresent() && usuarioOpcional.isPresent()
                && categoriaOpcional.isPresent()) {
            return new ReservaCategorica(usuarioOpcional.get(),
                    categoriaOpcional.get(),
                    carroOpcional.get(), reserva.getIdReserva(),
                    reserva.getDiaInicio(), reserva.getDiaFinal(),
                    reserva.getStatus(), reserva.getValorTotal());
        } else {
            throw new RuntimeException("Houve um problema ao tentar recuperar "
                    + "as dependências desta reserva");
        }
    }

    /**
     * Método para gerar uma lista de reservas em reservas categóricas
     * (Mapeadas)
     *
     * @param reservas
     * @return
     */
    private List<ReservaCategorica> criaListaDeReservaCategorica(List<Reserva> reservas) {
        List<ReservaCategorica> reservaCategoricas = new LinkedList<>();

        reservas.forEach((reserva) -> {
            reservaCategoricas.add(criaReservaCategorica(reserva));
        });
        return reservaCategoricas;
    }
}
