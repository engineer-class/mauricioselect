package com.fatec.mauricioselect.core.dao;

import com.fatec.mauricioselect.api.dao.CarroDAO;
import com.fatec.mauricioselect.api.modelo.Carro;
import com.fatec.mauricioselect.api.modelo.CarroBuilder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CarroDAOMariaDB implements CarroDAO {

    private Conexao conexao;
    
    public CarroDAOMariaDB() {
        conexao = new Conexao();
    }
    
    @Override
    public Carro inserir(Carro carro) {

        Connection conn = conexao.novaConexao();
        PreparedStatement sql = null;
        
        try {
            sql = conn.prepareStatement("INSERT INTO carro "
                    + "(car_cat_id, car_fab_id, car_ano, car_conservacao, "
                    + "car_cor, car_placa, car_quilometragem, car_tanque, car_modelo, car_quebrado) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            sql.setInt(1, carro.getIdCategoria());
            sql.setInt(2, carro.getIdFabricante());
            sql.setInt(3, carro.getAno());
            sql.setString(4, carro.getEstadoConserva());
            sql.setString(5, carro.getCor());
            sql.setString(6, carro.getPlaca());
            sql.setDouble(7, carro.getQuilometragem());
            sql.setInt(8, carro.getTanque());
            sql.setString(9, carro.getModelo());
            sql.setInt(10, carro.getEstaQuebrado());
            sql.execute();
            sql.close();
        } catch (SQLException ex) {
            Logger.getLogger(CarroDAOMariaDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return encontrarPorPlaca(carro.getPlaca());
    }

    @Override
    public Carro encontrarPorId(Integer idCarro) {
        Connection conn = conexao.novaConexao();
        PreparedStatement sql = null;
        ResultSet rs = null;
        Carro carro = null;
        
        try {
            sql = conn.prepareStatement("SELECT car_id, car_cat_id, car_fab_id, "
                    + "car_ano, car_conservacao, car_cor, car_placa, "
                    + "car_quilometragem, car_tanque, car_modelo, car_quebrado FROM carro where car_id = ?");
            sql.setInt(1, idCarro);
            rs = sql.executeQuery();
            
            if (rs.next()) {
                CarroBuilder carroBuilder = CarroBuilder.getBuilder();
                carro = carroBuilder.adicionaIDCarro(rs.getInt("car_id"))
                                        .adicionaCategoria(rs.getInt("car_cat_id"))
                                        .adicionaIDFabricante(rs.getInt("car_fab_id"))
                                        .adicionaAno(rs.getInt("car_ano"))
                                        .adicionaEstadoConserva(rs.getString("car_conservacao"))
                                        .adicionaCor(rs.getString("car_cor"))
                                        .adicionaPlaca(rs.getString("car_placa"))
                                        .adicionaModelo(rs.getString("car_modelo"))
                                        .adicionaQuilometragem(rs.getDouble("car_quilometragem"))
                                        .adicionaTanque(rs.getInt("car_tanque"))
                                        .adicionaEstadoDoCarro(rs.getInt("car_quebrado")).build();                
            }
        } catch (SQLException ex) {
            Logger.getLogger(CarroDAOMariaDB.class.getName()).log(Level.INFO, null, ex);
        }
        return carro;
    }

    @Override
    public List<Carro> encontrarPorAno(Integer ano) {
        List<Carro> carros = new LinkedList<>();
        Connection conn = conexao.novaConexao();
        ResultSet rs = null;
        PreparedStatement sql = null;
        
        try {
            sql = conn.prepareStatement("SELECT car_id, car_cat_id, car_fab_id, "
                    + "car_ano, car_conservacao, car_cor, car_placa, "
                    + "car_quilometragem, car_tanque, car_modelo, car_quebrado FROM carro where car_ano = ?");
            sql.setInt(1, ano);
            rs = sql.executeQuery();

            while (rs.next()) {
                CarroBuilder carroBuilder = CarroBuilder.getBuilder();
                Carro carro = carroBuilder.adicionaIDCarro(rs.getInt("car_id"))
                                        .adicionaCategoria(rs.getInt("car_cat_id"))
                                        .adicionaIDFabricante(rs.getInt("car_fab_id"))
                                        .adicionaAno(rs.getInt("car_ano"))
                                        .adicionaModelo(rs.getString("car_modelo"))
                                        .adicionaEstadoConserva(rs.getString("car_conservacao"))
                                        .adicionaCor(rs.getString("car_cor"))
                                        .adicionaPlaca(rs.getString("car_placa"))
                                        .adicionaQuilometragem(rs.getDouble("car_quilometragem"))
                                        .adicionaTanque(rs.getInt("car_tanque"))
                                        .adicionaEstadoDoCarro(rs.getInt("car_quebrado")).build();
                carros.add(carro);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(CarroDAOMariaDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return carros;
    }

    @Override
    public List<Carro> encontrarPorFabricante(Integer idFabricante) {
        List<Carro> carros = new LinkedList<>();
        Connection conn = conexao.novaConexao();
        ResultSet rs = null;
        PreparedStatement sql = null;
        
        try {
            sql = conn.prepareStatement("SELECT car_id, car_cat_id, car_fab_id, "
                    + "car_ano, car_conservacao, car_cor, car_placa, "
                    + "car_quilometragem, car_tanque , car_modelo, car_quebrado FROM carro where car_fab_id = ?");
            sql.setInt(1, idFabricante);
            rs = sql.executeQuery();

            while (rs.next()) {
                CarroBuilder carroBuilder = CarroBuilder.getBuilder();
                Carro carro = carroBuilder.adicionaIDCarro(rs.getInt("car_id"))
                                        .adicionaCategoria(rs.getInt("car_cat_id"))
                                        .adicionaIDFabricante(rs.getInt("car_fab_id"))
                                        .adicionaAno(rs.getInt("car_ano"))
                                        .adicionaModelo(rs.getString("car_modelo"))
                                        .adicionaEstadoConserva(rs.getString("car_conservacao"))
                                        .adicionaCor(rs.getString("car_cor"))
                                        .adicionaPlaca(rs.getString("car_placa"))
                                        .adicionaQuilometragem(rs.getDouble("car_quilometragem"))
                                        .adicionaTanque(rs.getInt("car_tanque"))
                                        .adicionaEstadoDoCarro(rs.getInt("car_quebrado")).build();
                carros.add(carro);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(CarroDAOMariaDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return carros;
    }

    @Override
    public List<Carro> encontrarPorCategoria(Integer idCategoria) {

        List<Carro> carros = new LinkedList<>();
        Connection conn = conexao.novaConexao();
        ResultSet rs = null;
        PreparedStatement sql = null;
        
        try {
            sql = conn.prepareStatement("SELECT car_id, car_cat_id, car_fab_id, "
                    + "car_ano, car_conservacao, car_cor, car_placa, "
                    + "car_quilometragem, car_tanque, car_modelo, car_quebrado FROM carro where car_cat_id = ?");
            sql.setInt(1, idCategoria);
            rs = sql.executeQuery();

            while (rs.next()) {
                CarroBuilder carroBuilder = CarroBuilder.getBuilder();
                Carro carro = carroBuilder.adicionaIDCarro(rs.getInt("car_id"))
                                        .adicionaCategoria(rs.getInt("car_cat_id"))
                                        .adicionaIDFabricante(rs.getInt("car_fab_id"))
                                        .adicionaAno(rs.getInt("car_ano"))
                                        .adicionaModelo(rs.getString("car_modelo"))
                                        .adicionaEstadoConserva(rs.getString("car_conservacao"))
                                        .adicionaCor(rs.getString("car_cor"))
                                        .adicionaPlaca(rs.getString("car_placa"))
                                        .adicionaQuilometragem(rs.getDouble("car_quilometragem"))
                                        .adicionaTanque(rs.getInt("car_tanque"))
                                        .adicionaEstadoDoCarro(rs.getInt("car_quebrado")).build();
                carros.add(carro);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(CarroDAOMariaDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return carros;
    }

    @Override
    public List<Carro> encontrarTudo() {
        List<Carro> carros = new LinkedList<>();
        Connection conn = conexao.novaConexao();
        ResultSet rs = null;
        PreparedStatement sql = null;
        
        try {
            sql = conn.prepareStatement("SELECT car_id, car_cat_id, car_fab_id, "
                    + "car_ano, car_conservacao, car_cor, car_placa, "
                    + "car_quilometragem, car_tanque, car_modelo, car_quebrado FROM carro");
            rs = sql.executeQuery();

            while (rs.next()) {
                CarroBuilder carroBuilder = CarroBuilder.getBuilder();
                Carro carro = carroBuilder.adicionaIDCarro(rs.getInt("car_id"))
                                        .adicionaCategoria(rs.getInt("car_cat_id"))
                                        .adicionaIDFabricante(rs.getInt("car_fab_id"))
                                        .adicionaAno(rs.getInt("car_ano"))
                                        .adicionaModelo(rs.getString("car_modelo"))
                                        .adicionaEstadoConserva(rs.getString("car_conservacao"))
                                        .adicionaCor(rs.getString("car_cor"))
                                        .adicionaPlaca(rs.getString("car_placa"))
                                        .adicionaQuilometragem(rs.getDouble("car_quilometragem"))
                                        .adicionaTanque(rs.getInt("car_tanque"))
                                        .adicionaEstadoDoCarro(rs.getInt("car_quebrado")).build();
                carros.add(carro);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(CarroDAOMariaDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return carros;
    }

    @Override
    public Carro atualizar(Carro carroAntigo, Carro carroAtual) {
        Connection conn = conexao.novaConexao();
        PreparedStatement sql;
        Carro carroAtualizado = null;
        try {   
            sql = conn.prepareStatement("UPDATE carro SET car_ano = ?, "
                    + "car_conservacao = ?, car_cor = ?,"
                    + " car_placa = ?, car_quilometragem = ?, "
                    + "car_tanque = ?, car_modelo = ?, car_quebrado = ? WHERE car_id = ?");
            sql.setInt(1, carroAtual.getAno());
            sql.setString(2, carroAtual.getEstadoConserva());
            sql.setString(3, carroAtual.getCor());
            sql.setString(4, carroAtual.getPlaca());
            sql.setDouble(5, carroAtual.getQuilometragem());
            sql.setInt(6, carroAtual.getTanque());
            sql.setString(7, carroAtual.getModelo());
            sql.setInt(8, carroAtual.getEstaQuebrado());
            sql.setInt(9, carroAntigo.getIdCarro());
            sql.execute();
            carroAtualizado = encontrarPorId(carroAntigo.getIdCarro());
        } catch (SQLException ex) {
            Logger.getLogger(CarroDAOMariaDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return carroAtualizado;
    }

    @Override
    public boolean deletar(Integer idCarro) {
        Connection conn = conexao.novaConexao();
        boolean resultado = false;
        PreparedStatement sql = null;
        
        try {
            sql = conn.prepareStatement("DELETE FROM carro WHERE car_id = ?");
            sql.setInt(1, idCarro);
            resultado = !sql.execute();

        } catch (SQLException ex) {
            Logger.getLogger(CarroDAOMariaDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultado;
    }    

    @Override
    public Carro encontrarPorPlaca(String placa) {
        Connection conn = conexao.novaConexao();
        PreparedStatement sql = null;
        ResultSet rs = null;
        Carro carro = null;
        
        try {
            sql = conn.prepareStatement("SELECT car_id, car_cat_id, car_fab_id, "
                    + "car_ano, car_conservacao, car_cor, car_placa, "
                    + "car_quilometragem, car_tanque, car_modelo, car_quebrado FROM "
                    + "carro where car_placa = ?");
            sql.setString(1, placa);
            rs = sql.executeQuery();
            
            if (rs.next()) {
                CarroBuilder carroBuilder = CarroBuilder.getBuilder();
                carro = carroBuilder.adicionaIDCarro(rs.getInt("car_id"))
                                        .adicionaCategoria(rs.getInt("car_cat_id"))
                                        .adicionaIDFabricante(rs.getInt("car_fab_id"))
                                        .adicionaAno(rs.getInt("car_ano"))
                                        .adicionaModelo(rs.getString("car_modelo"))
                                        .adicionaEstadoConserva(rs.getString("car_conservacao"))
                                        .adicionaCor(rs.getString("car_cor"))
                                        .adicionaPlaca(rs.getString("car_placa"))
                                        .adicionaQuilometragem(rs.getDouble("car_quilometragem"))
                                        .adicionaTanque(rs.getInt("car_tanque"))
                                        .adicionaEstadoDoCarro(rs.getInt("car_quebrado")).build();                
            }
        } catch (SQLException ex) {
            Logger.getLogger(CarroDAOMariaDB.class.getName()).log(Level.INFO, null, ex);
        }
        return carro;
    }

    @Override
    public List<Carro> encontrarPorModelo(String modelo) {
        List<Carro> carros = new LinkedList<>();
        Connection conn = conexao.novaConexao();
        ResultSet rs = null;
        PreparedStatement sql = null;
        
        try {
            sql = conn.prepareStatement("SELECT car_id, car_cat_id, car_fab_id, "
                    + "car_ano, car_conservacao, car_cor, car_placa, "
                    + "car_quilometragem, car_tanque, car_modelo, car_quebrado FROM carro WHERE car_modelo = ?");
            sql.setString(1, modelo);
            rs = sql.executeQuery();

            while (rs.next()) {
                CarroBuilder carroBuilder = CarroBuilder.getBuilder();
                Carro carro = carroBuilder.adicionaIDCarro(rs.getInt("car_id"))
                                        .adicionaCategoria(rs.getInt("car_cat_id"))
                                        .adicionaIDFabricante(rs.getInt("car_fab_id"))
                                        .adicionaModelo(rs.getString("car_modelo"))
                                        .adicionaAno(rs.getInt("car_ano"))
                                        .adicionaEstadoConserva(rs.getString("car_conservacao"))
                                        .adicionaCor(rs.getString("car_cor"))
                                        .adicionaPlaca(rs.getString("car_placa"))
                                        .adicionaQuilometragem(rs.getDouble("car_quilometragem"))
                                        .adicionaTanque(rs.getInt("car_tanque"))
                                        .adicionaEstadoDoCarro(rs.getInt("car_quebrado")).build();
                carros.add(carro);
            }     
        } catch (SQLException ex) {
            Logger.getLogger(CarroDAOMariaDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return carros;
    }

}
