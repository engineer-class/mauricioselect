package com.fatec.mauricioselect.core.servico;

import com.fatec.mauricioselect.api.modelo.Operador;
import com.fatec.mauricioselect.api.modelo.OperadorBuilder;
import com.fatec.mauricioselect.api.modelo.Usuario;
import com.fatec.mauricioselect.api.servico.OperadorServico;
import com.fatec.mauricioselect.core.dao.OperadorDAOMariaDB;
import com.fatec.mauricioselect.core.dao.UsuarioDAOMariaDB;

public class ImplOperadorServico implements OperadorServico {

    private UsuarioDAOMariaDB usuarioDAO;
    private OperadorDAOMariaDB operadorDAO;
    
    @Override
    public Operador inserir(Operador operador) {
        OperadorBuilder operadorBuilder = OperadorBuilder.getBuilder();
        operadorDAO = new OperadorDAOMariaDB();
        usuarioDAO = new UsuarioDAOMariaDB();
        Operador operadorCadastrado = null;
                
        Usuario usuario = usuarioDAO.encontrarPorApelido(operador.getApelido()); 
                
        if (usuario != null) {
            operadorCadastrado = encontrarPorIdUsuario(usuario.getIdUsuario());
                        
            operador = operadorBuilder.consomeObjetoUsuario(usuario)
                            .adicionaCargo(operador.getCargo()).build();
        } else {
            return null;
        }
        
        if (null == operadorCadastrado) {
            operadorDAO.inserir(operador);
        } 
                
        operadorCadastrado = encontrarPorIdUsuario(usuario.getIdUsuario());
        // Caso o usuário seja encontrado, um objeto completo, unindo o 
        // usuário e o operador é criado, utilizando o builder.
        if (operadorCadastrado != null) {
            operadorCadastrado = operadorBuilder.consomeObjetoUsuario(usuario)
                                                .adicionaCargo(operadorCadastrado.getCargo()).build();
        }
        return operadorCadastrado;
    }

    @Override
    public Operador encontrarPorIdUsuario(Integer idUsuario) { 
        OperadorBuilder operadorBuilder = OperadorBuilder.getBuilder();
        operadorDAO = new OperadorDAOMariaDB();
        usuarioDAO = new UsuarioDAOMariaDB();
        
        Usuario usuario = usuarioDAO.encontrarPorId(idUsuario);
        
        if (usuario != null) {
            operadorBuilder.consomeObjetoUsuario(usuario);
            Operador operador = operadorDAO.encontrarPorId(idUsuario);
            
            if (operador != null) {
                operadorBuilder.adicionaCargo(operador.getCargo());
            } else {
                return null;
            }
        } else {
            return null;
        }
        
       return operadorBuilder.build();
    }
    
    @Override
    public Operador encontrarPorApelido(String apelido) {
        OperadorBuilder operadorBuilder = OperadorBuilder.getBuilder();
        operadorDAO = new OperadorDAOMariaDB();
        usuarioDAO = new UsuarioDAOMariaDB();
        
        Usuario usuario = usuarioDAO.encontrarPorApelido(apelido); 
        
        if (usuario != null) {
            operadorBuilder.consomeObjetoUsuario(usuario);
            Operador operador = operadorDAO.encontrarPorId(usuario.getIdUsuario());
            
            if (operador != null) {
                operadorBuilder.adicionaCargo(operador.getCargo());
            } else {
               return null;
            }
        } else {
            return null;
        }        
        return operadorBuilder.build();
    }

    @Override
    public boolean deleta(Operador operador) {
        operadorDAO = new OperadorDAOMariaDB();
        return operadorDAO.deletar(operador.getIdUsuario()); 
    };
    
    @Override
    public boolean usuarioEhOperador(String nomeUsuario) {
        operadorDAO = new OperadorDAOMariaDB();
        usuarioDAO = new UsuarioDAOMariaDB();
        
        Integer idUsuario = usuarioDAO.encontrarPorApelido(nomeUsuario)
                                        .getIdUsuario();
        
        return operadorDAO.encontrarPorId(idUsuario) != null;
    }
}
