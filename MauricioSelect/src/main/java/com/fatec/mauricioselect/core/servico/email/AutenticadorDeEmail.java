package com.fatec.mauricioselect.core.servico.email;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

/**
 * Classe para realizar autenticação no servidor de email
 * @author felipe
 */
public class AutenticadorDeEmail extends Authenticator {
    
    private String usuario;
    private String senha;
    
    public AutenticadorDeEmail(String usuario, String senha) {
        this.usuario = usuario;
        this.senha = senha;
    }

    @Override
    protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(usuario, senha);
    }   
}
