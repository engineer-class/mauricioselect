/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fatec.mauricioselect.core.dao.utilitarios;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;


public class LoadProperties {

    /**
     * 
     * Este metodo recupera valores para que a configuração do banco seja 
     * feita corretamente
     * 
     */
    public static Properties loadDBProperties(){
        Properties prop = null;
        try{
            prop = new Properties();
            prop.load(new FileInputStream("src/main/resources/mariadb/mariadb.properties"));
        } catch(IOException e){
            Logger.getLogger(LoadProperties.class.getName()).log(Level.SEVERE,"Erro ao ler arquivo de propriedades: ",e);
        }
        return prop;
    }
}
