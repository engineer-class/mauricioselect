package com.fatec.mauricioselect.core.servico;

import com.fatec.mauricioselect.api.dao.FabricanteDAO;
import com.fatec.mauricioselect.api.modelo.Fabricante;
import com.fatec.mauricioselect.api.servico.FabricanteServico;
import com.fatec.mauricioselect.core.dao.FabricanteDAOMariaDB;
import java.util.List;

public class ImplFabricanteServico implements FabricanteServico {

    private FabricanteDAO fabricanteDAO;
    
    public ImplFabricanteServico() {
        fabricanteDAO = new FabricanteDAOMariaDB();
    }
    
    @Override
    public Fabricante inserir(Fabricante fabricante) {
        validaDadosFabricante(fabricante);
        
        return fabricanteDAO.inserir(fabricante);
    }

    @Override
    public Fabricante encontrarPorId(Integer id) {
        return fabricanteDAO.encontrarPorId(id);
    }

    @Override
    public Fabricante encontrarPorNome(String nome) {
        if (nome == null || nome.trim().isEmpty()) {
            throw new RuntimeException("Nome inválido!");
        }
        return fabricanteDAO.encontrarPorNome(nome);
    }

    @Override
    public List<Fabricante> encontrarTudo() {
        return fabricanteDAO.encontrarTudo();
    }

    @Override
    public Fabricante atualizar(Fabricante fabricanteNovo) {
        validaDadosFabricante(fabricanteNovo);
                
        Fabricante fabricanteAntigo = fabricanteDAO
                                .encontrarPorId(fabricanteNovo.getIdFabricante());
        return fabricanteDAO.atualizar(fabricanteAntigo, fabricanteNovo);
    }

    @Override
    public boolean deletar(Integer idFabricante) {   
        if (idFabricante == null) {
            throw new RuntimeException("ID de fabricante inválido!"); 
        }
        return fabricanteDAO.deletar(idFabricante);
    }
    
    /**
     * Método para validar os dados presentes no objeto de fabricante
     * @param fabricante 
     */
    private void validaDadosFabricante(Fabricante fabricante) {
        if (fabricante.getNome().trim().isEmpty()) {
            throw new RuntimeException("Há inconsistências nos dados do fabricante");
        }
    }
}
