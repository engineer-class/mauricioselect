package com.fatec.mauricioselect.core.servico;

import br.com.caelum.stella.ValidationMessage;
import br.com.caelum.stella.validation.CPFValidator;
import com.fatec.mauricioselect.api.dao.UsuarioDAO;
import com.fatec.mauricioselect.api.modelo.Usuario;
import com.fatec.mauricioselect.api.servico.UsuarioServico;
import com.fatec.mauricioselect.core.dao.UsuarioDAOMariaDB;
import com.fatec.mauricioselect.web.servlet.utilitarios.ValidadorDeCampos;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ImplUsuarioServico implements UsuarioServico {

    private UsuarioDAO usuarioDAO;

    public ImplUsuarioServico() {
        usuarioDAO = new UsuarioDAOMariaDB();
    }
    
    @Override
    public Usuario inserir(Usuario usuario) {
        // Validando dados do usuarío
        validaDadosUsuario(usuario);
        
        // Buscando para verificação de existência
        Usuario usuarioEncontrado = encontrarPorApelido(usuario.getApelido());
               
        // Caso os usuários tenham características iguais
        // o processo não pode ser realizado
        if (usuarioEncontrado == null) {
            return usuarioDAO.inserir(usuario);
        }  
        throw new RuntimeException("Usuário já cadastrado");
    }

    @Override
    public Usuario encontrarPorApelido(String apelidoUsuario) {
        Usuario usuario = usuarioDAO.encontrarPorApelido(apelidoUsuario);
        
        return usuario;
    }
    
    @Override
    public boolean validaUsuario(String apelidoUsuario, String senha) {
        Usuario usuario = usuarioDAO.encontrarPorApelido(apelidoUsuario);
                   
        return usuario != null && senha != null && 
                apelidoUsuario.equals(usuario.getApelido())
                && senha.equals(usuario.getSenha());
    }
    
    @Override
    public boolean deleta(Usuario usuario) {
        return usuarioDAO.deletar(usuario.getIdUsuario());
    }

    @Override
    public Usuario atualizaUsuario(Usuario usuarioNovo) {        
        // Na página do usuário o nome é mostrado com aspas (") então uma rápida
        // remoção é feita. As aspas são mantidas para indicar o APELIDO.
        usuarioNovo.setApelido(usuarioNovo.getApelido().replaceAll("\"", ""));
        Usuario usuarioAntigo = encontrarPorApelido(usuarioNovo.getApelido());
        
        // Verificação da existência de senha para atualizar
        // Caso não exista, utiliza senha antiga
        if (usuarioNovo.getSenha() == null) {
            usuarioNovo.setSenha(usuarioAntigo.getSenha());
        }
        
        return usuarioDAO.atualizar(usuarioAntigo, usuarioNovo);
    }

    @Override
    public Usuario encontrarUsuarioPorId(Integer idUsuario) {
        Usuario usuario = null;
        try{
            usuario = usuarioDAO.encontrarPorId(idUsuario);
        }catch(Exception e){
            Logger.getLogger(this.getClass().getName(), "Não foi possível "
                    + "encontrar o usuário por id: " + e.getMessage());
        }
        return usuario;
    }
    
    @Override
    public Usuario encontrarPorEmail(String email) {
        if (email != null) {
            if (!email.trim().isEmpty()) {
                return usuarioDAO.encontrarPorEmail(email);
            }
        }
        throw new RuntimeException("Email inválido!");
    }
    
    /**
     * Método para a validação dos dados do usuário
     * @param usuario 
     */
    private void validaDadosUsuario(Usuario usuario) {
     
        // Validador de CPF criado pela Caellum :)
        CPFValidator validadorDeCPF = new CPFValidator();
        List<ValidationMessage> errosCPF = validadorDeCPF
                                    .invalidMessagesFor(usuario.getCpf());
        // Valida o CPF
        if (!errosCPF.isEmpty()) {
            throw new RuntimeException("O CPF está com problemas na formatação!");
        }
        
        try {
            ValidadorDeCampos.validaCamposDoObjeto(usuario);
        } catch (InvocationTargetException ex) {
            Logger.getLogger(ImplUsuarioServico.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Usuario> encontrarTodosOsUsuarios() {
        return usuarioDAO.encontrarTudo();
    }
}
