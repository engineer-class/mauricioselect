package com.fatec.mauricioselect.core.servico;

import com.fatec.mauricioselect.api.dao.AluguelDAO;
import com.fatec.mauricioselect.api.dao.CarroDAO;
import com.fatec.mauricioselect.api.dao.CategoriaDAO;
import com.fatec.mauricioselect.api.dao.FabricanteDAO;
import com.fatec.mauricioselect.api.dao.ReservaDAO;
import com.fatec.mauricioselect.api.modelo.Aluguel;
import com.fatec.mauricioselect.api.modelo.Carro;
import com.fatec.mauricioselect.api.modelo.Categoria;
import com.fatec.mauricioselect.api.modelo.Fabricante;
import com.fatec.mauricioselect.api.modelo.Reserva;
import com.fatec.mauricioselect.api.modelo.categorico.CarroCategorico;
import com.fatec.mauricioselect.api.modelo.excecao.ExcecaoDeDadosInvalidos;
import com.fatec.mauricioselect.api.servico.CarroServico;
import com.fatec.mauricioselect.core.dao.AluguelDAOMariaDB;
import com.fatec.mauricioselect.core.dao.CarroDAOMariaDB;
import com.fatec.mauricioselect.core.dao.CategoriaDAOMariaDB;
import com.fatec.mauricioselect.core.dao.FabricanteDAOMariaDB;
import com.fatec.mauricioselect.core.dao.ReservaDAOMariaDB;
import com.fatec.mauricioselect.web.servlet.utilitarios.ValidadorDeCampos;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ImplCarroServico implements CarroServico {

    private ReservaDAO reservaDAO;
    private CarroDAO carroDAO;
    private CategoriaDAO categoriaDAO;
    private FabricanteDAO fabricanteDAO;
    private AluguelDAO aluguelDAO;

    public ImplCarroServico() {
        this.aluguelDAO = new AluguelDAOMariaDB();
        this.reservaDAO = new ReservaDAOMariaDB();
        this.carroDAO = new CarroDAOMariaDB();
        this.categoriaDAO = new CategoriaDAOMariaDB();
        this.fabricanteDAO = new FabricanteDAOMariaDB();
    }

    @Override
    public boolean deleta(Carro carro) {

        boolean resultado = false;

        if (carro.getIdCarro() == null) {
            throw new ExcecaoDeDadosInvalidos("O ID do carro é inválido!");
        } else {
            resultado = carroDAO.deletar(carro.getIdCarro());
        }
        return resultado;
    }

    @Override
    public CarroCategorico inserir(Carro carro) {
        try {
            validaDadosDoCarro(carro);
        } catch (Exception ex) {
            Logger.getLogger(ImplCarroServico.class.getName()).log(Level.SEVERE, null, ex);
            throw new ExcecaoDeDadosInvalidos("Os campos do carro não estão preenchidos corretamente");
        }
        carro.setEstaQuebrado(0);
        Carro carroInserido = carroDAO.inserir(carro);
        CarroCategorico carroCategorico = criaCarroCategorico(carroInserido);

        return carroCategorico;

    }

    @Override
    public CarroCategorico encontrarPorId(Integer idCarro) {

        Carro carro = carroDAO.encontrarPorId(idCarro);
        CarroCategorico carroCategorico = null;
        if (Objects.nonNull(carro)) {
            carroCategorico = criaCarroCategorico(carro);
        }

        return carroCategorico;

    }

    @Override
    public List<CarroCategorico> encontrarPorCategoria(String nomeCategoria) {

        Categoria categoria = categoriaDAO.encontrarPorNome(nomeCategoria);
        List<CarroCategorico> carros = null;

        if (Objects.nonNull(categoria)) {
            List<Carro> carrosEncontrados = carroDAO.encontrarPorCategoria(categoria.getIdCategoria());

            if (Objects.nonNull(carrosEncontrados)) {
                carros = criarListaDeCarrosCategoricos(carrosEncontrados);
            }

        }

        return carros;
    }

    @Override
    public List<CarroCategorico> encontrarTodosOsCarros() {
        List<Carro> carrosEncontrados = carroDAO.encontrarTudo();
        List<CarroCategorico> carrosDisponiveis = encontrarCarrosDisponiveis();

        List<CarroCategorico> carros = null;
        if (carrosEncontrados != null) {
            carros = criarListaDeCarrosCategoricos(carrosEncontrados);
        }

        // Verifica se os carros possuem reservas pendentes ou alugueis alugados
        if (Objects.nonNull(carros)) {
            carros.stream().filter(carro -> carrosDisponiveis.stream().noneMatch(c -> c.getIdCarro().equals(carro.getIdCarro()))).forEach(carro -> carro.setStatus("INDISPONIVEL"));
        } else {
            carros.forEach(carro -> carro.setStatus("INDISPONIVEL"));
        }
        return carros;
    }

    @Override
    public CarroCategorico atualizar(Carro carroNovo) {

        try {
            validaDadosDoCarro(carroNovo);
        } catch (Exception ex) {
            Logger.getLogger(ImplCarroServico.class.getName()).log(Level.SEVERE, null, ex);
            throw new ExcecaoDeDadosInvalidos("Os campos do carro não estão preenchidos corretamente");
        }

        Carro carroAntigo = carroDAO.encontrarPorId(carroNovo.getIdCarro());
        Carro carroAtualizado = carroDAO.atualizar(carroAntigo, carroNovo);
        CarroCategorico carroCategorico = null;

        if (Objects.isNull(carroAtualizado)) {
            throw new RuntimeException("Erro ao atualizar carro!");
        } else {
            carroCategorico = criaCarroCategorico(carroAtualizado);
        }

        return carroCategorico;
    }

    @Override
    public List<CarroCategorico> encontrarPorModelo(String modelo) {

        List<Carro> carrosEncontrados = carroDAO.encontrarPorModelo(modelo);
        List<CarroCategorico> carros = null;

        if (carrosEncontrados != null) {
            carros = criarListaDeCarrosCategoricos(carrosEncontrados);
        }
        return carros;
    }

    @Override
    public List<CarroCategorico> encontrarCarrosDisponiveisPorCategoria(String categoria) {

        List<Aluguel> alugueis = aluguelDAO.encontrarTudo();
        List<Reserva> reservas = reservaDAO.encontrarTudo();
        List<Carro> carros = carroDAO.encontrarTudo();
        List<CarroCategorico> carrosCategoricos = null;
        Categoria categoriaNome = categoriaDAO.encontrarPorNome(categoria);

        // Valida se nenhuma reserva possui o id do carro atual e se ele não está quebrado
        List<Carro> carrosDisponiveis = carros.stream()
                .filter(carro -> reservas.stream()
                .noneMatch(reserva -> Objects
                .equals(reserva.getIdCarro(), carro.getIdCarro()) && "PENDENTE".equals(reserva.getStatus())) && carro.getEstaQuebrado() == 0
                && carro.getIdCategoria().equals(categoriaNome.getIdCategoria()))
                .filter(carro -> alugueis.stream().noneMatch(
                aluguel -> "ALUGADO".equals(aluguel.getStatus()) && Objects.equals(aluguel.getIdCarro(), carro.getIdCarro())
        )).collect(Collectors.toList());

        if (Objects.nonNull(carrosDisponiveis)) {
            carrosCategoricos = criarListaDeCarrosCategoricos(carrosDisponiveis);
        }

        return carrosCategoricos;
    }

    @Override
    public List<CarroCategorico> encontrarCarrosDisponiveis() {

        List<Reserva> reservas = reservaDAO.encontrarTudo();
        List<Aluguel> alugueis = aluguelDAO.encontrarTudo();
        List<Carro> carros = carroDAO.encontrarTudo();
        List<CarroCategorico> carrosCategoricos = null;

        // Valida se nenhuma reserva possui o id do carro atual e se ele não está quebrado
        List<Carro> carrosDisponiveis = carros.stream()
                .filter(carro -> reservas.stream()
                .noneMatch(reserva -> Objects
                .equals(reserva.getIdCarro(), carro.getIdCarro()) && "PENDENTE".equals(reserva.getStatus())) && carro.getEstaQuebrado() == 0)
                .filter(carro -> alugueis.stream().noneMatch(
                aluguel -> "ALUGADO".equals(aluguel.getStatus()) && Objects.equals(aluguel.getIdCarro(), carro.getIdCarro())
        )).collect(Collectors.toList());

        if (Objects.nonNull(carrosDisponiveis)) {
            carrosCategoricos = criarListaDeCarrosCategoricos(carrosDisponiveis);

        }
        return carrosCategoricos;
    }

    @Override
    public List<CarroCategorico> encontrarCarrosIndisponiveis() {

        List<CarroCategorico> carros = criarListaDeCarrosCategoricos(carroDAO.encontrarTudo());
        List<CarroCategorico> carrosCategoricos = null;
        List<CarroCategorico> carrosDisponiveis = encontrarCarrosDisponiveis();

        if (Objects.nonNull(carrosDisponiveis)) {

            carrosCategoricos = carros.stream()
                    .filter(carro -> carrosDisponiveis.stream()
                    .noneMatch(c -> carro.getIdCarro().equals(c.getIdCarro())))
                    .collect(Collectors.toList());
            carrosCategoricos.forEach(carro -> carro.setStatus("INDISPONIVEL"));
            
        } else {
            carrosCategoricos = carros;
            carrosCategoricos.forEach(carro -> carro.setStatus("INDISPONIVEL"));
        }

        return carrosCategoricos;
    }

    private List<CarroCategorico> criarListaDeCarrosCategoricos(List<Carro> carros) {

        List<CarroCategorico> carrosCategoricos = new ArrayList<>();

        carros.stream().forEach(carro -> {
            CarroCategorico carroCategorico = criaCarroCategorico(carro);
            carrosCategoricos.add(carroCategorico);
        });

        return carrosCategoricos;
    }

    private CarroCategorico criaCarroCategorico(Carro carro) {

        Categoria categoria = categoriaDAO.encontrarPorId(carro.getIdCategoria());
        Fabricante fabricante = fabricanteDAO.encontrarPorId(carro.getIdFabricante());

        CarroCategorico carroCategorico = new CarroCategorico(
                carro.getIdCarro(),
                categoria,
                fabricante,
                carro.getAno(),
                carro.getEstadoConserva(),
                carro.getCor(),
                carro.getPlaca(),
                carro.getQuilometragem(),
                carro.getTanque(),
                carro.getModelo(),
                carro.getEstaQuebrado());

        return carroCategorico;
    }

    /**
     * Método para validar os dados dos resultados
     *
     * @param carro
     */
    private void validaDadosDoCarro(Carro carro) throws InvocationTargetException {

        ValidadorDeCampos.validaCamposDoObjeto(carro);

        if (carro.getAno() < 1990
                || carro.getAno() > 9999
                || "Selecione".equals(carro.getEstaQuebrado())
                || carro.getTanque() < 0
                || carro.getTanque() > 100
                || carro.getQuilometragem() < 0) {
            throw new ExcecaoDeDadosInvalidos("Campo inválido!");
        }
    }
}
