package com.fatec.mauricioselect.core.dao;

import com.fatec.mauricioselect.api.dao.FabricanteDAO;
import com.fatec.mauricioselect.api.modelo.Categoria;
import com.fatec.mauricioselect.api.modelo.Fabricante;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author felipe
 */
public class FabricanteDAOMariaDB implements FabricanteDAO {

    private Conexao conexao;
    
    public FabricanteDAOMariaDB() {
        this.conexao = new Conexao();
    }
    
    @Override
    public Fabricante inserir(Fabricante fabricante) {
        
        PreparedStatement sql;
        Connection conn = conexao.novaConexao();
        
        try {
            sql = conn.prepareStatement("INSERT INTO fabricante (fab_nome) VALUES (?)");
            sql.setString(1, fabricante.getNome());
            sql.execute();
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAOMariaDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return encontrarPorNome(fabricante.getNome());
    }
    
    @Override
    public Fabricante encontrarPorNome(String nome) {
        PreparedStatement sql;
        ResultSet rs = null;
        Connection conn = conexao.novaConexao();
        Fabricante fabricante = null;
        
        try {
            sql = conn.prepareStatement("SELECT fab_id, fab_nome FROM fabricante WHERE "
                                        + "fab_nome = ?");
            sql.setString(1, nome.toUpperCase());
            rs = sql.executeQuery();
            
            if(rs.next()) {
                fabricante = new Fabricante(rs.getInt("fab_id"), 
                                                rs.getString("fab_nome"));
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAOMariaDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return fabricante;
    }
    
    @Override
    public Fabricante encontrarPorId(Integer id) {
        PreparedStatement sql;
        ResultSet rs = null;
        Connection conn = conexao.novaConexao();
        Fabricante fabricante = null;
        
        try {
            sql = conn.prepareStatement("SELECT fab_id, fab_nome FROM fabricante WHERE "
                                        + "fab_id = ?");
            sql.setInt(1, id);
            rs = sql.executeQuery();
            sql.close();
            
            if(rs.next()) {
                fabricante = new Fabricante(rs.getInt("fab_id"), 
                                                rs.getString("fab_nome"));
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAOMariaDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return fabricante;
    }

    @Override
    public List<Fabricante> encontrarTudo() {
        Connection conn = conexao.novaConexao();
        PreparedStatement sql;
        ResultSet rs = null;
        Categoria categoria = null;
        List<Fabricante> fabricates = new LinkedList<>();
        
        try {
            sql = conn.prepareStatement("SELECT fab_id, fab_nome FROM fabricante");
           
            rs = sql.executeQuery();
            sql.close();  
            
            while (rs.next()) {
                fabricates.add(new Fabricante(rs.getInt("fab_id"), 
                                                rs.getString("fab_nome")));
            }
        } catch (SQLException ex) {
            Logger.getLogger(CategoriaDAOMariaDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return fabricates;     
    }

    @Override
    public Fabricante atualizar(Fabricante fabricanteAntiga, Fabricante fabricanteAtual) {
        Connection conn = conexao.novaConexao();
        PreparedStatement sql;
        
        try {   
            sql = conn.prepareStatement("UPDATE fabricante "
                    + "SET fab_nome = ? WHERE fab_nome = ?");
            sql.setString(1, fabricanteAtual.getNome());
            sql.setString(2, fabricanteAntiga.getNome());
            sql.execute();
            
        } catch (SQLException ex) {
            Logger.getLogger(CategoriaDAOMariaDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return encontrarPorNome(fabricanteAtual.getNome());
    }

    @Override
    public boolean deletar(Integer idFabricante) {
        boolean resultado = false;
        Connection conn = conexao.novaConexao();
        PreparedStatement sql = null;
        
        try {
            sql = conn.prepareStatement("DELETE FROM fabricante WHERE fab_id = ?");
            sql.setInt(1, idFabricante);
            resultado = !sql.execute();
            
        } catch (SQLException ex) {
            Logger.getLogger(FabricanteDAOMariaDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultado;
    }    
}
