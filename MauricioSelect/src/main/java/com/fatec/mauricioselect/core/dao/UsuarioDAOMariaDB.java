package com.fatec.mauricioselect.core.dao;

import com.fatec.mauricioselect.api.dao.UsuarioDAO;
import com.fatec.mauricioselect.api.modelo.Categoria;
import com.fatec.mauricioselect.api.modelo.Usuario;
import com.fatec.mauricioselect.api.modelo.UsuarioBuilder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UsuarioDAOMariaDB implements UsuarioDAO {

    private Conexao conexao;
    public UsuarioDAOMariaDB() {
        this.conexao = new Conexao();
    }
        
    @Override
    public Usuario inserir(Usuario usuario) {           

        Connection conn = conexao.novaConexao();
        
        PreparedStatement sql;
        
        try {

            sql = conn.prepareStatement("INSERT INTO usuario "
                    + "(usr_apelido, usr_senha, usr_email, usr_nome, usr_cpf,"
                    + " usr_rg, usr_sexo) "
                    + "VALUES(?, ?, ?, ?, ?, ?, ?)");

            sql.setString(1, usuario.getApelido());
            sql.setString(2, usuario.getSenha());
            sql.setString(3, usuario.getEmail());
            sql.setString(4, usuario.getNome());
            sql.setString(5, usuario.getCpf());
            sql.setString(6, usuario.getRg());
            sql.setString(7, usuario.getSexo());
            sql.execute();             

        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAOMariaDB.class.getName()).log(Level.INFO, null, ex);
        }

        Usuario usuarioEncontrado = encontrarPorApelido(usuario.getApelido());
        usuario.setIdUsuario(usuarioEncontrado.getIdUsuario());

        return usuario;
    } 
    
    @Override
    public Usuario encontrarPorId(Integer id) {
        Connection con = conexao.novaConexao();
        PreparedStatement sql;
        ResultSet rs = null;
        Usuario usuario = null;
        try{
            sql = con.prepareStatement("SELECT usr_apelido, usr_senha, usr_email, usr_nome, usr_cpf,"
                    + " usr_rg, usr_sexo FROM usuario WHERE usr_id = ?");
            sql.setInt(1, id);
            rs = sql.executeQuery();
            sql.close();
            if(rs.next()){
                usuario = UsuarioBuilder.getBuilder()
                        .adicionaID(id)
                        .adicionaApelido(rs.getString(1))
                        .adicionaSenha(rs.getString(2))
                        .adicionaEmail(rs.getString(3))
                        .adicionaNome(rs.getString(4))
                        .adicionaCPF(rs.getString(5))
                        .adicionaRG(rs.getString(6))
                        .adicionaSexo(rs.getString(7))
                        .build();
            }
        }catch(SQLException e){
            Logger.getLogger(UsuarioDAOMariaDB.class.getName()).log(Level.SEVERE, null, e);
        }
        return usuario;
    }

    @Override
    public Usuario encontrarPorApelido(String apelidoUsuario) {
        Connection con = conexao.novaConexao();
        PreparedStatement sql;
        ResultSet rs = null;
        Usuario usuario = null;
        
        try {
            sql = con.prepareStatement("SELECT usr_id, usr_cpf, usr_email, "
                    + "usr_apelido, usr_rg, usr_nome, usr_senha, usr_sexo "
                    + "FROM usuario WHERE usr_apelido = ?");
            sql.setString(1, apelidoUsuario);
            rs = sql.executeQuery();
            sql.close();   
            
            if (rs.next()){                
                usuario = UsuarioBuilder.getBuilder()
                                        .adicionaID(rs.getInt(1))
                                        .adicionaCPF(rs.getString(2))
                                        .adicionaEmail(rs.getString(3))
                                        .adicionaApelido(rs.getString(4))
                                        .adicionaRG(rs.getString(5))
                                        .adicionaNome(rs.getString(6))
                                        .adicionaSenha(rs.getString(7))
                                        .adicionaSexo(rs.getString(8)).build();
            } 
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAOMariaDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return usuario;
    }
    
    @Override
    public Usuario encontrarPorEmail(String email) {
        Connection con = conexao.novaConexao();
        PreparedStatement sql;
        ResultSet rs = null;
        Usuario usuario = null;
        
        try {
            sql = con.prepareStatement("SELECT usr_id, usr_cpf, usr_email, "
                    + "usr_apelido, usr_rg, usr_nome, usr_senha, usr_sexo "
                    + "FROM usuario WHERE usr_email = ?");
            sql.setString(1, email);
            rs = sql.executeQuery();
            sql.close();   
            
            if (rs.next()){                
                usuario = UsuarioBuilder.getBuilder()
                                        .adicionaID(rs.getInt(1))
                                        .adicionaCPF(rs.getString(2))
                                        .adicionaEmail(rs.getString(3))
                                        .adicionaApelido(rs.getString(4))
                                        .adicionaRG(rs.getString(5))
                                        .adicionaNome(rs.getString(6))
                                        .adicionaSenha(rs.getString(7))
                                        .adicionaSexo(rs.getString(8)).build();
            } 
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAOMariaDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return usuario;
    }

    @Override
    public boolean deletar(Integer idUsuario) {
        Connection con = conexao.novaConexao();
        PreparedStatement sql;
        boolean resultado = false;
        
        try {
            sql = con.prepareStatement("DELETE FROM usuario WHERE usr_id = ?");
            sql.setInt(1, idUsuario);
            resultado = sql.execute();
            sql.close();
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAOMariaDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultado;
    }

    @Override
    public List<Usuario> encontrarTudo() {
        Connection con = conexao.novaConexao();
        PreparedStatement sql;
        ResultSet rs = null;
        List<Usuario> usuarios = null;
        
        try {
            sql = con.prepareStatement("SELECT "
                    + "usr_id, "
                    + "usr_cpf, "
                    + "usr_email, "
                    + "usr_apelido, "
                    + "usr_rg, "
                    + "usr_nome, "
                    + "usr_senha, "
                    + "usr_sexo "
                    + "FROM usuario");
            
            rs = sql.executeQuery();
            sql.close();
            
            if (rs != null){
                usuarios = new ArrayList<>();
                while(rs.next()){
                    Usuario usuario = UsuarioBuilder.getBuilder()
                                            .adicionaID(rs.getInt(1))
                                            .adicionaCPF(rs.getString(2))
                                            .adicionaEmail(rs.getString(3))
                                            .adicionaApelido(rs.getString(4))
                                            .adicionaRG(rs.getString(5))
                                            .adicionaNome(rs.getString(6))
                                            .adicionaSenha(rs.getString(7))
                                            .adicionaSexo(rs.getString(8))
                            .build();
                    usuarios.add(usuario);
                }
            } 
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAOMariaDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return usuarios;
    }

    @Override
    public Usuario atualizar(Usuario usuarioAntigo, Usuario usuarioAtual) {
        Connection conn = conexao.novaConexao();
        PreparedStatement sql;
        ResultSet rs = null;
        Categoria categoria = null;
        
        if (usuarioAtual.getSenha() == null) {
            
        }
        
        try {   
            sql = conn.prepareStatement("UPDATE usuario "
                    + "SET usr_nome = ?, usr_email = ?,"
                    + "usr_rg = ?, usr_cpf = ?, usr_senha = ?"
                    + " WHERE usr_id = ?");
            sql.setString(1, usuarioAtual.getNome());
            sql.setString(2, usuarioAtual.getEmail());
            sql.setString(3, usuarioAtual.getRg());
            sql.setString(4, usuarioAtual.getCpf());
            sql.setString(5, usuarioAtual.getSenha());
            sql.setInt(6, usuarioAntigo.getIdUsuario());
            sql.execute();
         
        } catch (SQLException ex) {
            Logger.getLogger(CategoriaDAOMariaDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return encontrarPorApelido(usuarioAtual.getApelido());
    }
}
