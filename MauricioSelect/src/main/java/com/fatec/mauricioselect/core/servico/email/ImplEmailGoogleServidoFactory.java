package com.fatec.mauricioselect.core.servico.email;

import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Factory da implementação do serviço de email do Google
 * @author felipe
 */
public class ImplEmailGoogleServidoFactory {
    
   /**
    * Método para criar um objeto de envio de email com o servidor do Google
    * @return 
    */
   public static ImplEmailGoogleServico fabricaServicoDeEmail() {
       
       ImplEmailGoogleServico implEmailGoogleServico = null;
       
       try {
           Properties propCredencial = CarregadorDeProperiedadeEmail
                   .carregaProperiedadesDeCredenciais();
           AutenticadorDeEmail autenticadorDeEmail = 
                                new AutenticadorDeEmail(
                                        propCredencial.getProperty("usuario"), 
                                        propCredencial.getProperty("senha"));
           
           implEmailGoogleServico = new ImplEmailGoogleServico(autenticadorDeEmail);
       
       } catch (IOException ex) {
           Logger.getLogger(ImplEmailGoogleServidoFactory.class.getName()).log(Level.SEVERE, null, ex);
       }
       return implEmailGoogleServico;
   } 
}
