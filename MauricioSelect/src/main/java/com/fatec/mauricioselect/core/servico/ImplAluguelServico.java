package com.fatec.mauricioselect.core.servico;

import com.fatec.mauricioselect.api.dao.AluguelDAO;
import com.fatec.mauricioselect.api.dao.CategoriaDAO;
import com.fatec.mauricioselect.api.dao.OperadorDAO;
import com.fatec.mauricioselect.api.dao.ReservaDAO;
import com.fatec.mauricioselect.api.dao.CarroDAO;
import com.fatec.mauricioselect.api.dao.UsuarioDAO;
import com.fatec.mauricioselect.api.modelo.Aluguel;
import com.fatec.mauricioselect.api.modelo.abstrato.AluguelAbstrato;
import com.fatec.mauricioselect.api.modelo.Carro;
import com.fatec.mauricioselect.api.modelo.Categoria;
import com.fatec.mauricioselect.api.modelo.Operador;
import com.fatec.mauricioselect.api.modelo.OperadorBuilder;
import com.fatec.mauricioselect.api.modelo.Reserva;
import com.fatec.mauricioselect.api.modelo.Usuario;
import com.fatec.mauricioselect.api.modelo.categorico.AluguelCategorico;
import com.fatec.mauricioselect.api.modelo.enumerador.PrecosMultaCarro;
import com.fatec.mauricioselect.api.servico.AluguelServico;
import com.fatec.mauricioselect.core.dao.AluguelDAOMariaDB;
import com.fatec.mauricioselect.core.dao.CarroDAOMariaDB;
import com.fatec.mauricioselect.core.dao.CategoriaDAOMariaDB;
import com.fatec.mauricioselect.core.dao.OperadorDAOMariaDB;
import com.fatec.mauricioselect.core.dao.ReservaDAOMariaDB;
import com.fatec.mauricioselect.core.dao.UsuarioDAOMariaDB;
import com.fatec.mauricioselect.web.servlet.utilitarios.ManipuladorDeData;
import com.fatec.mauricioselect.web.servlet.utilitarios.ValidadorDeCampos;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ImplAluguelServico implements AluguelServico {

    private final AluguelDAO aluguelDAO;

    public ImplAluguelServico() {
        aluguelDAO = new AluguelDAOMariaDB();
    }

    @Override
    public AluguelCategorico inserirAluguel(Aluguel aluguel) {

        verificacaoGeralDeAluguel(aluguel);

        AluguelCategorico aluguelEncontrado = encontrarAluguel(aluguel);
        if (aluguelEncontrado == null) {
            return criaAluguelCategorico(aluguelDAO.inserir(aluguel));
        } else {
            return null;
        }
    }

    @Override
    public AluguelCategorico encontrarAluguelPorId(Integer idAluguel) {

        if (Objects.isNull(idAluguel)) {
            throw new RuntimeException("O ID inserido é nulo!");
        }

        AluguelCategorico aluguelCategorico = null;
        try {
            aluguelCategorico = criaAluguelCategorico(aluguelDAO.encontrarPorId(idAluguel));
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName(), Level.SEVERE + "Erro ao buscar aluguel por id: " + e.getMessage());
        }
        return aluguelCategorico;
    }

    @Override
    public AluguelCategorico encontrarAluguel(Aluguel aluguel) {

        if (Objects.isNull(aluguel)) {
            throw new RuntimeException("O objeto inserido está vazio!");
        }

        AluguelCategorico aluguelEncontrado = null;
        try {
            aluguelEncontrado = criaAluguelCategorico(aluguelDAO.encontrarAluguel(aluguel));
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName(), Level.SEVERE + "Erro ao buscar aluguel: " + e.getMessage());
        }
        return aluguelEncontrado;
    }

    @Override
    public List<AluguelCategorico> encontrarTudo() {

        List<Aluguel> alugueis = null;
        List<AluguelCategorico> aluguelCategoricos = null;

        try {
            alugueis = aluguelDAO.encontrarTudo();            
            aluguelCategoricos = criaListaDeAluguelCategorico(alugueis);
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName(), Level.SEVERE + "Erro ao buscar todos os alugueis: " + e.getMessage());
        }

        return aluguelCategoricos;
    }

    @Override
    public boolean atualizar(Aluguel aluguelAtual) {
        validaDadosDoAluguelParaAtualizacao(aluguelAtual);
        
        Aluguel aluguelAtualizado = null;
        Aluguel aluguelAntigo = aluguelDAO.encontrarAluguel(aluguelAtual);
        if (aluguelAntigo != null) {
            try {
                aluguelAtualizado = aluguelDAO.atualizar(aluguelAntigo, aluguelAtual);
                if (Objects.nonNull(aluguelAtualizado)) {
                    return true;
                }
            } catch (Exception e) {
                Logger.getLogger(this.getClass().getName(), Level.SEVERE + "Erro ao atualizar o aluguel: " + e.getMessage());
            }
        }
        return false;
    }

    @Override
    public boolean finalizaAluguel(AluguelCategorico aluguelCategorico) {
        validaDadosParaFinalizacao(aluguelCategorico);
        aluguelCategorico.setStatus("FINALIZADO");
                        
        Aluguel aluguelAtualizado = null;
        Aluguel aluguelParaAtualizar = new Aluguel(aluguelCategorico.getIdAluguel(),
                                        aluguelCategorico.getDataDevolucao(),
                                        aluguelCategorico.getValorTotal(),
                                        aluguelCategorico.getStatus());
        
        try {
            Aluguel aluguelAtual = aluguelDAO.encontrarPorId(aluguelCategorico.getIdAluguel());
                        
            aluguelAtualizado = aluguelDAO.atualizar(aluguelAtual, aluguelParaAtualizar);
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName(), 
                    Level.SEVERE + "Erro ao atualizar o aluguel: " + e.getMessage());
        }
                
        // Após a atualização do aluguel, o carro também é atualizado
        CarroDAO carroDAO = new CarroDAOMariaDB();
        // Busca o estado atual do carro no banco de dados
        Carro carroAntigo = carroDAO.encontrarPorId(aluguelCategorico
                                                        .getCarro()
                                                        .getIdCarro());
        Carro carroAtualizado = carroDAO
                                .atualizar(carroAntigo, aluguelCategorico.getCarro());
       
        // true caso o carro e o aluguel tenham sido atualizados com sucesso!
        return Objects.nonNull(aluguelAtualizado) && Objects.nonNull(carroAtualizado);
    }
    
    @Override
    public boolean deletar(Integer idAluguel) {

        boolean operacao = false;
        try {
            operacao = aluguelDAO.deletar(idAluguel);
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName(),
                    Level.SEVERE + "Erro ao deletar o aluguel: " + e.getMessage());
        }
        return operacao;
    }

    
    @Override
    public List<AluguelCategorico> encontraPorEstado(String estado) {
        if (!estado.isEmpty()) {
            return criaListaDeAluguelCategorico(aluguelDAO
                                                    .encontrarPorEstado(estado));
        }
        return null;
    }
    
    /**
     * Método para a criação dos objetos categóricos (União das tabelas pontes)
     *
     * @param aluguel
     * @return
     */
    private AluguelCategorico criaAluguelCategorico(Aluguel aluguel) {

        if (Objects.isNull(aluguel)) {
            return null;
        }

        Carro carro = null;
        Reserva reserva = null;
        Categoria categoria = null;

        UsuarioDAO usuarioDAO = new UsuarioDAOMariaDB();
        OperadorDAO operadorDAO = new OperadorDAOMariaDB();
        CarroDAO carroDAO = new CarroDAOMariaDB();
        CategoriaDAO categoriaDAO = new CategoriaDAOMariaDB();
        ReservaDAO reservaDAO = new ReservaDAOMariaDB();

        List<Carro> carros = carroDAO.encontrarTudo();
        List<Categoria> categorias = categoriaDAO.encontrarTudo();
        List<Reserva> reservas = reservaDAO.encontrarTudo();

        Optional<Carro> carroOpcional = carros.stream().filter(c
                -> c.getIdCarro()
                        .equals(aluguel.getIdCarro()))
                .findFirst();

        Optional<Reserva> reservaOpcional = reservas.stream().filter(p
                -> p.getIdReserva()
                        .equals(aluguel.getIdReserva()))
                .findFirst();

        // Verifica se carros e reservas foram encontradas
        if (carroOpcional.isPresent() && reservaOpcional.isPresent()) {

            reserva = reservaOpcional.get();
            carro = carroOpcional.get();
            // Criando final para ser passado no lambda
            final Integer idCategoria = carro.getIdCategoria();

            Optional<Categoria> categoriaOpcional = categorias.stream()
                    .filter(c
                            -> c.getIdCategoria()
                            .equals(idCategoria))
                    .findFirst();

            if (categoriaOpcional.isPresent()) {
                categoria = categoriaOpcional.get();
            }
        } else {
            throw new RuntimeException("Nenhum carro/reserva foi encontrado para este aluguel");
        }

        // Construindo o objeto do operador
        Usuario usuarioOperador = usuarioDAO
                                        .encontrarPorId(aluguel.getIdOperador());
        Operador operadorTemporario = operadorDAO
                                        .encontrarPorId(aluguel.getIdOperador());
        Operador operador = OperadorBuilder.getBuilder()
                                    .consomeObjetoUsuario(usuarioOperador)
                                    .adicionaCargo(operadorTemporario.getCargo())
                                    .build();
        // Usuário requirente do aluguel
        Usuario usuario = usuarioDAO.encontrarPorId(aluguel.getIdUsuario());
        
        try {
            return new AluguelCategorico(aluguel.getIdAluguel(), aluguel.getIdReserva(),
                    aluguel.getStatus(), aluguel.getDataRetirada(),
                    aluguel.getDataDevolucao(), aluguel.getValorTotal(),
                    aluguel.getValorPagoAntecipado(), usuario,
                    operador, carro, categoria, reserva);
        } catch (Exception e) {
            throw new RuntimeException("Não foi possível criar o objeto de aluguel categórico");
        }
    }

    /**
     * Método para criar uma lista de alugueis categóricos
     *
     * @param alugueis
     * @return
     */
    private List<AluguelCategorico> criaListaDeAluguelCategorico(List<Aluguel> alugueis) {
        List<AluguelCategorico> aluguelCategoricos = new LinkedList<>();

        alugueis.forEach((aluguel) -> {
            aluguelCategoricos.add(criaAluguelCategorico(aluguel));
        });
        return aluguelCategoricos;
    }

    /**
     * Método para validar o aluguel que será atualizado
     */
    private void validaDadosDoAluguelParaAtualizacao(Aluguel aluguel) {
        if (Objects.isNull(aluguel)) {
            throw new RuntimeException("O objeto inserido é nulo!");
        }
        if ("FINALIZADO".equals(aluguel.getStatus())) {
            throw new RuntimeException("Alugueis finalizados não podem ser alterados!");
        }
    }
    
    /**
     * Método para validar os dados do aluguel antes de finalizar ele
     * @param aluguelCategorico 
     */
    private void validaDadosParaFinalizacao(AluguelCategorico aluguelCategorico) {  
        if (aluguelCategorico.getCarro().getTanque() > 100) {
            throw new RuntimeException("O tanque pode ir até 100%");
        }
        
        // Verificando estado de conservação (Para validar que o valor foi aplicado
        // corretamente
        Integer valorDaMulta = PrecosMultaCarro
                                    .valueOf(aluguelCategorico
                                            .getCarro().getEstadoConserva()).getValor();
                
        // Verifica se a data de finalização consumiu o valorPagoAntecipado
        // Caso não seja um erro é retornado! Nossa aplicação não aceita devoluções
        Integer distanciaEntreDatas = ManipuladorDeData
                .distanciaEntreAsDatas(aluguelCategorico.getDataRetirada(), 
                                        aluguelCategorico.getDataDevolucao());
        Double valorTotal = (distanciaEntreDatas * 24) * aluguelCategorico
                                                                .getCategoria()
                                                                .getValorPorHora();
                                
        if ((valorTotal - aluguelCategorico.getValorPagoAntecipado()) < 0) {              
            throw new RuntimeException("O usuário deve consumir o carro até que "
                    + "todo o pagamento antecipado seja utilizado");
        }
                        
        // Lógica para a verificação dos dados para a finalização do aluguel
        if (aluguelCategorico.getCarro().getEstaQuebrado().equals(1) && 
                aluguelCategorico.getCarro().getTanque() < 80) {
            
            if (!aluguelCategorico.getValorTotal().equals(valorTotal 
                                        + (PrecosMultaCarro.ESTA_QUEBRADO.getValor() +
                                                PrecosMultaCarro.TANQUE_SEM_MINIMO.getValor())
                                                    + valorDaMulta)) {
                
                throw new RuntimeException("Parece que o valor da multa "
                                + "está errado. Verifique os valores!!");
            }
        } else {
            // Caso as duas alternativas não sejam verdadeiras, verifica uma a uma
            if (aluguelCategorico.getCarro().getEstaQuebrado().equals(1)) {
                if (!aluguelCategorico.getValorTotal().equals(valorTotal + 
                        PrecosMultaCarro.ESTA_QUEBRADO.getValor() + valorDaMulta)) {
                    
                    throw new RuntimeException("Há inconsistências nos "
                                                    + "valores aplicado a "
                                                    + "multa do carro quebrado!");
                }
            }
            else if (aluguelCategorico.getCarro().getTanque() < 80) {
                if (!aluguelCategorico.getValorTotal().equals(valorTotal + 
                        PrecosMultaCarro.TANQUE_SEM_MINIMO.getValor() + valorDaMulta)) {
                    
                    throw new RuntimeException("Há inconsistências nos "
                                    + "valores da multa "
                                    + "aplicada ao carro sem o tanque mínimo");
                }
            } else {                
                if (!aluguelCategorico.getValorTotal().equals(valorTotal + valorDaMulta)) {
                    throw new RuntimeException("Há inconsistência nas multas por estado de conservação!");                    
                }
            }
        }
    }   

    /**
     * Método para verificar informações gerais presentes no aluguel
     * @param aluguelAbstrato
     */
    public void verificacaoGeralDeAluguel(AluguelAbstrato aluguelAbstrato) {
        if (Objects.isNull(aluguelAbstrato)) {
            throw new RuntimeException("O objeto inserido é nulo!");
        }
        
        // Este campo tem que ser definido pois o padrão é 0
        aluguelAbstrato.setIdAluguel(0);
        try {
            ValidadorDeCampos.validaCamposDoObjeto(aluguelAbstrato);
        } catch (InvocationTargetException ex) {
            Logger.getLogger(ImplAluguelServico.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
