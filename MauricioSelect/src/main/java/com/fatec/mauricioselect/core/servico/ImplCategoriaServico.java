package com.fatec.mauricioselect.core.servico;

import com.fatec.mauricioselect.api.dao.CategoriaDAO;
import com.fatec.mauricioselect.api.modelo.Categoria;
import com.fatec.mauricioselect.api.servico.CategoriaServico;
import com.fatec.mauricioselect.core.dao.CategoriaDAOMariaDB;
import java.util.List;

public class ImplCategoriaServico implements CategoriaServico {

    private CategoriaDAO categoriaDAO;

    public ImplCategoriaServico() {
        categoriaDAO = new CategoriaDAOMariaDB();
    }

    @Override
    public List<Categoria> recuperaTodasCategorias() {
        return categoriaDAO.encontrarTudo();
    }

    @Override
    public boolean deleta(Categoria categoria) {
        Boolean resultado = false;
        if (categoria.getIdCategoria() == null) {
            throw new RuntimeException("ID inválido!");
        } else {
            resultado = categoriaDAO.deletar(categoria.getIdCategoria());
        }
        
        return resultado;
    }

    @Override
    public Categoria inserir(Categoria categoria) {
        validaDadosDaCategoria(categoria);
        
        return categoriaDAO.inserir(categoria);
    }

    @Override
    public Categoria encontrarPorNome(String nomeCategoria) {
        return categoriaDAO.encontrarPorNome(nomeCategoria);
    }

    @Override
    public Categoria encontrarPorId(Integer idCategria) {
        return categoriaDAO.encontrarPorId(idCategria);
    }
    
    @Override
    public Categoria atualizarCategoria(Categoria novaCategoria) {
        validaDadosDaCategoria(novaCategoria);
                
        // Busca categoria antiga e atualiza com a nova categoria
        Categoria categoriaAntiga = categoriaDAO
                            .encontrarPorId(novaCategoria.getIdCategoria());
        return categoriaDAO.atualizar(categoriaAntiga, novaCategoria);
    }
    
    /**
     * Método para validar os dados da categoria
     * @param categoria 
     */
    private void validaDadosDaCategoria(Categoria categoria) {
        if (categoria.getNome().trim().isEmpty() || categoria.getValorPorHora() <= 0) {
            throw new RuntimeException("Os dados da categoria são inconsistente");
        }
    }
}
