package com.fatec.mauricioselect.core.servico.email;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * Classe para facilitar o carregamento das propriedades utilizadas nos
 * serviços de email
 */
public class CarregadorDeProperiedadeEmail {

    /**
     * Carrega as propriedades do servidor de email
     * @return
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public static Properties carregaProperiedadesDoServidor() throws FileNotFoundException, IOException {
        Properties prop = new Properties();
        /**
         * Exemplo deste arquivo:
         * mail.smtp.host=true
         * mail.smtp.starttls.enable=true
         * mail.smtp.host=smtp.gmail.com
         * mail.smtp.port=587
         * mail.smtp.auth=true
         */
        prop.load(new FileInputStream("src/main/java/com/fatec/mauricioselect/core/servico/email/servidor.properties"));
        
        return prop;
    }
    
    /**
     * Carrega as propriedades de autenticação no servidor de email
     * @return
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public static Properties carregaProperiedadesDeCredenciais() throws FileNotFoundException, IOException {
        Properties prop = new Properties();
        /**
         * Exemplo deste arquivo:
         * usuario=juan@gmail.com
         * senha=1234
         */
        prop.load(new FileInputStream("src/main/java/com/fatec/mauricioselect/core/servico/email/credencial.properties"));
        
        return prop;
    }
}
