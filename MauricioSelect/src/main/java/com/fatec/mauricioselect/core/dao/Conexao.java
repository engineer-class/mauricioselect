/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fatec.mauricioselect.core.dao;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Conexao {
    
    private String jdbcDriver;
    private String jdbcURL;
    private String jdbcUsuario;
    private String jdbcSenha;
    private static Connection conexao;
    private Properties prop;
    private URI jdbcUri;
    public Conexao(){

       
        try {
            jdbcUri = new URI(System.getenv("JAWSDB_MARIA_URL"));
        } catch(URISyntaxException ex){
            Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, "Não foi possível carregar a variável de ambiente: ", ex);
        }
        this.jdbcUsuario = jdbcUri.getUserInfo().split(":")[0];
        this.jdbcSenha = jdbcUri.getUserInfo().split(":")[1];
        String port = String.valueOf(jdbcUri.getPort());
        this.jdbcURL = "jdbc:mysql://" + jdbcUri.getHost() + ":" + port + jdbcUri.getPath();
    }

    public Connection novaConexao() {
        try {
            if(conexao == null){
                conexao = DriverManager.getConnection(jdbcURL,jdbcUsuario, jdbcSenha);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, 
                            "Erro ao abrir conexão com o banco de dados: ", ex);
        }    
        return conexao;
    }

}
