package com.fatec.mauricioselect.core.dao;

import com.fatec.mauricioselect.api.dao.AluguelDAO;
import com.fatec.mauricioselect.api.modelo.Aluguel;
import com.fatec.mauricioselect.api.modelo.AluguelBuilder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AluguelDAOMariaDB implements AluguelDAO {

    private Conexao conexao;

    public AluguelDAOMariaDB() {
        this.conexao = new Conexao();
    }

    @Override
    public Aluguel inserir(Aluguel aluguel) {
        
        Connection conn = conexao.novaConexao();
        PreparedStatement sql;
        Aluguel aluguelEncontrado = null;
        
        try {
            
            sql = conn.prepareStatement("INSERT INTO aluguel "
                    + "(alu_res_id, "
                    + "alu_usr_id, "
                    + "alu_ope_id, "
                    + "alu_data_devolucao, "
                    + "alu_data_retirada, "
                    + "alu_status, "
                    + "alu_valor_total, "
                    + "alu_valor_pago_antecipado, "
                    + "alu_car_id)"
                    + "VALUES(?, ?, ?, ?, ?, ?, ?, ?,?)"
            );
            
            sql.setInt(1, aluguel.getIdReserva());
            sql.setInt(2, aluguel.getIdUsuario());
            sql.setInt(3, aluguel.getIdOperador());
            sql.setDate(4, aluguel.getDataDevolucao());
            sql.setDate(5, aluguel.getDataRetirada());
            sql.setString(6, aluguel.getStatus());
            sql.setDouble(7, aluguel.getValorTotal());
            sql.setDouble(8, aluguel.getValorPagoAntecipado());
            sql.setInt(9, aluguel.getIdCarro());
            
            sql.execute();
            sql.close();
        
        } catch (SQLException e) {
            Logger.getLogger(AluguelDAOMariaDB.class.getName()).log(Level.SEVERE, null, e);
        }

        aluguelEncontrado = encontrarAluguel(aluguel);
        aluguel.setIdAluguel(aluguelEncontrado.getIdAluguel());

        return aluguel;
    }

    @Override
    public Aluguel encontrarPorId(Integer idAluguel) {
        Connection conn = conexao.novaConexao();
        PreparedStatement sql;
        ResultSet rs;
        Aluguel aluguelEncontrado = null;

        try {
            sql = conn.prepareStatement("SELECT "
                    + "alu_id,"
                    + "alu_res_id,"
                    + "alu_usr_id,"
                    + "alu_ope_id,"
                    + "alu_car_id,"
                    + "alu_status,"
                    + "alu_data_devolucao,"
                    + "alu_data_retirada,"
                    + "alu_valor_total,"
                    + "alu_valor_pago_antecipado "
                    + "FROM aluguel "
                    + "WHERE alu_id = ?");
            sql.setInt(1, idAluguel);
            rs = sql.executeQuery();
            if (rs.next()) {
                aluguelEncontrado = AluguelBuilder.getBuilder()
                        .adicionaIdAluguel(rs.getInt("alu_id"))
                        .adicionaIdReserva(rs.getInt("alu_res_id"))
                        .adicionaIdUsuario(rs.getInt("alu_usr_id"))
                        .adicionaIdOperador(rs.getInt("alu_ope_id"))
                        .adicionaIdCarro(rs.getInt("alu_car_id"))
                        .adicionaStatus(rs.getString("alu_status"))
                        .adicionaDataDevolucao(rs.getDate("alu_data_devolucao"))
                        .adicionaDataRetirada(rs.getDate("alu_data_retirada"))
                        .adicionaValorTotal(rs.getDouble("alu_valor_total"))
                        .adicionaValorPagoAntecipado(rs.getDouble("alu_valor_pago_antecipado"))
                        .build();
            }
        } catch (SQLException e) {
            Logger.getLogger(AluguelDAOMariaDB.class.getName()).log(Level.SEVERE, null, e);
        }
        return aluguelEncontrado;
    }

    @Override
    public Aluguel encontrarAluguel(Aluguel aluguel) {
        Connection conn = conexao.novaConexao();
        PreparedStatement sql;
        ResultSet rs;
        Aluguel aluguelEncontrado = null;
        try {
            sql = conn.prepareStatement("SELECT "
                    + "alu_id,"
                    + "alu_res_id,"
                    + "alu_usr_id,"
                    + "alu_ope_id,"
                    + "alu_car_id,"
                    + "alu_status,"
                    + "alu_data_devolucao,"
                    + "alu_data_retirada,"
                    + "alu_valor_total,"
                    + "alu_valor_pago_antecipado "
                    + "FROM aluguel "
                    + "WHERE alu_res_id = ?");

            sql.setInt(1, aluguel.getIdReserva());
            rs = sql.executeQuery();

            if (rs.next()) {
                aluguelEncontrado = AluguelBuilder.getBuilder()
                        .adicionaIdAluguel(rs.getInt("alu_id"))
                        .adicionaIdReserva(rs.getInt("alu_res_id"))
                        .adicionaIdUsuario(rs.getInt("alu_usr_id"))
                        .adicionaIdOperador(rs.getInt("alu_ope_id"))
                        .adicionaIdCarro(rs.getInt("alu_car_id"))
                        .adicionaStatus(rs.getString("alu_status"))
                        .adicionaDataDevolucao(rs.getDate("alu_data_devolucao"))
                        .adicionaDataRetirada(rs.getDate("alu_data_retirada"))
                        .adicionaValorTotal(rs.getDouble("alu_valor_total"))
                        .adicionaValorPagoAntecipado(rs.getDouble("alu_valor_pago_antecipado"))
                        .build();
            }
        } catch (SQLException e) {
            Logger.getLogger(AluguelDAOMariaDB.class.getName()).log(Level.SEVERE, null, e);
        }
        return aluguelEncontrado;
    }

    @Override
    public List<Aluguel> encontrarTudo() {
        Connection conn = conexao.novaConexao();
        PreparedStatement sql;
        ResultSet rs = null;
        List<Aluguel> alugueis = null;
        try {
            sql = conn.prepareStatement("SELECT "
                    + "alu_id,"
                    + "alu_res_id,"
                    + "alu_usr_id,"
                    + "alu_ope_id,"
                    + "alu_car_id,"
                    + "alu_status,"
                    + "alu_data_devolucao,"
                    + "alu_data_retirada,"
                    + "alu_valor_total,"
                    + "alu_valor_pago_antecipado "
                    + "FROM aluguel"
            );
            rs = sql.executeQuery();
            if (rs != null) {
                alugueis = new ArrayList<>();
                while (rs.next()) {
                    Aluguel aluguel = AluguelBuilder.getBuilder()
                            .adicionaIdAluguel(rs.getInt("alu_id"))
                            .adicionaIdReserva(rs.getInt("alu_res_id"))
                            .adicionaIdUsuario(rs.getInt("alu_usr_id"))
                            .adicionaIdOperador(rs.getInt("alu_ope_id"))
                            .adicionaIdCarro(rs.getInt("alu_car_id"))
                            .adicionaStatus(rs.getString("alu_status"))
                            .adicionaDataDevolucao(rs.getDate("alu_data_devolucao"))
                            .adicionaDataRetirada(rs.getDate("alu_data_retirada"))
                            .adicionaValorTotal(rs.getDouble("alu_valor_total"))
                            .adicionaValorPagoAntecipado(rs.getDouble("alu_valor_pago_antecipado"))
                            .build();

                    alugueis.add(aluguel);
                }
            }
        } catch (SQLException e) {
            Logger.getLogger(AluguelDAOMariaDB.class.getName()).log(Level.SEVERE, null, e);
        }
        return alugueis;
    }

    @Override
    public Aluguel atualizar(Aluguel aluguelAntigo, Aluguel aluguelAtual) {
        Connection conn = conexao.novaConexao();
        PreparedStatement sql;
        
        // Para o aluguel, apenas alguns valores podem ser atualizados!        
        try {
            sql = conn.prepareStatement("UPDATE aluguel "
                    + "SET alu_data_devolucao = ?,"
                    + "alu_valor_total = ?,"
                    + "alu_status = ? "
                    + "WHERE alu_id = ?");
            sql.setDate(1, aluguelAtual.getDataDevolucao());
            sql.setDouble(2, aluguelAtual.getValorTotal());
            sql.setString(3, aluguelAtual.getStatus());
            sql.setInt(4, aluguelAntigo.getIdAluguel());
            sql.execute();
        } catch(SQLException e) {
            Logger.getLogger(AluguelDAOMariaDB.class.getName()).log(Level.SEVERE, null, e);
        }
        return encontrarPorId(aluguelAntigo.getIdAluguel());
    }

    @Override
    public boolean deletar(Integer idAluguel) {
        Connection conn = conexao.novaConexao();
        PreparedStatement sql;
        boolean resultado = false;
        try {
            sql = conn.prepareStatement("DELETE FROM aluguel WHERE alu_id = ?");
            sql.setInt(1, idAluguel);
            sql.execute();
            Aluguel aluguel = encontrarPorId(idAluguel);
            if (aluguel == null) {
                resultado = true;
            }
            sql.close();
        } catch (SQLException e) {
            Logger.getLogger(AluguelDAOMariaDB.class.getName()).log(Level.SEVERE, null, e);
        }
        return resultado;
    }

    @Override
    public List<Aluguel> encontrarPorEstado(String estado) {
               Connection conn = conexao.novaConexao();
        PreparedStatement sql;
        ResultSet rs = null;
        List<Aluguel> alugueis = null;
        try {
            sql = conn.prepareStatement("SELECT "
                    + "alu_id,"
                    + "alu_res_id,"
                    + "alu_usr_id,"
                    + "alu_ope_id,"
                    + "alu_car_id,"
                    + "alu_status,"
                    + "alu_data_devolucao,"
                    + "alu_data_retirada,"
                    + "alu_valor_total,"
                    + "alu_valor_pago_antecipado "
                    + "FROM aluguel "
                    + "WHERE alu_status = ?"
            );
            sql.setString(1, estado);
            rs = sql.executeQuery();
            if(rs != null){
                alugueis = new ArrayList<>();
                while (rs.next()) {
                    Aluguel aluguel = AluguelBuilder.getBuilder()
                        .adicionaIdAluguel(rs.getInt("alu_id"))
                        .adicionaIdReserva(rs.getInt("alu_res_id"))
                        .adicionaIdUsuario(rs.getInt("alu_usr_id"))
                        .adicionaIdOperador(rs.getInt("alu_ope_id"))
                        .adicionaIdCarro(rs.getInt("alu_car_id"))
                        .adicionaStatus(rs.getString("alu_status"))
                        .adicionaDataDevolucao(rs.getDate("alu_data_devolucao"))
                        .adicionaDataRetirada(rs.getDate("alu_data_retirada"))
                        .adicionaValorTotal(rs.getDouble("alu_valor_total"))
                        .adicionaValorPagoAntecipado(rs.getDouble("alu_valor_pago_antecipado"))
                        .build();
                                        
                    alugueis.add(aluguel);
                }
            }
        }catch(SQLException e){
            Logger.getLogger(AluguelDAOMariaDB.class.getName()).log(Level.SEVERE, null, e);
        }
        return alugueis;
    }
}
