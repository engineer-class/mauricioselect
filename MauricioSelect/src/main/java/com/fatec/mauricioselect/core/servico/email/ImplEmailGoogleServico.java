package com.fatec.mauricioselect.core.servico.email;

import com.fatec.mauricioselect.api.modelo.Usuario;
import com.fatec.mauricioselect.api.servico.EmailServico;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class ImplEmailGoogleServico implements EmailServico {

    private AutenticadorDeEmail autenticadorDeEmail;
    private Properties propriedadeDoServidor;
    
    public ImplEmailGoogleServico(AutenticadorDeEmail autenticadorDeEmail) throws IOException {
        this.autenticadorDeEmail = autenticadorDeEmail;
        this.propriedadeDoServidor = CarregadorDeProperiedadeEmail
                .carregaProperiedadesDoServidor();
    } 

    @Override
    public void enviaEmailDeRecuperacao(Usuario usuario) {
        Session session = Session.getInstance(propriedadeDoServidor, autenticadorDeEmail);
                        
        Message mensagem = new MimeMessage(session);
        try {
            mensagem.setRecipients(
                    Message.RecipientType.TO, InternetAddress.parse(usuario.getEmail()));
            mensagem.setSubject("MauricioSelect - Recuperação de senha");
		 
            String corpoDoEmail = String.format("Olar %s, sua senha para utilizar"
                    + " o MauricioSelect eh: %s", usuario.getApelido(), usuario.getSenha());
		 
        MimeBodyPart objetoDoCorpoDoEmail = new MimeBodyPart();
        objetoDoCorpoDoEmail.setContent(corpoDoEmail, "text/html");
		 
        Multipart mp = new MimeMultipart();
        mp.addBodyPart(objetoDoCorpoDoEmail);
		 
        mensagem.setContent(mp);
		 
        Transport.send(mensagem);   
        
        } catch (MessagingException ex) {
            Logger.getLogger(ImplEmailGoogleServico.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
