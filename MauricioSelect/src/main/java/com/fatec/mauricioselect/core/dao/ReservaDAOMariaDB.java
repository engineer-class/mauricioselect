package com.fatec.mauricioselect.core.dao;

import com.fatec.mauricioselect.api.dao.ReservaDAO;
import com.fatec.mauricioselect.api.modelo.Reserva;
import com.fatec.mauricioselect.api.modelo.ReservaBuilder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ReservaDAOMariaDB implements ReservaDAO {

    private Conexao conexao;
    public ReservaDAOMariaDB() {
        this.conexao = new Conexao();
    }
    
    @Override
    public Reserva inserir(Reserva reserva) {
        
        Connection conn = conexao.novaConexao();
        PreparedStatement sql;
        ResultSet chavesGeradasNaInsercao = null;
                
        try {
            sql = conn.prepareStatement("INSERT INTO reserva (res_usr_id,"
                    + "res_cat_id,"
                    + "res_car_id,"
                    + "res_dia_final,"
                    + "res_dia_inicio,"
                    + "res_status,"
                    + "res_valor_total) "
                    + "VALUES (?,?,?,?,?,?,?)",
            Statement.RETURN_GENERATED_KEYS);
            sql.setInt(1, reserva.getIdUsuario());
            sql.setInt(2, reserva.getIdCategoria());
            sql.setInt(3, reserva.getIdCarro());
            sql.setDate(4, reserva.getDiaFinal());
            sql.setDate(5, reserva.getDiaInicio());
            sql.setString(6, reserva.getStatus());
            sql.setDouble(7, reserva.getValorTotal());
            sql.execute();
            
            // Recupera as chaves geradas na inserção
            chavesGeradasNaInsercao = sql.getGeneratedKeys();
                        
            sql.close();
        } catch (SQLException ex) {
             Logger.getLogger(ReservaDAOMariaDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            // Buscando a chave gerada
            if (chavesGeradasNaInsercao.next()) {
                reserva.setIdReserva(chavesGeradasNaInsercao.getInt(1));
            }
            conn = null;
        } catch (SQLException ex) {
            Logger.getLogger(ReservaDAOMariaDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return encontrarPorId(reserva.getIdReserva()); 
    }
    
    @Override
    public Reserva encontrarPorId(Integer id) {
        Connection conn = conexao.novaConexao();
        PreparedStatement sql;
        ResultSet rs = null;
        Reserva reserva = null;
        try {
            sql = conn.prepareStatement("SELECT "
                    + "res_id, "
                    + "res_usr_id,"
                    + "res_cat_id,"
                    + "res_car_id,"
                    + "res_dia_final,"
                    + "res_dia_inicio,"
                    + "res_status,"
                    + "res_valor_total"
                    + " FROM reserva "
                    + "WHERE res_id = ?"
            );
            sql.setInt(1, id);
            rs = sql.executeQuery();
            sql.close();
            if(rs.next()){
                reserva = ReservaBuilder.getBuilder()
                        .adicionaIdReserva(rs.getInt(1))
                        .adicionaIdUsuario(rs.getInt(2))
                        .adicionaIdCategoria(rs.getInt(3))
                        .adicionaIdCarro(rs.getInt(4))
                        .adicionaDiaFinal(rs.getDate(5))
                        .adicionaDiaInicio(rs.getDate(6))
                        .adicionaStatus(rs.getString(7))
                        .adicionaValorTotal(rs.getDouble(8))
                        .build();                
            }
            conn = null;
        } catch (SQLException ex) {
            Logger.getLogger(ReservaDAOMariaDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return reserva;
    }

    @Override
    public List<Reserva> encontrarPorIdUsuario(Integer idUsuario) {
        Connection conn = conexao.novaConexao();
        PreparedStatement sql;
        ResultSet rs = null;
        List<Reserva> reservas = null;
        
        try {
            sql = conn.prepareStatement("SELECT "
                    + "res_id, "
                    + "res_usr_id,"
                    + "res_cat_id,"
                    + "res_car_id,"
                    + "res_dia_final,"
                    + "res_dia_inicio,"
                    + "res_status,"
                    + "res_valor_total"
                    + " FROM reserva "
                    + "WHERE res_usr_id = ?"
            );
            sql.setInt(1, idUsuario);
            rs = sql.executeQuery();
            sql.close();
            if(rs != null){
                reservas = new ArrayList<>();
                while(rs.next()){
                    Reserva reserva = ReservaBuilder.getBuilder()
                            .adicionaIdReserva(rs.getInt("res_id"))
                            .adicionaIdUsuario(rs.getInt("res_usr_id"))
                            .adicionaIdCategoria(rs.getInt("res_cat_id"))
                            .adicionaIdCarro(rs.getInt("res_car_id"))
                            .adicionaDiaFinal(rs.getDate("res_dia_final"))
                            .adicionaDiaInicio(rs.getDate("res_dia_inicio"))
                            .adicionaStatus(rs.getString("res_status"))
                            .adicionaValorTotal(rs.getDouble("res_valor_total"))
                            .build();                    
                    reservas.add(reserva);
                }
            }
            conn = null;
        } catch (SQLException ex) {
            Logger.getLogger(ReservaDAOMariaDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return reservas;
    }

    @Override
    public List<Reserva> encontrarTudo() {
        Connection conn = conexao.novaConexao();
        PreparedStatement sql;
        ResultSet rs = null;
        List<Reserva> reservas = null;
        try {
            sql = conn.prepareStatement("SELECT "
                    + "res_id, "
                    + "res_usr_id,"
                    + "res_cat_id,"
                    + "res_car_id,"
                    + "res_dia_final,"
                    + "res_dia_inicio,"
                    + "res_status,"
                    + "res_valor_total"
                    + " FROM reserva"
            );
            rs = sql.executeQuery();
            sql.close();
            if (rs != null) {
                reservas = new ArrayList<>();
                while(rs.next()){
                    Reserva reserva = ReservaBuilder.getBuilder()
                            .adicionaIdReserva(rs.getInt("res_id"))
                            .adicionaIdUsuario(rs.getInt("res_usr_id"))
                            .adicionaIdCategoria(rs.getInt("res_cat_id"))
                            .adicionaIdCarro(rs.getInt("res_car_id"))
                            .adicionaDiaFinal(rs.getDate("res_dia_final"))
                            .adicionaDiaInicio(rs.getDate("res_dia_inicio"))
                            .adicionaStatus(rs.getString("res_status"))
                            .adicionaValorTotal(rs.getDouble("res_valor_total"))
                            .build();                    
                    reservas.add(reserva);
                }
            }
            conn = null;
        } catch (SQLException ex) {
            Logger.getLogger(ReservaDAOMariaDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return reservas;
    }

    @Override
    public Reserva atualizar(Reserva reservaAntiga, Reserva reservaAtual) {
        Connection conn = conexao.novaConexao();
        PreparedStatement sql;
        
        try {
            sql = conn.prepareStatement("UPDATE reserva "
                    + "SET res_usr_id=?,"
                    + "res_cat_id=?,"
                    + "res_car_id=?,"
                    + "res_dia_final=?,"
                    + "res_dia_inicio=?, "
                    + "res_status=?, "
                    + "res_valor_total=?"
                    + " WHERE res_id=?");
            sql.setInt(1, reservaAtual.getIdUsuario());
            sql.setInt(2, reservaAtual.getIdCategoria());
            sql.setInt(3, reservaAtual.getIdCarro());
            sql.setDate(4, reservaAtual.getDiaFinal());
            sql.setDate(5, reservaAtual.getDiaInicio());
            sql.setString(6, reservaAtual.getStatus());
            sql.setDouble(7, reservaAtual.getValorTotal());
            sql.setInt(8, reservaAntiga.getIdReserva());
            sql.executeUpdate();
            sql.close();
            
            conn = null;
        } catch(SQLException e) {
            Logger.getLogger(ReservaDAOMariaDB.class.getName()).log(Level.SEVERE, null, e);
        }
        
        return encontrarPorId(reservaAntiga.getIdReserva());
    }

    @Override
    public boolean deletar(Integer idReserva) {
        Connection con = conexao.novaConexao();
        PreparedStatement sql;
        boolean resultado = false;
        
        try {
            sql = con.prepareStatement("DELETE FROM reserva WHERE res_id = ?");
            sql.setInt(1, idReserva);
            sql.execute();
            sql.close();
            Reserva reserva = encontrarPorId(idReserva);
            if(reserva == null)
                resultado = true;
            con = null;
        } catch (SQLException e) {
            Logger.getLogger(ReservaDAOMariaDB.class.getName()).log(Level.SEVERE, null, e);
        }
        return resultado;
    }
    
    @Override
    public List<Reserva> encontrarReservasDisponiveis() {
        Connection conn = conexao.novaConexao();
        PreparedStatement sql;
        ResultSet rs = null;
        List<Reserva> reservas = null;
        try {
            sql = conn.prepareStatement("SELECT "
                    + "res_id, "
                    + "res_usr_id,"
                    + "res_cat_id,"
                    + "res_car_id,"
                    + "res_dia_final,"
                    + "res_dia_inicio,"
                    + "res_status,"
                    + "res_valor_total"
                    + " FROM reserva "
                    + "WHERE res_status = 'PENDENTE'"
            );
            rs = sql.executeQuery();
            if (rs != null) {
                reservas = new ArrayList<>();
                while (rs.next()) {
                    Reserva reserva = ReservaBuilder.getBuilder()
                        .adicionaIdReserva(rs.getInt("res_id"))
                        .adicionaIdUsuario(rs.getInt("res_usr_id"))
                        .adicionaIdCategoria(rs.getInt("res_cat_id"))
                        .adicionaIdCarro(rs.getInt("res_car_id"))
                        .adicionaDiaFinal(rs.getDate("res_dia_final"))
                        .adicionaDiaInicio(rs.getDate("res_dia_inicio"))
                        .adicionaStatus(rs.getString("res_status"))
                        .adicionaValorTotal(rs.getDouble("res_valor_total"))
                        .build();
                    reservas.add(reserva);
                }
            }
            conn = null;
        } catch(SQLException e) {
            Logger.getLogger(AluguelDAOMariaDB.class.getName()).log(Level.SEVERE, null, e);
        }
        return reservas;
    }
}
