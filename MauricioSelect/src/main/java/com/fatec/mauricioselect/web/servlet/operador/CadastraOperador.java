package com.fatec.mauricioselect.web.servlet.operador;

import com.fatec.mauricioselect.api.modelo.Operador;
import com.fatec.mauricioselect.core.servico.ImplOperadorServico;
import java.io.IOException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author felipe
 */
@WebServlet(name = "CadastraOperador", urlPatterns = {"/cadastro/operador"})
public class CadastraOperador extends HttpServlet {
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        ServletContext context = req.getServletContext();
        ImplOperadorServico operadorServico = new ImplOperadorServico();
        String status = "nao";
        String nomeUsuario = req.getParameter("nomeUsuario");
        String cargoUsuario = req.getParameter("cargoUsuario");
        Operador operador;
        
        if(nomeUsuario != null && cargoUsuario != null){
            operador = new Operador(nomeUsuario, cargoUsuario);
            if (operadorServico.inserir(operador) != null) {
                status = "sim";
            } 
        }
        req.setAttribute("operadorCadastrado", status);
        context.getRequestDispatcher("/jsp/admin/gerenciaOperador.jsp")
                                                .forward(req, resp);   
    }
}
