package com.fatec.mauricioselect.web.servlet.aluguel;

import com.fatec.mauricioselect.api.modelo.Aluguel;
import com.fatec.mauricioselect.api.modelo.AluguelBuilder;
import com.fatec.mauricioselect.api.modelo.Usuario;
import com.fatec.mauricioselect.api.servico.AluguelServico;
import com.fatec.mauricioselect.core.servico.ImplAluguelServico;
import com.fatec.mauricioselect.core.servico.ImplUsuarioServico;
import com.fatec.mauricioselect.web.servlet.utilitarios.RespostaJSON;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/aluguel/atualiza")
public class AtualizaAluguel extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        Aluguel aluguel = null;        
        AluguelServico aluguelServico = new ImplAluguelServico();
        String mensagemDeRetorno = 
                RespostaJSON.formataJson("Atualização feita com sucesso!", false);
        
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

        try {
            java.util.Date dataFinal = simpleDateFormat
                                    .parse(request.getParameter("dataFinal"));
            String status = request.getParameter("status");
            Integer idReserva = Integer.parseInt(
                                    request.getParameter("idReserva"));
            Integer idAluguel = Integer.parseInt(
                                    request.getParameter("idAluguel"));
            Double valorTotalDaReserva = Double
                                        .parseDouble(request
                                        .getParameter("valorTotalDaReserva"));
            
            aluguel = new Aluguel(idAluguel, idReserva, 
                                new java.sql.Date(dataFinal.getTime()),
                                            valorTotalDaReserva, status);
            
            aluguelServico.atualizar(aluguel);
        } catch (ParseException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            mensagemDeRetorno = RespostaJSON
                    .formataJson(String
                            .format("Não foi possível atualizar: %s", e.getMessage()), true);
        }        
        RespostaJSON.respondeJSON(response, mensagemDeRetorno);
    }
}