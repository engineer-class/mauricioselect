package com.fatec.mauricioselect.web.servlet.carro;

import com.fatec.mauricioselect.api.modelo.Carro;
import com.fatec.mauricioselect.api.servico.CarroServico;
import com.fatec.mauricioselect.core.servico.ImplCarroServico;
import com.fatec.mauricioselect.web.servlet.utilitarios.RespostaJSON;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Servlet com a rota para a deleção de carros
 */
@WebServlet(name = "DeletarCarros", urlPatterns = {"/carro/deletar"})
public class DeletarCarro extends HttpServlet {
    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        String mensagemDeRetorno = 
                RespostaJSON.formataJson("Carro excluído com sucesso!", false);

        CarroServico carroServico = new ImplCarroServico();
        Integer idCarro = Integer.parseInt(req.getParameter("idCarro"));
        Boolean processo = false;
        try {
            processo = carroServico.deleta(new Carro(idCarro));
        } catch (Exception ex) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            mensagemDeRetorno = RespostaJSON.formataJson(ex.getMessage(), true);
            Logger.getLogger(ListarCarrosPorCategoria.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(!processo)
            mensagemDeRetorno = RespostaJSON.formataJson("O carro não pode ser excluído", true);
            
        RespostaJSON.respondeJSON(resp, mensagemDeRetorno);
    }   
}
