package com.fatec.mauricioselect.web.servlet.carro;

import com.fatec.mauricioselect.api.modelo.categorico.CarroCategorico;
import com.fatec.mauricioselect.core.servico.ImplCarroServico;
import com.fatec.mauricioselect.web.servlet.utilitarios.RespostaJSON;
import com.google.gson.Gson;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet para a listagem dos carros disponíveis de uma determinada categoria
 */
@WebServlet(name = "ListarCarros", urlPatterns = {"/carro/listar/categoria"})
public class ListarCarrosPorCategoria extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String nomeCategoria = req.getParameter("categoria");

        ImplCarroServico implCarroServico = new ImplCarroServico();
        List<CarroCategorico> listaCarros = implCarroServico
                                            .encontrarCarrosDisponiveisPorCategoria(nomeCategoria);

        if (listaCarros.isEmpty()) {
            RespostaJSON.respondeJSON(resp, 
                    RespostaJSON.formataJson("Não há carros nesta caregoria", true));
        } else {
            RespostaJSON.respondeJSON(resp, new Gson().toJsonTree(listaCarros).toString());
        }
    }
}
