package com.fatec.mauricioselect.web.servlet;

import com.fatec.mauricioselect.api.modelo.Usuario;
import com.fatec.mauricioselect.api.servico.EmailServico;
import com.fatec.mauricioselect.api.servico.UsuarioServico;
import com.fatec.mauricioselect.core.servico.ImplUsuarioServico;
import com.fatec.mauricioselect.core.servico.email.ImplEmailGoogleServidoFactory;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "RecuperarSenha", urlPatterns = {"/recuperarSenha"})
public class RecuperarSenha extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        ServletContext context = req.getServletContext();
        
        context.getRequestDispatcher("/jsp/recuperaSenha.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) 
            throws ServletException, IOException {

        req.setAttribute("dadosInvalidos", "nao");
        
        ServletContext context = req.getServletContext();
        String emailUsuario = req.getParameter("emailUsuario");
        
        // Criando o objeto para o envio de email
        UsuarioServico usuarioServico = new ImplUsuarioServico();
        EmailServico implEmailGoogleServico = ImplEmailGoogleServidoFactory
                                                        .fabricaServicoDeEmail();
        
        try {
            Usuario usuario = usuarioServico.encontrarPorEmail(emailUsuario);
            implEmailGoogleServico.enviaEmailDeRecuperacao(usuario);
        } catch (RuntimeException ex) {
            req.setAttribute("dadosInvalidos", "sim");
            req.setAttribute("mensagemDeErro", ex.getMessage());
        }
        
        req.setAttribute("formularioEnviado", "sim");
        context.getRequestDispatcher("/jsp/recuperaSenha.jsp").forward(req, resp);
    }
}
