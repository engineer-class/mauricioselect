package com.fatec.mauricioselect.web.servlet.reserva;

import com.fatec.mauricioselect.api.modelo.Aluguel;
import com.fatec.mauricioselect.api.modelo.AluguelBuilder;
import com.fatec.mauricioselect.api.modelo.Categoria;
import com.fatec.mauricioselect.api.modelo.Reserva;
import com.fatec.mauricioselect.api.modelo.ReservaBuilder;
import com.fatec.mauricioselect.api.modelo.Usuario;
import com.fatec.mauricioselect.api.modelo.UsuarioBuilder;
import com.fatec.mauricioselect.api.modelo.categorico.AluguelCategorico;
import com.fatec.mauricioselect.api.modelo.categorico.ReservaCategorica;
import com.fatec.mauricioselect.api.servico.AluguelServico;
import com.fatec.mauricioselect.api.servico.CategoriaServico;
import com.fatec.mauricioselect.api.servico.ReservaServico;
import com.fatec.mauricioselect.api.servico.UsuarioServico;
import com.fatec.mauricioselect.core.servico.ImplAluguelServico;
import com.fatec.mauricioselect.core.servico.ImplCategoriaServico;
import com.fatec.mauricioselect.core.servico.ImplReservaServico;
import com.fatec.mauricioselect.core.servico.ImplUsuarioServico;
import com.fatec.mauricioselect.web.servlet.usuario.ReservaDeCarro;
import com.fatec.mauricioselect.web.servlet.utilitarios.RespostaJSON;
import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = {"/reserva/aprova"})
public class AprovarReserva extends HttpServlet {

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession sessao = req.getSession();

        String mensagemResposta = RespostaJSON.formataJson("Reserva aprovada com sucesso, aluguel criado", false);
        String pattern = "yyyy-MM-dd";

        String operador = (String) sessao.getAttribute("usuarioAutenticado");

        CategoriaServico categoriaServico = new ImplCategoriaServico();
        UsuarioServico usuarioServico = new ImplUsuarioServico();
        ReservaServico reservaServico = new ImplReservaServico();
        AluguelServico aluguelServico = new ImplAluguelServico();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

        Categoria categoria = categoriaServico.encontrarPorNome(req.getParameter("carro[idCategoria]"));
        java.util.Date dataInicial = null, dataFinal = null;
        AluguelCategorico aluguelInserido = null;

        Reserva reservaNova = null;
        ReservaCategorica reservaAntiga = null;
        Aluguel aluguel = null;

        Usuario usuario = UsuarioBuilder
                .getBuilder()
                .adicionaNome(req.getParameter("usuario[nome]"))
                .adicionaApelido(req.getParameter("usuario[apelido]"))
                .adicionaCPF(req.getParameter("usuario[cpf]"))
                .adicionaRG(req.getParameter("usuario[rg]"))
                .adicionaEmail(req.getParameter("usuario[email]"))
                .adicionaSexo(req.getParameter("usuario[sexo]"))
                .adicionaID(Integer.parseInt(req.getParameter("usuario[idUsuario]")))
                .build();

        try {

            dataInicial = simpleDateFormat.parse(req.getParameter("reserva[dataInicio]"));
            dataFinal = simpleDateFormat.parse(req.getParameter("reserva[dataFinal]"));

            reservaNova = ReservaBuilder
                    .getBuilder()
                    .adicionaIdCarro(Integer.parseInt(req.getParameter("carro[idCarro]")))
                    .adicionaIdUsuario(usuario.getIdUsuario())
                    .adicionaIdCategoria(categoria.getIdCategoria())
                    .adicionaIdReserva(Integer.parseInt(req.getParameter("reserva[idReserva]")))
                    .adicionaValorTotal(Double.parseDouble(req.getParameter("reserva[valorTotal]")))
                    .adicionaStatus(req.getParameter("reserva[status]"))
                    .adicionaDiaFinal(new Date(dataFinal.getTime()))
                    .adicionaDiaInicio(new Date(dataInicial.getTime()))
                    .build();

            reservaAntiga = reservaServico.encontrarPorIdReserva(reservaNova.getIdReserva());

            aluguel = AluguelBuilder.getBuilder()
                    .adicionaIdOperador(usuarioServico.encontrarPorApelido(operador).getIdUsuario())
                    .adicionaIdUsuario(usuario.getIdUsuario())
                    .adicionaIdReserva(reservaNova.getIdReserva())
                    .adicionaIdCarro(reservaNova.getIdCarro())
                    .adicionaValorTotal(reservaNova.getValorTotal())
                    .adicionaValorPagoAntecipado(Double.parseDouble(
                            req.getParameter("aluguel[valorPagoAntecipado]")))
                    .adicionaDataRetirada(new Date(dataInicial.getTime()))
                    .adicionaDataDevolucao(new Date(dataFinal.getTime()))
                    .adicionaStatus("ALUGADO")
                    .build();

            reservaServico.atualizaReserva(reservaNova);
            aluguelInserido = aluguelServico.inserirAluguel(aluguel);

        } catch (ParseException ex) {
            
            Reserva reserva = new Reserva(
                    reservaAntiga.getIdReserva(),
                    reservaAntiga.getCarro().getIdCarro(), 
                    reservaAntiga.getUsuario().getIdUsuario(),
                    reservaAntiga.getCategoria().getIdCategoria(),
                    reservaAntiga.getDiaFinal(),
                    reservaAntiga.getDiaInicio(), reservaAntiga.getStatus(), 
                    reservaAntiga.getValorTotal()
            );

            reservaServico.atualizaReserva(reserva);
            Logger.getLogger(ReservaDeCarro.class.getName()).log(Level.SEVERE, null, ex);
            mensagemResposta = RespostaJSON.formataJson(String.format("Reserva não pode ser aprovada %s", ex.getMessage()), true);
        }

        if (aluguelInserido == null) {
            mensagemResposta = RespostaJSON.formataJson("Esta reserva já possui aluguel", true);
        }

        RespostaJSON.respondeJSON(resp, mensagemResposta);

    }

}
