package com.fatec.mauricioselect.web.servlet.reserva;

import com.fatec.mauricioselect.api.modelo.Reserva;
import com.fatec.mauricioselect.api.modelo.ReservaBuilder;
import com.fatec.mauricioselect.api.modelo.categorico.ReservaCategorica;
import com.fatec.mauricioselect.api.modelo.categorico.CarroCategorico;
import com.fatec.mauricioselect.api.servico.CarroServico;
import com.fatec.mauricioselect.api.servico.CategoriaServico;
import com.fatec.mauricioselect.api.servico.ReservaServico;
import com.fatec.mauricioselect.api.servico.UsuarioServico;
import com.fatec.mauricioselect.core.servico.ImplCarroServico;
import com.fatec.mauricioselect.core.servico.ImplCategoriaServico;
import com.fatec.mauricioselect.core.servico.ImplReservaServico;
import com.fatec.mauricioselect.core.servico.ImplUsuarioServico;
import com.fatec.mauricioselect.web.servlet.usuario.ReservaDeCarro;
import com.google.gson.Gson;
import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "AtualizaReserva", urlPatterns = {"/reserva/atualiza"})
public class AtualizaReserva extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        CategoriaServico categoriaServico = new ImplCategoriaServico();
        CarroServico carroServico = new ImplCarroServico();
        UsuarioServico usuarioServico = new ImplUsuarioServico();
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

        java.util.Date dataInicial = null, dataFinal = null;
        CarroCategorico carro = null;

        try {
            dataInicial = simpleDateFormat.parse(req.getParameter("dataInicial"));
            dataFinal = simpleDateFormat.parse(req.getParameter("dataFinal"));
        } catch (ParseException ex) {
            Logger.getLogger(ReservaDeCarro.class.getName()).log(Level.SEVERE, null, ex);
        }

        String modelo = req.getParameter("carro");
        List<CarroCategorico> carros = carroServico.encontrarPorCategoria(req.getParameter("idCategoria"));

        for (CarroCategorico c : carros) {
            if (c.getModelo().equals(modelo)) {
                carro = c;
            }
        }
        Integer idUsuario = usuarioServico.encontrarPorApelido(
                req.getParameter("idUsuario")
        ).getIdUsuario();
        Reserva reserva = ReservaBuilder.getBuilder()
                .adicionaIdUsuario(idUsuario)
                .adicionaIdReserva(Integer.valueOf(req.getParameter("idReserva")))
                .adicionaIdCategoria(categoriaServico.encontrarPorNome(req.getParameter("idCategoria")).getIdCategoria())
                .adicionaIdCarro(carro.getIdCarro())
                .adicionaStatus(req.getParameter("status"))
                .adicionaDiaFinal(new Date(dataFinal.getTime()))
                .adicionaDiaInicio(new Date(dataInicial.getTime()))
                .adicionaValorTotal(Double.valueOf(req.getParameter("valorTotal")))
                .build();

        ReservaServico reservaServico;
        ReservaCategorica reservaEncontrada;
        resp.setContentType("text/html");
        resp.setCharacterEncoding("UTF-8");

        if (reserva != null) {
            reservaServico = new ImplReservaServico();

            try {
                reservaEncontrada = reservaServico.encontrarPorIdReserva(reserva.getIdReserva());
                if (reservaEncontrada != null) {
                    reservaServico.atualizaReserva(reserva);
                    resp.getWriter().print("Reserva atualizada com sucesso");
                } else {
                    resp.getWriter().print("Reserva não encontrada");
                }
            } catch (NumberFormatException e) {
                resp.getWriter().print("Digite um ID válido " + e.getMessage());
            }
        } else {
            resp.getWriter().print("ID inválido");
        }
    }

}
