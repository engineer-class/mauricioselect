package com.fatec.mauricioselect.web.servlet.fabricante;

import com.fatec.mauricioselect.api.modelo.Fabricante;
import com.fatec.mauricioselect.api.servico.FabricanteServico;
import com.fatec.mauricioselect.core.servico.ImplFabricanteServico;
import com.fatec.mauricioselect.web.servlet.utilitarios.RespostaJSON;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet com a rota para atualizar o fabricante por nome
 */
@WebServlet(name = "AtualizarFabricantesPorNome", urlPatterns = {"/fabricante/atualizar"})
public class AtualizarFabricante extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) 
            throws ServletException, IOException {
        
        String mensagemDeRetorno = 
                RespostaJSON.formataJson("Atualização feita com sucesso!", false);
        String nomeFabricante = req.getParameter("nomeFabricante");
        Integer idFabricante = Integer.parseInt(req.getParameter("idFabricante"));
                
        FabricanteServico fabricanteServico = new ImplFabricanteServico();
        try {
            fabricanteServico.atualizar(new Fabricante(idFabricante, nomeFabricante));
        } catch (RuntimeException ex) {
            mensagemDeRetorno = RespostaJSON.formataJson(ex.getMessage(), true);
        }
        
        RespostaJSON.respondeJSON(resp, mensagemDeRetorno);
    }  
}
