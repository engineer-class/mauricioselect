package com.fatec.mauricioselect.web.servlet.categoria;

import com.fatec.mauricioselect.api.modelo.Categoria;
import com.fatec.mauricioselect.api.servico.CategoriaServico;
import com.fatec.mauricioselect.core.servico.ImplCategoriaServico;
import com.fatec.mauricioselect.web.servlet.utilitarios.RespostaJSON;
import com.google.gson.Gson;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/categoria/lista/nome"})
public class ListarCategoriaPorNome extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String nome = req.getParameter("nome");
        
        CategoriaServico implCategoriaServico = new ImplCategoriaServico();
        Categoria categoria = implCategoriaServico.encontrarPorNome(nome);
             
        if (categoria == null) {
            RespostaJSON.respondeJSON(resp, 
                    RespostaJSON.formataJson("Nenhuma categoria foi encontrada", true));
        } else {
           RespostaJSON.respondeJSON(resp, new Gson().toJson(categoria)); 
        }
    }

}