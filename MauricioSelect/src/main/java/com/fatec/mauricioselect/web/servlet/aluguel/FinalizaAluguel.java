package com.fatec.mauricioselect.web.servlet.aluguel;

import com.fatec.mauricioselect.api.modelo.categorico.AluguelCategorico;
import com.fatec.mauricioselect.api.servico.AluguelServico;
import com.fatec.mauricioselect.core.servico.ImplAluguelServico;
import com.fatec.mauricioselect.web.servlet.utilitarios.RespostaJSON;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet com a rota para a finalização de alugueis
 */
@WebServlet(urlPatterns = {"/aluguel/finaliza"})
public class FinalizaAluguel extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) 
            throws ServletException, IOException {
        
        String mensagemDeRetorno = RespostaJSON.formataJson("Finalização "
                                                + "realizada com sucesso", false);
        
        AluguelCategorico aluguel = null;
        AluguelServico aluguelServico = new ImplAluguelServico();
                        
        try {     
            aluguel = aluguelServico
                                .encontrarAluguelPorId(Integer
                                .parseInt(req.getParameter("idAluguel")));

            aluguel.getCarro().setEstaQuebrado(Integer
                                .parseInt(req.getParameter("carroEstaQuebrado")));
            aluguel.getCarro().setQuilometragem(aluguel.getCarro().getQuilometragem() 
                    + Double.parseDouble(req.getParameter("carroQuilometragem")));
            aluguel.getCarro().setTanque(Integer
                                .parseInt(req.getParameter("carroTanque")));
            aluguel.getCarro().setEstadoConserva(req.getParameter("conservacao"));
            aluguel.setValorTotal(Double
                                .parseDouble(req.getParameter("valorTotalDaReserva")));
            aluguel.setStatus(req.getParameter("status"));
            aluguel.setDataDevolucao(new java.sql.Date(new java.util.Date().getTime()));
        } catch (NumberFormatException e) {
            mensagemDeRetorno = RespostaJSON.formataJson("Preencha todos "
                    + "os campos para finalizar a reseva!", true);
        }
        
        try {
            boolean atualizadoComSucesso = aluguelServico
                                                    .finalizaAluguel(aluguel);
            if (!atualizadoComSucesso) {
                mensagemDeRetorno = RespostaJSON
                                        .formataJson("Houveram problemas ao "
                                                + "atualizar", true);
            }
        } catch (RuntimeException e) {
            mensagemDeRetorno = RespostaJSON.formataJson(e.getMessage(), true);
        }
      
        RespostaJSON.respondeJSON(resp, mensagemDeRetorno);
    }
}
