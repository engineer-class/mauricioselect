package com.fatec.mauricioselect.web.servlet.utilitarios;

import com.fatec.mauricioselect.api.modelo.CarroBuilder;
import com.fatec.mauricioselect.api.modelo.Categoria;
import com.fatec.mauricioselect.api.modelo.ReservaBuilder;
import com.fatec.mauricioselect.api.servico.CategoriaServico;
import com.fatec.mauricioselect.core.servico.ImplCategoriaServico;
import java.text.ParseException;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;


/**
 * Classe que facilita a recuperação de parâmetros nos diferentes contextos da
 * aplicação
 */
public class RecuperadorDeParametros {
    
    /**
     * Método para recuperar objetos de carrosBuilder a partir de parâmetros HTTP
     * Neste método os seguintes parâmetros são buscados:
     *  - Fabricante;
     *  - Categoria;
     *  - Conservação;
     *  - Placa do carro;
     *  - Modelo do carro;
     *  - Cor do carro;
     *  - Quilometragem;
     *  - Tanque do carro.
     * @param req
     * @return 
     */
    public static CarroBuilder recuperaCarroBuilderApartirDosParametros(
            HttpServletRequest req) {
        
        String estadoConservacao = req.getParameter("conservacao");
        String placaDoCarro = req.getParameter("placaDoCarro");
        String modeloDoCarro = req.getParameter("modeloDoCarro");
        String corDoCarro = req.getParameter("corDoCarro");
        Double quilometragem = Double.parseDouble(req.getParameter("quilometragem"));
        Integer tanqueDoCarro = Integer.parseInt(req.getParameter("tanqueDoCarro"));
        Integer anoDoCarro = Integer.parseInt(req.getParameter("anoDoCarro"));
        String estaQuebradoReq = req.getParameter("carroQuebrado");
        Integer estaQuebrado = 0;
        if("SIM".equals(estaQuebradoReq)){
            estaQuebrado = 1;
        }
        
        CarroBuilder carroBuilder = CarroBuilder.getBuilder()
                    .adicionaAno(anoDoCarro)
                    .adicionaCor(corDoCarro)
                    .adicionaEstadoConserva(estadoConservacao)
                    .adicionaModelo(modeloDoCarro)
                    .adicionaPlaca(placaDoCarro)
                    .adicionaQuilometragem(quilometragem)
                    .adicionaTanque(tanqueDoCarro)
                    .adicionaEstadoDoCarro(estaQuebrado)
                    .adicionaIDCarro(null);
                    
        return carroBuilder;
    }
    
    public static ReservaBuilder 
        recuperaReservaBuilderApartirDosParametros(HttpServletRequest req) throws ParseException {
        
        CategoriaServico categoriaServico = new ImplCategoriaServico();
        ReservaBuilder reservaBuilder = ReservaBuilder.getBuilder();
        
        Date dataInicial = null, dataFinal = null;
       
        dataInicial = ManipuladorDeData.stringParaDate(req.getParameter("dataInicial"));
        dataFinal = ManipuladorDeData.stringParaDate(req.getParameter("dataFinal"));
 
        // Recupera os valores da requisição
        Integer carroSelecionado = Integer.parseInt(req.getParameter("carroSelecionado"));
        String categoriaSelecionada = req.getParameter("categoriaSelecionado");
        Double valorTotal = Double.parseDouble(req.getParameter("valorTotal"));
                
        // Verificando se o ID inserido é válido
        Integer idReserva = Integer.parseInt(req.getParameter("idReserva"));

        // Busca as informações corretas de cada parâmetro nos serviços
        Categoria categoria = categoriaServico.encontrarPorNome(categoriaSelecionada);
        
        reservaBuilder = reservaBuilder
                .adicionaDiaInicio(new java.sql.Date(dataInicial.getTime()))
                .adicionaDiaFinal(new java.sql.Date(dataFinal.getTime()))
                .adicionaIdCarro(carroSelecionado)
                .adicionaIdCategoria(categoria.getIdCategoria())
                .adicionaIdReserva(idReserva)
                .adicionaValorTotal(valorTotal);
                
        return reservaBuilder;
    }
}
