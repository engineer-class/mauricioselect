package com.fatec.mauricioselect.web.servlet.carro;

import com.fatec.mauricioselect.api.modelo.categorico.CarroCategorico;
import com.fatec.mauricioselect.api.servico.CarroServico;
import com.fatec.mauricioselect.core.servico.ImplCarroServico;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/carro/listar"})
public class ListarCarro extends HttpServlet {
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        ServletContext context = request.getServletContext();
        
        CarroServico carroServico = new ImplCarroServico();           
        List<CarroCategorico> carros = carroServico.encontrarTodosOsCarros();
        
        request.setAttribute("carros", carros);        
        context.getRequestDispatcher("/jsp/operador/frotaAtual.jsp").forward(request, response);
    }
}
