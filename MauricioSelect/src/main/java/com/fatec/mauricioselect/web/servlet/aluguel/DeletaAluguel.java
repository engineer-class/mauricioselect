package com.fatec.mauricioselect.web.servlet.aluguel;

import com.fatec.mauricioselect.api.servico.AluguelServico;
import com.fatec.mauricioselect.core.servico.ImplAluguelServico;
import com.fatec.mauricioselect.web.servlet.utilitarios.RespostaJSON;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/aluguel/deleta"})
public class DeletaAluguel extends HttpServlet {

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        AluguelServico aluguelServico = new ImplAluguelServico();
        
        String mensagemDeRetorno = 
                RespostaJSON.formataJson("Deleção feita com sucesso!", false);
        
        Boolean operacao = false;
        try {
            operacao = aluguelServico.deletar(Integer.parseInt(request.getParameter("id")));
        } catch (NumberFormatException e) {
            mensagemDeRetorno = RespostaJSON.formataJson(String
                    .format("Não foi possível deletar: %s",e.getMessage()), true);
        }
        
        if(!operacao)
            mensagemDeRetorno = RespostaJSON
                    .formataJson(String.format("Não foi possível "
                            + "deletar o aluguel"), true);
        
        RespostaJSON.respondeJSON(response, mensagemDeRetorno);
    }
}
