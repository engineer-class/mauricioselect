package com.fatec.mauricioselect.web.servlet.reserva;

import com.fatec.mauricioselect.api.modelo.Categoria;
import com.fatec.mauricioselect.api.modelo.Reserva;
import com.fatec.mauricioselect.api.modelo.Usuario;
import com.fatec.mauricioselect.api.modelo.categorico.CarroCategorico;
import com.fatec.mauricioselect.api.servico.CarroServico;
import com.fatec.mauricioselect.api.servico.CategoriaServico;
import com.fatec.mauricioselect.api.servico.ReservaServico;
import com.fatec.mauricioselect.api.servico.UsuarioServico;
import com.fatec.mauricioselect.core.servico.ImplCarroServico;
import com.fatec.mauricioselect.core.servico.ImplCategoriaServico;
import com.fatec.mauricioselect.core.servico.ImplReservaServico;
import com.fatec.mauricioselect.core.servico.ImplUsuarioServico;
import com.fatec.mauricioselect.web.servlet.utilitarios.ManipuladorDeData;
import com.fatec.mauricioselect.web.servlet.utilitarios.RespostaJSON;
import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/reserva/cadastrar"})
public class CadastraReserva extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        ReservaServico reservaServico = new ImplReservaServico();
        UsuarioServico usuarioServico = new ImplUsuarioServico();
        CarroServico carroServico = new ImplCarroServico();
        CategoriaServico categoriaServico = new ImplCategoriaServico();

        CarroCategorico carro;
        String mensagemResposta = RespostaJSON.formataJson("Reserva criada", false);
        String apelido = req.getParameter("apelido");
        String modelo = req.getParameter("modeloCarro");

        java.util.Date dataInicial = null, dataFinal = null;
        try {
            dataFinal = ManipuladorDeData.stringParaDate(req.getParameter("dataFinal"));
            dataInicial = ManipuladorDeData.stringParaDate(req.getParameter("dataInicial"));

            Categoria categoria = categoriaServico.encontrarPorNome(req.getParameter("idCategoria"));
            Usuario usuario = usuarioServico.encontrarPorApelido(apelido);

            List<CarroCategorico> carros = carroServico.encontrarCarrosDisponiveis();

            Optional<CarroCategorico> carroOptional = carros.stream()
                    .filter(
                            c -> Objects.equals(c.getModelo(), modelo)
                    ).findFirst();

            if (carroOptional.isPresent()) {

                carro = carroOptional.get();

                if (Objects.equals(carro.getCategoria().getIdCategoria(), categoria.getIdCategoria())) {

                    Date diaFinal = new Date(dataFinal.getTime());
                    Date diaInicio = new Date(dataInicial.getTime());

                    Integer distancia = ManipuladorDeData.distanciaEntreAsDatas(diaInicio, diaFinal);

                    Reserva reserva = new Reserva(
                            usuario.getIdUsuario(), carro.getIdCarro(), categoria.getIdCategoria(),
                            diaFinal, diaInicio,
                            "PENDENTE", (distancia * 24) * categoria.getValorPorHora()
                    );

                    reservaServico.inserir(reserva);
                }
            } else {
                mensagemResposta = RespostaJSON.formataJson("Não foi possível criar a reserva, as categorias não são as mesmas", true);
            }
        } catch (Exception e) {
            mensagemResposta = RespostaJSON.formataJson(String.format("Não foi possível criar a reserva %s", e), true);
        }

        RespostaJSON.respondeJSON(resp, mensagemResposta);

    }

}
