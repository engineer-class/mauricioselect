package com.fatec.mauricioselect.web.servlet.operador;

import com.fatec.mauricioselect.api.modelo.Operador;
import com.fatec.mauricioselect.api.servico.OperadorServico;
import com.fatec.mauricioselect.core.servico.ImplOperadorServico;
import java.io.IOException;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "CadastroOperador", urlPatterns = {"/gerencia/operador"})
public class GerenciaOperador extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
            throws ServletException, IOException {
        ServletContext context = req.getServletContext();
        
        context.getRequestDispatcher("/jsp/admin/gerenciaOperador.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        resp.setContentType("text/html");
        resp.setCharacterEncoding("UTF-8");
            
        OperadorServico operadorServico = new ImplOperadorServico();
        
        
        String nomeOperador = null;
        Operador operador = null;
        
        try{
            nomeOperador = req.getParameter("nomeUsuario");
            operador = operadorServico.encontrarPorApelido(nomeOperador);
            
        }catch(NumberFormatException e){
            Logger.getLogger(GerenciaOperador.class.getName(), "Nome de usuário inválido: "+e.getMessage());
        }
        
        if(operador != null){
        
            if(!operadorServico.deleta(operador)){
                resp.getWriter().println("Operador "+nomeOperador+ " foi excluído com sucesso");
            }else{
                resp.getWriter().println("Ocorreu algum problema ao excluir o operador "+nomeOperador);
            }
        }else{
            resp.getWriter().println("Operador "+nomeOperador+ " não foi encontrado");
        }
    }
    
}
