package com.fatec.mauricioselect.web.servlet.categoria;

import com.fatec.mauricioselect.api.modelo.Categoria;
import com.fatec.mauricioselect.api.servico.CategoriaServico;
import com.fatec.mauricioselect.core.servico.ImplCategoriaServico;
import com.fatec.mauricioselect.web.servlet.utilitarios.RespostaJSON;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet com a rota para a deleção de categorias
 */
@WebServlet(name = "DeletarCategorias", urlPatterns = {"/categoria/deletar"})
public class DeletarCategoria extends HttpServlet {
    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        String mensagemDeRetorno = 
                RespostaJSON.formataJson("A categoria foi excluída com sucesso!", false);
        CategoriaServico categoriaServico = new ImplCategoriaServico();
        
        Integer idCategoria = Integer.parseInt(req.getParameter("idCategoria"));
        Boolean resultado = categoriaServico.deleta(new Categoria(idCategoria));
        
        if (!resultado) {
            mensagemDeRetorno = 
                    RespostaJSON.formataJson("Não foi possível deletar a categoria", true);
        }
        
        RespostaJSON.respondeJSON(resp, mensagemDeRetorno);
    }   
}
