package com.fatec.mauricioselect.web.servlet.fabricante;

import java.io.IOException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet com a rota para a página principal de gerenciamento de fabricante
 */
@WebServlet(name = "PrincipalFabricante", urlPatterns = {"/fabricante"})
public class PaginaDoFabricante extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletContext context = req.getServletContext();
        
        context.getRequestDispatcher("/jsp/fabricante/gerencia.jsp").forward(req, resp);
    }
}
