package com.fatec.mauricioselect.web.servlet.categoria;

import com.fatec.mauricioselect.api.modelo.Categoria;
import com.fatec.mauricioselect.api.servico.CategoriaServico;
import com.fatec.mauricioselect.core.servico.ImplCategoriaServico;
import com.fatec.mauricioselect.web.servlet.utilitarios.RespostaJSON;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet com a rota para a atualização de categoria
 */
@WebServlet(name = "AtualizarCategorias", urlPatterns = {"/categoria/atualizar"})
public class AtualizarCategoria extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) 
            throws ServletException, IOException {
        
        String mensagemDeRetorno = 
                RespostaJSON.formataJson("Atualização feita com sucesso!", false);
        CategoriaServico categoriaServico = new ImplCategoriaServico();
        
        String nomeCategoria = req.getParameter("nomeCategoria");
        Double valorHoraCategoria = Double.parseDouble(req.getParameter("valorHoraCategoria"));
        Integer idCategoria = Integer.parseInt(req.getParameter("idCategoria"));
        
        try {
            categoriaServico.atualizarCategoria(
                    new Categoria(nomeCategoria, idCategoria, valorHoraCategoria));
        } catch (RuntimeException ex) {
            mensagemDeRetorno = RespostaJSON.formataJson(ex.getMessage(), true);
        }
        
        RespostaJSON.respondeJSON(resp, mensagemDeRetorno);
    }
}
