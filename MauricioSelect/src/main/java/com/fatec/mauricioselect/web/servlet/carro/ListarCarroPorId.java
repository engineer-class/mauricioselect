package com.fatec.mauricioselect.web.servlet.carro;

import com.fatec.mauricioselect.api.modelo.categorico.CarroCategorico;
import com.fatec.mauricioselect.api.servico.CarroServico;
import com.fatec.mauricioselect.core.servico.ImplCarroServico;
import com.fatec.mauricioselect.web.servlet.utilitarios.RespostaJSON;
import com.google.gson.Gson;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/carro/lista/id"})
public class ListarCarroPorId extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        Integer idCarro = Integer.parseInt(req.getParameter("id"));
        
        CarroServico implCarroServico = new ImplCarroServico();
        CarroCategorico carro = implCarroServico.encontrarPorId(idCarro);

        if (carro == null) {
            RespostaJSON.respondeJSON(resp, 
                    RespostaJSON.formataJson("Nenhum carro foi encontrado", true));
        } else {
           RespostaJSON.respondeJSON(resp, new Gson().toJson(carro)); 
        }
    }
}