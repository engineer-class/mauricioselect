package com.fatec.mauricioselect.web.servlet;

import java.io.IOException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "Sair", urlPatterns = {"/sair"})
public class Sair extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
            throws ServletException, IOException {
        
        ServletContext contexto = req.getServletContext();
        HttpSession sessao = req.getSession();
        sessao.setAttribute("usuarioAutenticado", null);
        sessao.setAttribute("tipoUsuario", null);
        contexto.getRequestDispatcher("/index.jsp").forward(req, resp);
    } 
}
