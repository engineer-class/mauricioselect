package com.fatec.mauricioselect.web.servlet.utilitarios;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class ManipuladorDeData {
    
    /**
     * Método para transformar String em Date
     * @param data
     * @return 
     * @throws java.text.ParseException 
     */
    public static Date stringParaDate(String data) throws ParseException {
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        
        return simpleDateFormat.parse(data);
    }
    
    /**
     * Método para recuperar a data atual do sistema
     * @return 
     */
    public static String dataAtualDoSistema() {
        DateFormat formatoDesejado = new SimpleDateFormat("dd/MM/yyyy");
        
        return formatoDesejado.format(new Date());
    }
        
    /**
     * Método para verificar se data está correta (Ou igual ao do servidor NTP)
     * @return 
     */
    public static boolean dataEstaIgualANTP() {
        DateFormat formatoDesejado = new SimpleDateFormat("dd/MM/yyyy");
        Date dataNTP = ClienteNTP.recuperaDataAtual();
     
        // Transforma as datas em strings
        String dataDoNTP = formatoDesejado.format(dataNTP);
        String dataDoSistema = dataAtualDoSistema();

        return dataDoNTP.equals(dataDoSistema);
    }
    
    /**
     * Método para verificar a distância entre duas datas
     * @param dataInicial
     * @param dataFinal
     * @return 
     */
    public static Integer distanciaEntreAsDatas(Date dataInicial, Date dataFinal) {
        
        long dataInicialLong = dataInicial.getTime();
        long dataFinalLong = dataFinal.getTime();
        long distancia = dataFinalLong - dataInicialLong;
        Long distanciaEmDias = TimeUnit.DAYS.convert(distancia, TimeUnit.MILLISECONDS);
                
        if (distanciaEmDias.intValue() > 0) {
            return distanciaEmDias.intValue();
        } else {
            return 1;
        }
    }
    
    /**
     * Método para verificar a distância entre duas datas
     * @param dataInicial
     * @param dataFinal
     * @return 
     */
    public static Integer distanciaEntreAsDatas(java.sql.Date dataInicial, 
                                            java.sql.Date dataFinal) {

        long dataInicialLong = dataInicial.getTime();
        long dataFinalLong = dataFinal.getTime();
        long distancia = dataFinalLong - dataInicialLong;
        Long distanciaEmDias = TimeUnit.DAYS.convert(distancia, TimeUnit.MILLISECONDS);
           
        if (distanciaEmDias.intValue() > 0) {
            return distanciaEmDias.intValue();
        } else {
            return 1;
        }
    }
}
