package com.fatec.mauricioselect.web.servlet;

import com.fatec.mauricioselect.core.servico.ImplOperadorServico;
import com.fatec.mauricioselect.core.servico.ImplUsuarioServico;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "AutenticadorDeUsuario", urlPatterns = {"/ValidaUsuario"})
public class AutenticadorDeUsuario extends HttpServlet {

    public static Map<String, String> usuariosLogados = new HashMap<>();
    
    /**
     * 
     * Esta classe faz o intermédio entre um modelo que faz a validação e a visão.
     * Os parametros usuario e senha são passados para o modelo e retornados conforme a validação do modelo.
     * O atributo "usuarioInvalido" é informado como sim e não para que o envio de páginas dinâmico seja feito no jsp login.jsp.
     * 
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException 
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
                
        ImplUsuarioServico usuarioServico = new ImplUsuarioServico();
        ImplOperadorServico operadorServico = new ImplOperadorServico();
        
        String nomeDeUsuario = request.getParameter("usuario");
        String senhaDoUsuario = request.getParameter("senha");
        ServletContext context = request.getServletContext();
    
        if (!usuarioServico.validaUsuario(nomeDeUsuario, senhaDoUsuario)){
            request.setAttribute("usuarioInvalido", "sim");
            context.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
        } else { 
            HttpSession sessao = request.getSession();
            sessao.setAttribute("usuarioAutenticado", nomeDeUsuario);

            if (operadorServico.usuarioEhOperador(nomeDeUsuario)) {
                sessao.setAttribute("tipoUsuario", "operador");
                response.sendRedirect("/html/operador-principal.html");
            } else {
                sessao.setAttribute("tipoUsuario", "usuario");
                response.sendRedirect("/html/usuario-principal.html");            
            }
        }
    }
}
