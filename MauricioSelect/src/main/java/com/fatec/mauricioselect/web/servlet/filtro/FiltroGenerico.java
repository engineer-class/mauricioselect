/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fatec.mauricioselect.web.servlet.filtro;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter(filterName = "filtraPaginasVisualizadasPorTodoTipoDeUsuario",
            urlPatterns = {"/jsp/*", "/sair/*", "/perfil/*"})
public class FiltroGenerico implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        
               
        HttpServletRequest requisicaoServlet = (HttpServletRequest) request;
        HttpServletResponse respostaServlet = (HttpServletResponse) response;
        HttpSession sessao = requisicaoServlet.getSession();
        
        String url = ((HttpServletRequest) request).getRequestURI();
        if (url.startsWith("/jsp/sobre.jsp")) {
            chain.doFilter(request, response);
        }else if (sessao.getAttribute("usuarioAutenticado") != null) {
            chain.doFilter(request, response);
        } else {
            respostaServlet.sendRedirect("/");
        }
    }

    @Override
    public void destroy() {
  
    }
    
}
