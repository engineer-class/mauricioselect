package com.fatec.mauricioselect.web.servlet.usuario;

import com.fatec.mauricioselect.api.modelo.Reserva;
import com.fatec.mauricioselect.api.servico.ReservaServico;
import com.fatec.mauricioselect.core.servico.ImplReservaServico;
import com.fatec.mauricioselect.web.servlet.utilitarios.RespostaJSON;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/reserva/excluir"})
public class DeletarReserva extends HttpServlet {

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        
        String mensagemDeRetorno = 
                RespostaJSON.formataJson("A reserva foi excluída com sucesso!", false);
        Integer idReserva = Integer.valueOf(req.getParameter("id"));
        ReservaServico reservaServico = new ImplReservaServico();
        Boolean operacao = reservaServico.deleta(new Reserva(idReserva));
        if(!operacao){
           mensagemDeRetorno = RespostaJSON.formataJson("A reserva não foi excluída", true);
        }
        RespostaJSON.respondeJSON(resp, mensagemDeRetorno);
    }

}
