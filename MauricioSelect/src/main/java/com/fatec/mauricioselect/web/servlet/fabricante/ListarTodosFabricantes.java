package com.fatec.mauricioselect.web.servlet.fabricante;

import com.fatec.mauricioselect.api.modelo.Fabricante;
import com.fatec.mauricioselect.core.servico.ImplFabricanteServico;
import com.fatec.mauricioselect.web.servlet.utilitarios.RespostaJSON;
import com.google.gson.Gson;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet com a rota para listar todos os fabricantes diponíveis
 */
@WebServlet(name = "ListarTodosFabricantes", urlPatterns = {"/fabricante/listar"})
public class ListarTodosFabricantes extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ImplFabricanteServico implFabricanteServico = new ImplFabricanteServico();
        List<Fabricante> todosOsFabricantes = implFabricanteServico.encontrarTudo();
            
        if (todosOsFabricantes.isEmpty()) {
            RespostaJSON.respondeJSON(resp, 
                    RespostaJSON.formataJson("Não há fabricantes disponíveis", true));
        } else {
           RespostaJSON.respondeJSON(resp, new Gson().toJsonTree(todosOsFabricantes).toString()); 
        }
    }
}
