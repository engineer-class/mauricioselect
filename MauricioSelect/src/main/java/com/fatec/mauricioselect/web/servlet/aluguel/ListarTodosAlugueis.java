package com.fatec.mauricioselect.web.servlet.aluguel;

import com.fatec.mauricioselect.api.modelo.categorico.AluguelCategorico;
import com.fatec.mauricioselect.api.servico.AluguelServico;
import com.fatec.mauricioselect.core.servico.ImplAluguelServico;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * Servlet com a rota para listar todos os alugueis
 */
@WebServlet(urlPatterns = {"/aluguel/listar/todos"})
public class ListarTodosAlugueis extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            ServletContext context = req.getServletContext();
        AluguelServico aluguelServico = new ImplAluguelServico();

        List<AluguelCategorico> aluguelCategoricos = aluguelServico
                                                    .encontrarTudo();
                
        req.setAttribute("alugueis", aluguelCategoricos);
        context.getRequestDispatcher("/jsp/operador/gerenciarAluguel.jsp").forward(req, resp);
    }    
}
