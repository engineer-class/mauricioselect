package com.fatec.mauricioselect.web.servlet.carro;

import com.fatec.mauricioselect.api.modelo.CarroBuilder;
import com.fatec.mauricioselect.api.modelo.Categoria;
import com.fatec.mauricioselect.api.modelo.Fabricante;
import com.fatec.mauricioselect.api.servico.CarroServico;
import com.fatec.mauricioselect.api.servico.CategoriaServico;
import com.fatec.mauricioselect.api.servico.FabricanteServico;
import com.fatec.mauricioselect.core.servico.ImplCarroServico;
import com.fatec.mauricioselect.core.servico.ImplCategoriaServico;
import com.fatec.mauricioselect.core.servico.ImplFabricanteServico;
import com.fatec.mauricioselect.web.servlet.utilitarios.RecuperadorDeParametros;
import com.fatec.mauricioselect.web.servlet.utilitarios.RespostaJSON;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet com a rota de atualização dos carros
 */
@WebServlet(name = "AtualizarCarros", urlPatterns = {"/carro/atualizar"})
public class AtualizarCarro extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        String mensagemDeRetorno
                = RespostaJSON.formataJson("Atualização feita com sucesso!", false);

        CarroServico carroServico = new ImplCarroServico();
        CategoriaServico categoriaServico = new ImplCategoriaServico();
        FabricanteServico fabricanteServico = new ImplFabricanteServico();

        // Buscando informações
        Categoria categoriaDoCarro = categoriaServico
                .encontrarPorNome(req.getParameter("categoria"));
        Fabricante fabricante = fabricanteServico
                .encontrarPorNome(req.getParameter("fabricante"));

        // Cria o objeto a partir dos parâmetros HTTP
        CarroBuilder carroBuilder = RecuperadorDeParametros
                .recuperaCarroBuilderApartirDosParametros(req);
        carroBuilder.adicionaIDFabricante(fabricante.getIdFabricante());
        carroBuilder.adicionaCategoria(categoriaDoCarro.getIdCategoria());
        carroBuilder.adicionaIDCarro(Integer.parseInt(req.getParameter("idCarro")));

        try {
            carroServico.atualizar(carroBuilder.build());
        } catch (Exception ex) {
            
            mensagemDeRetorno
                    = RespostaJSON.formataJson(String.format("Não foi possível atualizar o carro: %s", ex.getMessage()), true);
        }
        RespostaJSON.respondeJSON(resp, mensagemDeRetorno);
    }
}
