package com.fatec.mauricioselect.web.servlet.reserva;

import com.fatec.mauricioselect.api.modelo.Categoria;
import com.fatec.mauricioselect.api.modelo.Usuario;
import com.fatec.mauricioselect.api.modelo.categorico.CarroCategorico;
import com.fatec.mauricioselect.api.modelo.categorico.ReservaCategorica;
import com.fatec.mauricioselect.api.servico.CarroServico;
import com.fatec.mauricioselect.api.servico.CategoriaServico;
import com.fatec.mauricioselect.api.servico.ReservaServico;
import com.fatec.mauricioselect.api.servico.UsuarioServico;
import com.fatec.mauricioselect.core.servico.ImplCarroServico;
import com.fatec.mauricioselect.core.servico.ImplCategoriaServico;
import com.fatec.mauricioselect.core.servico.ImplReservaServico;
import com.fatec.mauricioselect.core.servico.ImplUsuarioServico;
import com.fatec.mauricioselect.web.servlet.usuario.ReservaDeCarro;
import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/reserva/aprova/id"})
public class PaginaAprovaReserva extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        ServletContext context = req.getServletContext();

        ReservaServico reservaServico = new ImplReservaServico();
        CategoriaServico categoriaServico = new ImplCategoriaServico();
        CarroServico carroServico = new ImplCarroServico();
        UsuarioServico usuarioServico = new ImplUsuarioServico();
        
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

        ReservaCategorica reserva = reservaServico
                .encontrarPorIdReserva(Integer.parseInt(req.getParameter("idReserva")));
        CarroCategorico carro = carroServico.encontrarPorId(reserva.getCarro().getIdCarro());
        Categoria categoria = categoriaServico.encontrarPorNome(req.getParameter("idCategoria"));
        java.util.Date dataInicial = null, dataFinal = null;

        try {
            dataInicial = simpleDateFormat.parse(req.getParameter("dataInicial"));
            dataFinal = simpleDateFormat.parse(req.getParameter("dataFinal"));
            reserva.setDiaFinal(new Date(dataFinal.getTime()));
            reserva.setDiaInicio(new Date(dataInicial.getTime()));

        } catch (ParseException ex) {
            Logger.getLogger(ReservaDeCarro.class.getName()).log(Level.SEVERE, null, ex);
        }

        Usuario usuario = usuarioServico.encontrarPorApelido(
                req.getParameter("apelido")
        );

        reserva.setStatus(req.getParameter("status"));
        reserva.setValorTotal(Double.valueOf(req.getParameter("valorTotal")));
        reserva.getCategoria().setIdCategoria(categoria.getIdCategoria());
        reserva.getUsuario().setIdUsuario(usuario.getIdUsuario());

        req.setAttribute("reserva", reserva);
        req.setAttribute("carro", carro);
        req.setAttribute("usuario", usuario);

        context.getRequestDispatcher("/jsp/operador/aprovaReserva.jsp").forward(req, resp);

    }

}
