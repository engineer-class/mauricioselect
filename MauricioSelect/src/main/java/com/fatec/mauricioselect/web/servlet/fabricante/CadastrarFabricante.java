package com.fatec.mauricioselect.web.servlet.fabricante;

import com.fatec.mauricioselect.api.modelo.Fabricante;
import com.fatec.mauricioselect.api.servico.FabricanteServico;
import com.fatec.mauricioselect.core.servico.ImplFabricanteServico;
import com.fatec.mauricioselect.web.servlet.utilitarios.RespostaJSON;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet com a rota de cadastro de fabricantes
 */
@WebServlet(name = "CadastrarFabricantes", urlPatterns = {"/fabricante/cadastrar"})
public class CadastrarFabricante extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) 
            throws ServletException, IOException {
        
        String mensagemDeRetorno = 
                RespostaJSON.formataJson("O fabricante foi cadastrado com sucesso", false);
        String nomeFabricante = req.getParameter("nomeFabricante");
        
        FabricanteServico fabricanteServico = new ImplFabricanteServico();
        try {
            fabricanteServico.inserir(new Fabricante(nomeFabricante));
        } catch (RuntimeException ex) {
            mensagemDeRetorno = RespostaJSON.formataJson(ex.getMessage(), true);
        }
        
        RespostaJSON.respondeJSON(resp, mensagemDeRetorno);
    }
}
