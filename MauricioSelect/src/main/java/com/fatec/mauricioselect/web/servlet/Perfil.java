/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fatec.mauricioselect.web.servlet;

import com.fatec.mauricioselect.api.modelo.Usuario;
import com.fatec.mauricioselect.api.servico.UsuarioServico;
import com.fatec.mauricioselect.core.servico.ImplUsuarioServico;
import java.io.IOException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "Perfil", urlPatterns = {"/perfil"})
public class Perfil extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletContext context = req.getServletContext();
        UsuarioServico usuarioServico = new ImplUsuarioServico();
        
        // Recuperando os dados do usuário
        String apelidoUsuario = req.getParameter("apelido");
        String emailUsuario = req.getParameter("email");
        String rgUsuario = req.getParameter("rg");
        String cpfUsuario = req.getParameter("cpf");
        String nomeUsuario = req.getParameter("nome");
        String senhaUsuario = req.getParameter("senha");
               
        Usuario usuario = new Usuario(apelidoUsuario, 
                            emailUsuario, rgUsuario, cpfUsuario, nomeUsuario);
        
        if (senhaUsuario != null) {
            usuario.setSenha(senhaUsuario);
        }
        
        Usuario usuarioAtualizado = usuarioServico.atualizaUsuario(usuario);        
        req.setAttribute("usuario", usuarioAtualizado);
        context.getRequestDispatcher("/jsp/templates/perfil.jsp").forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
            throws ServletException, IOException {
        ServletContext context = req.getServletContext();
        UsuarioServico usuarioServico = new ImplUsuarioServico();
        HttpSession sessao = req.getSession();
        
        Usuario usuarioAutenticado = usuarioServico
                            .encontrarPorApelido((String) sessao.getAttribute("usuarioAutenticado"));
        req.setAttribute("usuario", usuarioAutenticado);
        context.getRequestDispatcher("/jsp/templates/perfil.jsp").forward(req, resp);
    }   
}
