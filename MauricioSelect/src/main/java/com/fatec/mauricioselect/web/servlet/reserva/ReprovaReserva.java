package com.fatec.mauricioselect.web.servlet.reserva;

import com.fatec.mauricioselect.api.modelo.Categoria;
import com.fatec.mauricioselect.api.modelo.Reserva;
import com.fatec.mauricioselect.api.modelo.ReservaBuilder;
import com.fatec.mauricioselect.api.modelo.Usuario;
import com.fatec.mauricioselect.api.modelo.UsuarioBuilder;
import com.fatec.mauricioselect.api.servico.CategoriaServico;
import com.fatec.mauricioselect.api.servico.ReservaServico;
import com.fatec.mauricioselect.core.servico.ImplCategoriaServico;
import com.fatec.mauricioselect.core.servico.ImplReservaServico;
import com.fatec.mauricioselect.web.servlet.usuario.ReservaDeCarro;
import com.fatec.mauricioselect.web.servlet.utilitarios.RespostaJSON;
import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/reserva/reprova/pendente"})
public class ReprovaReserva extends HttpServlet {

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String mensagemResposta = RespostaJSON.formataJson("Reserva atualizada com sucesso", false);
        String pattern = "yyyy-MM-dd";

        CategoriaServico categoriaServico = new ImplCategoriaServico();
        ReservaServico reservaServico = new ImplReservaServico();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

        Categoria categoria = categoriaServico.encontrarPorNome(req.getParameter("carro[idCategoria]"));
        java.util.Date dataInicial = null, dataFinal = null;
        Reserva reserva = null;

        Usuario usuario = UsuarioBuilder
                .getBuilder()
                .adicionaNome(req.getParameter("usuario[nome]"))
                .adicionaApelido(req.getParameter("usuario[apelido]"))
                .adicionaCPF(req.getParameter("usuario[cpf]"))
                .adicionaRG(req.getParameter("usuario[rg]"))
                .adicionaEmail(req.getParameter("usuario[email]"))
                .adicionaSexo(req.getParameter("usuario[sexo]"))
                .adicionaID(Integer.parseInt(req.getParameter("usuario[idUsuario]")))
                .build();

        try {

            dataInicial = simpleDateFormat.parse(req.getParameter("reserva[dataInicio]"));
            dataFinal = simpleDateFormat.parse(req.getParameter("reserva[dataFinal]"));

            reserva = ReservaBuilder
                    .getBuilder()
                    .adicionaIdUsuario(usuario.getIdUsuario())
                    .adicionaIdCarro(Integer.parseInt(req.getParameter("carro[idCarro]")))
                    .adicionaIdCategoria(categoria.getIdCategoria())
                    .adicionaIdReserva(Integer.parseInt(req.getParameter("reserva[idReserva]")))
                    .adicionaValorTotal(Double.parseDouble(req.getParameter("reserva[valorTotal]")))
                    .adicionaStatus(req.getParameter("reserva[status]"))
                    .adicionaDiaFinal(new Date(dataFinal.getTime()))
                    .adicionaDiaInicio(new Date(dataInicial.getTime()))
                    .build();

            reservaServico.atualizaReserva(reserva);

        } catch (ParseException ex) {

            Logger.getLogger(ReservaDeCarro.class.getName()).log(Level.SEVERE, null, ex);
            mensagemResposta = RespostaJSON.formataJson(String.format("Reserva não pode ser reprovada %s", ex.getMessage()), true);

        }

        RespostaJSON.respondeJSON(resp, mensagemResposta);

    }

}
