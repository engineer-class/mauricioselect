package com.fatec.mauricioselect.web.servlet;

import com.fatec.mauricioselect.api.servico.ReservaServico;
import com.fatec.mauricioselect.core.servico.ImplReservaServico;
import com.google.gson.Gson;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name="Reserva",urlPatterns = "/reserva")
public class Reserva extends HttpServlet{
     @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String reserva = req.getParameter("reserva");
        ReservaServico reservaServico = new ImplReservaServico();
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        
        if(reserva == null)
            resp.getWriter()
                    .write(
                            new Gson()
                                .toJson(
                                    reservaServico.encontrarTudo()
                            )
                    );
        else
            resp.getWriter()
                    .write(
                            new Gson()
                                    .toJson(reservaServico.encontrarPorIdReserva(Integer.valueOf(reserva)
                                    ))
                    );
    }
}
