/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fatec.mauricioselect.web.servlet.filtro;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebFilter(filterName = "paginasProtegidas",
        urlPatterns = {"/cadastro/operador/*",
            "/gerenciar/reserva/*",
            "/receber/carro/alugado/*",
            "/frota/atual/*",
            "/cadastrar/carro/*"})
public class FiltraSessaoOperador implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException { }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        
        HttpServletRequest requisicaoServlet = (HttpServletRequest) request;
        HttpServletResponse respostaServlet = (HttpServletResponse) response;
        HttpSession sessao = requisicaoServlet.getSession();
        
        if(sessao.getAttribute("usuarioAutenticado") != null && "operador".equals(sessao.getAttribute("tipoUsuario"))){
            chain.doFilter(request, response);
        }else{
            respostaServlet.sendRedirect("/");
        }
    }

    @Override
    public void destroy() { }
    
}
