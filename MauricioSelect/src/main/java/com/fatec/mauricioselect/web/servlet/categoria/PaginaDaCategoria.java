package com.fatec.mauricioselect.web.servlet.categoria;

import java.io.IOException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet com a rota para a paǵina principal do gerenciamento de categorias
 */
@WebServlet(name = "PrincipalCategoria", urlPatterns = {"/categoria"})
public class PaginaDaCategoria extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletContext context = req.getServletContext();
        
        context.getRequestDispatcher("/jsp/categoria/gerencia.jsp").forward(req, resp);
    }
}
