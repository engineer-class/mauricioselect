package com.fatec.mauricioselect.web.servlet.usuario;

import com.fatec.mauricioselect.api.modelo.Reserva;
import com.fatec.mauricioselect.api.modelo.Usuario;
import com.fatec.mauricioselect.api.servico.ReservaServico;
import com.fatec.mauricioselect.api.servico.UsuarioServico;
import com.fatec.mauricioselect.core.servico.ImplReservaServico;
import com.fatec.mauricioselect.core.servico.ImplUsuarioServico;
import com.fatec.mauricioselect.web.servlet.utilitarios.RecuperadorDeParametros;
import com.fatec.mauricioselect.web.servlet.utilitarios.RespostaJSON;
import java.io.IOException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author felipe
 */
@WebServlet(name = "ReservaDeCarro", urlPatterns = {"/reserva/carro"})
public class ReservaDeCarro extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {     
                        
        ReservaServico reservaServico = new ImplReservaServico();
        UsuarioServico usuarioServico = new ImplUsuarioServico();
        
        HttpSession sessao = req.getSession();
        String mensagemDeRetorno = "";
                         
        // Busca as informações corretas de cada parâmetro nos serviços
        String apelidoUsuario = (String) sessao.getAttribute("usuarioAutenticado");
        Usuario usuario = usuarioServico.encontrarPorApelido(apelidoUsuario);
        
        Reserva reserva = null;
        try {
            reserva = RecuperadorDeParametros
                            .recuperaReservaBuilderApartirDosParametros(req)
                            .adicionaIdUsuario(usuario.getIdUsuario())
                            .adicionaStatus(req.getParameter("status"))
                            .build();            
        } catch (ParseException ex) {
            mensagemDeRetorno = RespostaJSON.formataJson(ex.getMessage(), true);
        }

        try {
            reservaServico.inserir(reserva);
            mensagemDeRetorno = "{erro: false, mensagem: \"Inserção realizada com sucesso\"}";
        } catch (RuntimeException e) {
            mensagemDeRetorno = String.format("{erro: true, mensagem: %s}", e.getMessage());
            Logger.getLogger(ReservaDeCarro.class.getName()).log(Level.SEVERE, null, e);
        }
       
        RespostaJSON.respondeJSONSemFlush(resp, mensagemDeRetorno);
                
        ServletContext context = req.getServletContext();
        context.getRequestDispatcher("/jsp/usuario/confirmacaoReservaUsuario.jsp").forward(req, resp);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ReservaServico reservaServico = new ImplReservaServico();
        UsuarioServico usuarioServico = new ImplUsuarioServico();
        
        HttpSession sessao = req.getSession();
        String mensagemDeRetorno = "";
                         
        // Busca as informações corretas de cada parâmetro nos serviços
        String apelidoUsuario = (String) sessao.getAttribute("usuarioAutenticado");
        Usuario usuario = usuarioServico.encontrarPorApelido(apelidoUsuario);
        
        Reserva reserva = null;
        try {
            reserva = RecuperadorDeParametros
                            .recuperaReservaBuilderApartirDosParametros(req)
                            .adicionaIdUsuario(usuario.getIdUsuario())
                            .adicionaStatus(req.getParameter("status"))
                            .build();            
        } catch (ParseException ex) {
            mensagemDeRetorno = RespostaJSON.formataJson(ex.getMessage(), true);
        }       
        
        try {
            reservaServico.atualizaReserva(reserva);
            mensagemDeRetorno = "{erro: false, mensagem: \"Atualização realizada com sucesso\"}";            
        } catch (RuntimeException ex) {
            mensagemDeRetorno = RespostaJSON.formataJson(ex.getMessage(), true);
        }

        RespostaJSON.respondeJSONSemFlush(resp, mensagemDeRetorno);
                
        ServletContext context = req.getServletContext();
        context.getRequestDispatcher("/jsp/usuario/confirmacaoReservaUsuario.jsp").forward(req, resp);        
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
            throws ServletException, IOException {
    
        ServletContext context = req.getServletContext();
        context.getRequestDispatcher("/jsp/usuario/confirmacaoReservaUsuario.jsp").forward(req, resp);
    }
}
