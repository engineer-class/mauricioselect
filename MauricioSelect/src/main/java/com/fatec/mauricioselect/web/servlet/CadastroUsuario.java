package com.fatec.mauricioselect.web.servlet;

import com.fatec.mauricioselect.api.modelo.Usuario;
import com.fatec.mauricioselect.api.modelo.UsuarioBuilder;
import com.fatec.mauricioselect.api.servico.UsuarioServico;
import com.fatec.mauricioselect.core.servico.ImplUsuarioServico;
import java.io.IOException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author felipe
 */
@WebServlet(name = "Cadastro", urlPatterns = {"/cadastro"})
public class CadastroUsuario extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        ServletContext context = req.getServletContext();
        context.getRequestDispatcher("/jsp/cadastroUsuario.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletContext context = req.getServletContext();
        UsuarioServico usuarioServico = new ImplUsuarioServico();

        Usuario usuarioForm = UsuarioBuilder.getBuilder()
                                .adicionaApelido(req.getParameter("apelido"))
                                .adicionaNome(req.getParameter("nome"))
                                .adicionaSenha(req.getParameter("senha"))
                                .adicionaCPF(req.getParameter("cpf"))
                                .adicionaEmail(req.getParameter("email"))
                                .adicionaRG(req.getParameter("rg"))
                                .adicionaSexo(req.getParameter("sexo")).build();   
        
        try {
            usuarioServico.inserir(usuarioForm);
            req.setAttribute("dadosInvalidos", "nao");
        } catch (Exception ex) {
            req.setAttribute("dadosInvalidos", "sim");
            req.setAttribute("mensagemDeErro", ex.getMessage());
        }
        
        req.setAttribute("formularioPreenchido", true);
        context.getRequestDispatcher("/jsp/cadastroUsuario.jsp").forward(req, resp);
    }
}
