package com.fatec.mauricioselect.web.servlet.filtro;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter(filterName = "filtraPaginasSeguras",
            urlPatterns = {"/reserva/carro/*", 
                "/reserva/usuario/*"})
public class FiltraSessaoUsuario implements Filter{

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    /**
     * Método para realizar o sistema de filtro das páginas
     * @param request
     * @param response
     * @param chain
     * @throws IOException
     * @throws ServletException 
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
       
        HttpServletRequest requisicaoServlet = (HttpServletRequest) request;
        HttpServletResponse respostaServlet = (HttpServletResponse) response;
        HttpSession sessao = requisicaoServlet.getSession();
        
        if (sessao.getAttribute("usuarioAutenticado") != null && "usuario".equals(sessao.getAttribute("tipoUsuario"))) {
            chain.doFilter(request, response);
        } else {
            respostaServlet.sendRedirect("/");
        }
    }

    @Override
    public void destroy() {
    }
}
