package com.fatec.mauricioselect.web.servlet.carro;

import com.fatec.mauricioselect.api.modelo.CarroBuilder;
import com.fatec.mauricioselect.api.modelo.Categoria;
import com.fatec.mauricioselect.api.modelo.Fabricante;
import com.fatec.mauricioselect.api.servico.CarroServico;
import com.fatec.mauricioselect.api.servico.CategoriaServico;
import com.fatec.mauricioselect.api.servico.FabricanteServico;
import com.fatec.mauricioselect.core.servico.ImplCarroServico;
import com.fatec.mauricioselect.core.servico.ImplCategoriaServico;
import com.fatec.mauricioselect.core.servico.ImplFabricanteServico;
import com.fatec.mauricioselect.web.servlet.utilitarios.RecuperadorDeParametros;
import com.fatec.mauricioselect.web.servlet.utilitarios.RespostaJSON;
import java.io.IOException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet com a rota para o cadastro de carros
 * 
 */
@WebServlet(name = "InserirCarros", urlPatterns = {"/carro/cadastrar"})
public class CadastrarCarro extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletContext context = req.getServletContext();
        context.getRequestDispatcher("/jsp/operador/cadastroCarro.jsp").forward(req,resp);
    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        String mensagemDeRetorno = 
                RespostaJSON.formataJson("Cadastro feito com sucesso", false);
        
        CarroServico carroServico = new ImplCarroServico();
        CategoriaServico categoriaServico = new ImplCategoriaServico();
        FabricanteServico fabricanteServico = new ImplFabricanteServico();
        
        // Buscando informações
        Categoria categoriaDoCarro = categoriaServico
                            .encontrarPorNome(req.getParameter("categoria"));
        Fabricante fabricante = fabricanteServico
                            .encontrarPorNome(req.getParameter("fabricante"));
        
        // Cria o objeto a partir dos parâmetros HTTP
        CarroBuilder carroBuilder = RecuperadorDeParametros
                                .recuperaCarroBuilderApartirDosParametros(req);
        carroBuilder.adicionaIDFabricante(fabricante.getIdFabricante());
        carroBuilder.adicionaCategoria(categoriaDoCarro.getIdCategoria());
        
        try {
            carroServico.inserir(carroBuilder.build());
        } catch (RuntimeException ex) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            mensagemDeRetorno = 
                    RespostaJSON.formataJson(ex.getMessage(), true);
        }

        RespostaJSON.respondeJSON(resp, mensagemDeRetorno);
    }
}
