package com.fatec.mauricioselect.web.servlet;

import java.io.IOException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Login extends HttpServlet {

    /**
     * 
     * Este método recebe requisições para validação de usuários existentes, isto é, 
     * quando um usuário precisa logar no sistema a página jsp "login.jsp" é encaminhada na rota /login
     * 
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException 
     */
   @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
       ServletContext context = req.getServletContext();
       
       try {
           context.getRequestDispatcher("/jsp/login.jsp").forward(req, resp);
       } catch(Exception e) {
           System.out.println(e.getMessage());
       };    
    }
}
