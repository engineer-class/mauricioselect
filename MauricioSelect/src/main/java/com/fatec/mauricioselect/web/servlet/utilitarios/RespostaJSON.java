package com.fatec.mauricioselect.web.servlet.utilitarios;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author felipe
 */
public class RespostaJSON {
    
    /**
     * Método para realizar o envio de respostas JSON nos servlets
     * 
     * @param respostaHTTP 
     * @param json 
     * @throws IOException 
     */
    public static void respondeJSON(HttpServletResponse respostaHTTP, String json) 
            throws IOException {
       
        PrintWriter out = respostaHTTP.getWriter();
        respostaHTTP.setContentType("application/json");
        respostaHTTP.setCharacterEncoding("UTF-8");
                
        out.print(json);
        out.flush();   
    }
    
    /**
     * Método para realizar a respostaJSON sem o flush
     * @param respostaHTTP
     * @param json
     * @throws IOException 
     */
    public static void respondeJSONSemFlush(HttpServletResponse respostaHTTP, String json) 
            throws IOException {
        
        PrintWriter out = respostaHTTP.getWriter();
        respostaHTTP.setContentType("application/json");
        respostaHTTP.setCharacterEncoding("UTF-8");
                
        out.print(json);
    } 
    
    /**
     * Método para formatar o json no formato padronizado da API
     * @param mensagem
     * @param erro
     * @return 
     */
    public static String formataJson(String mensagem, boolean erro) {
        return String.format("{\"erro\": %b, \"mensagem\": \"%s\"}", erro, mensagem);
    }
}
