package com.fatec.mauricioselect.web.servlet.fabricante;

import com.fatec.mauricioselect.api.servico.FabricanteServico;
import com.fatec.mauricioselect.core.servico.ImplFabricanteServico;
import com.fatec.mauricioselect.web.servlet.utilitarios.RespostaJSON;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet com a rota para deletar fabricantes
 */
@WebServlet(name = "DeletarFabricantes", urlPatterns = {"/fabricante/deletar"})
public class DeletarFabricante extends HttpServlet {

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
                
        String mensagemDeRetorno = RespostaJSON
                            .formataJson("Fabricante excluído com sucesso", false);
        Integer idFabricante = Integer.parseInt(req.getParameter("id"));
        FabricanteServico servicoFabricante = new ImplFabricanteServico();
            
        Boolean resultado = null;
        try {
            resultado = servicoFabricante.deletar(idFabricante);
        } catch (RuntimeException ex) {
            mensagemDeRetorno = RespostaJSON.formataJson(ex.getMessage(), true);
        }
        
        if (!resultado)
            mensagemDeRetorno = RespostaJSON.formataJson("Não foi possível excluir este fabricante", true);
        
        RespostaJSON.respondeJSON(resp, mensagemDeRetorno);
    }
}
