package com.fatec.mauricioselect.web.servlet.utilitarios;

import java.io.IOException;
import java.net.InetAddress;
import java.sql.Date;
import org.apache.commons.net.ntp.NTPUDPClient;
import org.apache.commons.net.ntp.TimeInfo;
import java.util.logging.Logger;
import java.util.logging.Level;

/**
 * Classe para a recuperação de dados de tempo de servidores oficiais de NTP
 */
public class ClienteNTP {
    private static final String[] SERVIDORES_NTP = {"a.st1.ntp.br", 
                                                    "a.ntp.br", "c.ntp.br",
                                                    "b.st1.ntp.br", "gps.ntp.br"};
   
    /**
     * Método para recuperar a data atual utilizando o servidor do NTP.br
     * @return
     */
    public static Date recuperaDataAtual()  {
         
        // Percorre o array de servidores, assim caso algum apresente problemas
        // outros são testados
        for (String servidorNTP: SERVIDORES_NTP) {
            try {
                NTPUDPClient clienteNTP = new NTPUDPClient();
                 InetAddress enderecoDoServidorNTP = InetAddress.getByName(servidorNTP);
                TimeInfo informacaoDeData = clienteNTP.getTime(enderecoDoServidorNTP);
                long tempoDecimalAtual = informacaoDeData.getMessage()
                                                     .getTransmitTimeStamp()
                                                     .getTime();
                return new Date(tempoDecimalAtual);            
            } catch (IOException e) {
                Logger.getLogger(ClienteNTP.class.getName()).log(Level.WARNING, null, e);
            }
        }
        return null;
    };
}
