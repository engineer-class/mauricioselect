package com.fatec.mauricioselect.web.servlet.utilitarios;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ValidadorDeCampos {
    public static <T> void validaCamposDoObjeto(T objeto) throws InvocationTargetException {
        for (Method method : objeto.getClass().getMethods()) {
            if (method.getName().contains("get")) {
                Object resultadoDoInvoke = null;

                try {
                    resultadoDoInvoke = method.invoke(objeto);
                } catch (IllegalAccessException
                        | IllegalArgumentException
                        | InvocationTargetException ex) {
                    Logger.getLogger(objeto.getClass().getName()).log(Level.SEVERE, null, ex);
                }

                if (!(resultadoDoInvoke == null)) {
                    String stringDeVerificao = resultadoDoInvoke.toString();

                    if (!(stringDeVerificao.trim().isEmpty())) {
                        break;
                    }
                }
                throw new RuntimeException("Todos os campos precisam ser preenchidos!");
            }
        }
    }
}
