package com.fatec.mauricioselect.web.servlet.usuario;

import com.fatec.mauricioselect.api.modelo.Usuario;
import com.fatec.mauricioselect.api.servico.ReservaServico;
import com.fatec.mauricioselect.api.servico.UsuarioServico;
import com.fatec.mauricioselect.core.servico.ImplReservaServico;
import com.fatec.mauricioselect.core.servico.ImplUsuarioServico;
import java.io.IOException;
import com.fatec.mauricioselect.api.modelo.Reserva;
import com.fatec.mauricioselect.api.modelo.categorico.ReservaCategorica;
import com.fatec.mauricioselect.web.servlet.utilitarios.RespostaJSON;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "ReservaDoUsuario", urlPatterns = {"/reserva/usuario"})
public class ReservaDoUsuario extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletContext context = req.getServletContext();
        HttpSession sessao = req.getSession();
        
        UsuarioServico usuarioServico = new ImplUsuarioServico();
        ReservaServico reservaServico = new ImplReservaServico();
        
        Usuario usuarioAutenticado = usuarioServico
                .encontrarPorApelido((String) sessao.getAttribute("usuarioAutenticado"));
        
        List<ReservaCategorica> reservas = reservaServico.encontrarPorIdUsuario(usuarioAutenticado.getIdUsuario());
        
        sessao.setAttribute("reservas", reservas);
        context.getRequestDispatcher("/jsp/usuario/reservasUsuario.jsp").forward(req, resp);    
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {        
        ServletContext context = req.getServletContext();
        
        // Recuperando o ID a ser deleteado
        Integer idReserva = Integer.parseInt(req.getParameter("idReserva"));
        
        ReservaServico reservaServico = new ImplReservaServico();
        boolean resultado = reservaServico.deleta(new Reserva(idReserva));
        
        String mensagem = "{error: true, mensagem: \"Não foi possível excluir esta reserva!\"}";
        if (resultado) {
            mensagem = "{error: false, mensagem: \"A reserva foi excluida com sucesso!\"}";
        }
        
        // Envia mensagem para o usuário
        RespostaJSON.respondeJSONSemFlush(resp, mensagem);
                
        context.getRequestDispatcher("/jsp/usuario/reservasUsuario.jsp").forward(req, resp); 
    }   
}
