package com.fatec.mauricioselect.web.servlet.usuario;

import com.fatec.mauricioselect.api.modelo.Usuario;
import com.fatec.mauricioselect.api.servico.UsuarioServico;
import com.fatec.mauricioselect.core.servico.ImplUsuarioServico;
import com.fatec.mauricioselect.web.servlet.utilitarios.RespostaJSON;
import com.google.gson.Gson;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/usuario/lista/id"})
public class ListaUsuarioPorId extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        Integer idUsuario = Integer.parseInt(req.getParameter("id"));
        
        UsuarioServico implUsuarioServico = new ImplUsuarioServico();
        Usuario usuario = implUsuarioServico.encontrarUsuarioPorId(idUsuario);
             
        if (usuario == null) {
            RespostaJSON.respondeJSON(resp, 
                    RespostaJSON.formataJson("Nenhum usuario foi encontrado", true));
        } else {
           RespostaJSON.respondeJSON(resp, new Gson().toJson(usuario)); 
        }
    }

}