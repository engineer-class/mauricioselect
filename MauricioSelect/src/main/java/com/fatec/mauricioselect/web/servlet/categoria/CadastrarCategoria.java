package com.fatec.mauricioselect.web.servlet.categoria;

import com.fatec.mauricioselect.api.modelo.Categoria;
import com.fatec.mauricioselect.api.servico.CategoriaServico;
import com.fatec.mauricioselect.core.servico.ImplCategoriaServico;
import com.fatec.mauricioselect.web.servlet.utilitarios.RespostaJSON;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet com a rota para cadastrar categorias
 */
@WebServlet(name = "CadastrarCategorias", urlPatterns = {"/categoria/cadastrar"})
public class CadastrarCategoria extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        String mensagemDeRetorno = 
                RespostaJSON.formataJson("Categoria cadastrada com sucesso!", false);
        CategoriaServico categoriaServico = new ImplCategoriaServico();
        
        // Recuperando as informações
        String nomeDaCategoria = req.getParameter("nomeDaCategoria");
        Double horaDaCategoria = Double.parseDouble(req.getParameter("horaDaCategoria"));

        try {
            categoriaServico
                        .inserir(new Categoria(nomeDaCategoria, horaDaCategoria));
        } catch (RuntimeException ex) {
            mensagemDeRetorno = 
                    RespostaJSON.formataJson(ex.getMessage(), true);
        }
        
        RespostaJSON.respondeJSON(resp, mensagemDeRetorno);
    }
}
