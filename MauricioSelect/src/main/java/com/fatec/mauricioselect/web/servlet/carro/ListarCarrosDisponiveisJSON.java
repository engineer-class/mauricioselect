package com.fatec.mauricioselect.web.servlet.carro;

import com.fatec.mauricioselect.api.modelo.categorico.CarroCategorico;
import com.fatec.mauricioselect.api.servico.CarroServico;
import com.fatec.mauricioselect.core.servico.ImplCarroServico;
import com.fatec.mauricioselect.web.servlet.utilitarios.RespostaJSON;
import com.google.gson.Gson;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/carro/listar/disponiveis/json"})
public class ListarCarrosDisponiveisJSON extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        CarroServico carroServico = new ImplCarroServico();
        
        List<CarroCategorico> carros = carroServico.encontrarCarrosDisponiveis();
        
        RespostaJSON.respondeJSON(resp, new Gson().toJsonTree(carros).toString());
    }
}
