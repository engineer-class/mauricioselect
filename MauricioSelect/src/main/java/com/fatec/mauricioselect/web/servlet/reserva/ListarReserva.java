package com.fatec.mauricioselect.web.servlet.reserva;

import com.fatec.mauricioselect.api.modelo.categorico.ReservaCategorica;
import com.fatec.mauricioselect.api.servico.ReservaServico;
import com.fatec.mauricioselect.core.servico.ImplReservaServico;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "GerenciarReserva", urlPatterns = {"/reserva/listar"})
public class ListarReserva extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        ServletContext context = request.getServletContext();        
        HttpSession sessao = request.getSession();
        
        ReservaServico reservaServico = new ImplReservaServico();

        List<ReservaCategorica> reservas = reservaServico.encontrarReservasDisponiveis();

        sessao.setAttribute("reservas", reservas);        
        context.getRequestDispatcher("/jsp/operador/gerenciarReservas.jsp").forward(request, response);
    }
}
