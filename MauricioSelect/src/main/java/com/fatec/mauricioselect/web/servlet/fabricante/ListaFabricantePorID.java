package com.fatec.mauricioselect.web.servlet.fabricante;

import com.fatec.mauricioselect.api.modelo.Fabricante;
import com.fatec.mauricioselect.core.servico.ImplFabricanteServico;
import com.fatec.mauricioselect.web.servlet.utilitarios.RespostaJSON;
import com.google.gson.Gson;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet com a rota para listar os fabricantes por ID
 */
@WebServlet(name = "ListarFabricantesPorID", urlPatterns = {"/fabricante/lista/id"})
public class ListaFabricantePorID extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer idFabricante = Integer.parseInt(req.getParameter("id"));
        
        ImplFabricanteServico implFabricanteServico = new ImplFabricanteServico();
        Fabricante fabricante = implFabricanteServico.encontrarPorId(idFabricante);
             
        if (fabricante == null) {
            RespostaJSON.respondeJSON(resp, 
                    RespostaJSON.formataJson("Nenhum fabricante foi encontrado", true));
        } else {
           RespostaJSON.respondeJSON(resp, new Gson().toJson(fabricante)); 
        }
    }
}
