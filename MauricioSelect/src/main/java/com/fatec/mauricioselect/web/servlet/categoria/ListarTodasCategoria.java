package com.fatec.mauricioselect.web.servlet.categoria;

import com.fatec.mauricioselect.api.modelo.Categoria;
import com.fatec.mauricioselect.core.servico.ImplCategoriaServico;
import com.fatec.mauricioselect.web.servlet.utilitarios.RespostaJSON;
import com.google.gson.Gson;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet com a rota que lista todas as categorias disponíveis
 */
@WebServlet(name = "ListarCategorias", urlPatterns = {"/categoria/listar"})
public class ListarTodasCategoria extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        ImplCategoriaServico implCategoriaServico = new ImplCategoriaServico();
        List<Categoria> categorias = implCategoriaServico.recuperaTodasCategorias();

        RespostaJSON.respondeJSON(resp, new Gson().toJsonTree(categorias).toString());
    }
}
