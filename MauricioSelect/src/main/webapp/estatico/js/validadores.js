function recuperaSexoUsuarioNoCadastro() {
    let sexo = document.getElementsByName("sexo");
    
    for (let s = 0; s < sexo.length; s++) {
        if (sexo[s].checked) {
            return sexo[s].value;
        }
    }
}

/**
 * Verifica a validade de todos os campos do cadastro
 * 
 * @param {type} nomeFormulario
 * @returns {Boolean}
 */
function validaCadastroUsuario(nomeFormulario) {
   
   // Recuperando todos os campos de Login
   let nomeUsuario = document.forms[nomeFormulario]['nome'].value;
   let apelidoUsuario = document.forms[nomeFormulario]['apelido'].value;
   let cpf = document.forms[nomeFormulario]['cpf'].value;
   let senha = document.forms[nomeFormulario]['senha'].value;
   let email = document.forms[nomeFormulario]['email'].value;
   let rg = document.forms[nomeFormulario]['rg'].value;
   let sexo = recuperaSexoUsuarioNoCadastro();
      
   if (nomeUsuario.trim() === '' || apelidoUsuario.trim() === '' ||
        senha.trim() === '' || cpf.trim() === '' ||
        email.trim() === '' || rg.trim() === '' || sexo.trim() === "") {
        
        alert('Todos os campos devem ser preenchidos');
        return false;
    }
    
    if (!CPF.validate(cpf)) {
        alert('O campo CPF deve ser preenchido corretamente');
        return false;
    }
    return true;
}