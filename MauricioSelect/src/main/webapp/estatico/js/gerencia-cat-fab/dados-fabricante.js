/**
 * Funções para a aquisição de dados da página de gerenciamento de fabricante
*/

/**
 * Função para validar os dados de fabricante inseridos pelos usuários
 * 
 * @param {*} elementoNome Elemento da página com o conteúdo do nome do fabricante
 */
function validaDadosFabricante(objetoFabricante) {
    if (objetoFabricante.nomeFabricante.trim().length != 0) {
        return true;
    } 
    return false;
}

/**
 * Função para cadastrar fabricante
 */
function cadastraFabricante() {
    let elementoNome = document.getElementById('cadastroNomeFabricante');
    let objetoFabricante = {
        nomeFabricante: elementoNome.value
    };

    if (validaDadosFabricante(objetoFabricante)) {
        xhr = new XMLHttpRequest();
    
        xhr.open('POST', '/fabricante/cadastrar');
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.onload = function() {
            let resposta = JSON.parse(xhr.responseText);
            alert(resposta.mensagem);

            if (!resposta.erro) {
                location.reload();
            }
        };
        xhr.send(encodeURI('nomeFabricante=' + objetoFabricante.nomeFabricante));
    } else {
        alert('Nome de fabricante inválido!');
    }
}

/**
 * Função para recuperar os fabricantes disponíveis no MauricioSelect
 * 
 * @param {*} elementoSelectPai Elemento onde os resultados serão inseridos
 */
function recuperaFabricantes(elementoSelectPai) {

    let fabricantesInseridos = [];
    const ajax = new XMLHttpRequest();

    ajax.open("GET", "/fabricante/listar");
    ajax.send();

    // categoriasDisponiveis = [];
    ajax.onreadystatechange = function() {
        JSON.parse(ajax.responseText).forEach((elemento) => {
            // categoriasDisponiveis.push(elemento);
            let elementoSelect = document.createElement("option");
            
            // Verificador para evitar duplicatas no `select`
            if (!fabricantesInseridos.includes(elemento.nome)) {
                // Cria elemento e insere no `select` de categorias
                elementoSelect.innerHTML = elemento.nome;
                elementoSelect.id = elemento.idFabricante;

                elementoSelectPai.appendChild(elementoSelect);
                fabricantesInseridos.push(elemento.nome);
            }
        });
    }
}


/**
 * Função para recuperar o ID do fabricante selecionado no `select`
 */
function recuperaFabricanteDoSelect() {
    let fabricantesSelect = document.getElementById('selectFabricantes');
    let fabricanteSelecionado = fabricantesSelect[fabricantesSelect.selectedIndex];
    
    return {
        idDoFabricante: fabricanteSelecionado.id,
        nomeFabricante: fabricanteSelecionado.innerText
    };
}

/**
 * Função para deletar um fabricante selecionado no `Select` de fabricantes
 */
function deletaFabricanteSelecionadoNoSelect() {

    // Recuperando o ID do fabricante selecionado
    let fabricante = recuperaFabricanteDoSelect();

    $.ajax({
        url: '/fabricante/deletar' + '?' + $.param({
            id: fabricante.idDoFabricante
        }),
        type: 'DELETE'
    }).done(function(json) {
        alert(json.mensagem);

        if (!json.erro) {
            location.reload();
        }
    });
}


/**
 * Função para atualizar o fabricante presente no select
 */
function atualizaFabricante() {
    let fabricante = recuperaFabricanteDoSelect();
    let novoNomeFabricante = document.getElementById('atualizaNomeFabricante').value;
    let ehValido = validaDadosFabricante({nomeFabricante: novoNomeFabricante});

    if (ehValido) {
        xhr = new XMLHttpRequest();
    
        xhr.open('POST', '/fabricante/atualizar');
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.onload = function() {
            let resposta = JSON.parse(xhr.responseText);
            alert(resposta.mensagem);

            if (!resposta.erro) {
                location.reload();
            }
        };
        xhr.send(encodeURI('nomeFabricante=' + novoNomeFabricante
                            + '&idFabricante=' + fabricante.idDoFabricante));
    } else {
        alert('O novo nome inserido é inválido!');
    }
}
