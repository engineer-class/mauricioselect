/**
 * Script com funções para a aquisição de dados da página de gerenciamento de
 * categorias
 */

/**
 * Função para validar os dados de fabricante inseridos pelos usuários
 * 
 * @param {*} elementoNome Elemento da página com o conteúdo do nome do fabricante
 * @param {*} elementoValor Elemento da página com o conteúdo do valor do fabricante
 */
function validaDadosCategoria(elementoNome, elementoValor) {
    if (elementoNome.trim().length == 0 || elementoNome.trim() == "" ||
        elementoValor <= 0 || isNaN(elementoValor)) {
            return false;
    }
    return true;
}


/**
 * Função para recuperar as categorias disponíveis no MauricioSelect
 * 
 * @param {*} elementoSelectPai Elemento onde os resultados serão inseridos
 */
function recuperaCategorias(elementoSelectPai) {

    let categoriasInseridas = [];
    const ajax = new XMLHttpRequest();

    ajax.open("GET", "/categoria/listar");
    ajax.send();

    categoriasDisponiveis = [];
    ajax.onreadystatechange = function() {
        JSON.parse(ajax.responseText).forEach((elemento) => {
            // categoriasDisponiveis.push(elemento);
            let elementoSelect = document.createElement("option");
            
            // Verificador para evitar duplicatas no `select`
            if (!categoriasInseridas.includes(elemento.nome)) {
                // Cria elemento e insere no `select` de categorias
                elementoSelect.innerHTML = elemento.nome;
                elementoSelect.id = elemento.idCategoria;
                elementoSelect.value = elemento.valorPorHora;

                elementoSelectPai.appendChild(elementoSelect);
                categoriasInseridas.push(elemento.nome);
            }
        });
    }
}

/**
 * Função para cadastrar categoria inserida pelo usuário
 */
function cadastraCategoria() {
    let cadastroNomeCategoria = document.getElementById('cadastroNomeCategoria');
    let cadastroValorCategoria = document.getElementById('cadastroValorCategoria');
    let elementoNome = cadastroNomeCategoria.value;
    let elementoValor = parseFloat(cadastroValorCategoria.value);
    
    if (validaDadosCategoria(elementoNome, elementoValor)) {
        xhr = new XMLHttpRequest();
    
        xhr.open('POST', '/categoria/cadastrar');
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.onload = function() {
            let resposta = JSON.parse(xhr.responseText);
            alert(resposta.mensagem);

            if (!resposta.erro) {
                location.reload();
            }
        };
        xhr.send(encodeURI('nomeDaCategoria=' + elementoNome + 
                            '&horaDaCategoria=' + elementoValor));
    } else {
        alert('Preencha todos os campos corretamente!');   
    }
}

/**
 * Função para atualizar as categorias disponível
 */
function atualizaCategoria() {
    // Recupera os campos com os valores que o usuário inseriu
    let atualizaNomeCategoria = document.getElementById('atualizaNomeCategoria');
    let atualizaHoraCategoria = document.getElementById('atualizaHoraCategoria');
    let categoriaOriginalSelecionada = document.getElementById('selectCategorias');
    // Valores
    let idCategoria = categoriaOriginalSelecionada[categoriaOriginalSelecionada.selectedIndex].id;
    let nomeDaCategoria = atualizaNomeCategoria.value;
    let valorHora = atualizaHoraCategoria.value;

    if (validaDadosCategoria(nomeDaCategoria, valorHora)) {
        xhr = new XMLHttpRequest();
    
        xhr.open('POST', '/categoria/atualizar');
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.onload = function() {
            let resposta = JSON.parse(xhr.responseText);
            alert(resposta.mensagem);

            if (!resposta.erro) {
                location.reload();
            }
        };
        xhr.send(encodeURI('nomeCategoria=' + nomeDaCategoria + 
                            '&valorHoraCategoria=' + valorHora + 
                            '&idCategoria=' + idCategoria));
    } else {
        alert('Os dados inseridos não são válidos');
    }
}

/**
 * Função para deletar um fabricante selecionado no `Select` de fabricantes
 */
function deletaCategoriaSelecionadoNoSelect() {
    let selectCategorias = document.getElementById('selectCategorias');
    let categoriaSelecionada = selectCategorias[selectCategorias.selectedIndex];

    $.ajax({
        url: '/categoria/deletar' + '?' + $.param({
            idCategoria: categoriaSelecionada.id
        }),
        type: 'DELETE'
    }).done(function(json) {
        alert(json.mensagem);

        if (!json.erro) {
            location.reload();
        }
    });
}
