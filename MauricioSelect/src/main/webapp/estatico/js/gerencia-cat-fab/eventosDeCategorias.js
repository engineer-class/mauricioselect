/**
 * Funções de eventos dos elementos da página de gerenciamento de fabricante
 */
(function() {
    let btnCadastra = document.getElementById('btnCadastra');
    // Função cadastraCategoria disponível no arquivo `gerencia-cat-fab/dados.js`
    btnCadastra.addEventListener('click', cadastraCategoria);

    let selectCategorias = document.getElementById('selectCategorias');
    selectCategorias.onchange = function() {
        let elementoSelecionado = selectCategorias[selectCategorias.selectedIndex];
        let atualizaNomeCategoria = document.getElementById('atualizaNomeCategoria');
        let atualizaHoraCategoria = document.getElementById('atualizaHoraCategoria');
        
        atualizaNomeCategoria.value = elementoSelecionado.innerText;
        atualizaHoraCategoria.value = elementoSelecionado.value;
    }
    let btnAtualizar = document.getElementById('btnAtualizar');
    btnAtualizar.addEventListener('click', atualizaCategoria);
    let btnDeleta = document.getElementById('btnDeleta');
    btnDeleta.addEventListener('click', deletaCategoriaSelecionadoNoSelect);
})();

// Evento após carregar a página inicial
window.onload = function() {
    // Sobrescrevendo a função de abertura da página
    let selectCategorias = document.getElementById('selectCategorias');
    // Função de recuperação de categorias
    // Disponível no arquivo: `gerencia-cat-fab/dados-categoria.js`
    recuperaCategorias(selectCategorias);
}
