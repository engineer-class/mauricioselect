/**
 * Funções de eventos dos elementos da página de gerenciamento de fabricante
 */

(function() {
    let btnCadastra = document.getElementById('btnCadastra');
    // Função cadastraFabricante disponível no arquivo `gerencia-cat-fab/dados-fabricante.js`
    btnCadastra.addEventListener('click', cadastraFabricante);

    let btnDeleta = document.getElementById('btnDeleta');
    // Função deletaFabricanteSelecionadoNoSelect disponível no arquivo
    // `gerencia-cat-fab/dados-fabricante.js`
    btnDeleta.addEventListener('click', deletaFabricanteSelecionadoNoSelect);

    let btnAtualizar = document.getElementById('btnAtualizar');
    // Função atualizaFabricante disponível no arquivo `gerencia-cat-fab/dados-fabricante.js`
    btnAtualizar.addEventListener('click', atualizaFabricante);
})();

// Evento após carregar a página inicial
window.onload = function() {
    // Sobrescrevendo a função de abertura da página
    let selectFabricantes = document.getElementById('selectFabricantes');
    // Função de recuperação de categorias
    // Disponível no arquivo: `gerencia-cat-fab/dados-fabricante.js`
    recuperaFabricantes(selectFabricantes);
}
