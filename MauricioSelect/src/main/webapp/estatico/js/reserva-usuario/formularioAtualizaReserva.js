/**
 * Neste arquivo há funções para o tratamento das ações realizadas no modal
 */

// Função para adicionar os eventos aos botões do modal
(function() {
    document.getElementById("btnRecalcula").onclick = calculaValorTotal;
    document.getElementById("btnExcluir").onclick = function() {
        excluiReservaDoUsuario(document.getElementById('idReserva').innerText);
    };
})();

/**
 * Funçao para realizar a busca do elemento selecionado
 * 
 * @returns {int}
 */
function buscaSelecionadoNasReservasDoUsuario() {
    let elements = document.getElementsByTagName("input");
    
    for (var i = 0; i < elements.length; i++) {
        if (elements[i].checked) {
            return i + 1;
        }
    }
}

/**
 * Funçao para buscar a linha da matriz que esta sendo editada
 * 
 * @param {type} linhaSelecionada Linha selecionada pelo usuario para ediçao
 * @returns {recuperaLinhaMatriz.formularioAtualizaReservaAnonym$0}
 */
function recuperaLinhaMatriz(linhaSelecionada) { 
    let matriz = document.getElementById("tabelaReservas");
    let linha = matriz.rows[linhaSelecionada].cells;
    return {
            'id': linha[0].innerText, 
            'categoria': linha[1].innerText,
            'valorCategoria': linha[2].innerText,
            'modelo': linha[3].innerText,
            'dataInicial': linha[4].innerText,
            'dataFinal': linha[5].innerText,
            //'valorAntecipado': linha[6].innerText.split('$')[1].trim(),
            'valorTotal': linha[6].innerText.trim(),
            'status': linha[7].innerText
        };
}

$(function() {
  let dialogo;
  
  // Recupera as categorias
  // Função disponível no script `dados.js`
  recuperarCategorias();

  function trocaEstadoCategoriaECarro(categoriaSelecionada) {
    let labelValor = document.getElementById('lblValorHora');
    let modeloSelecao = document.getElementById('modeloDeCarro');
    let modelo = document.getElementsByName("modelo")[0];
   if (categoriaSelecionada.innerText === "Selecione") {
       modeloSelecao.disabled = true;
       labelValor.innerHTML = 0.0;
   } else {
       modeloSelecao.disabled = false;

       // Recupera o valor e insere no campo correto
       labelValor.innerHTML = categoriaSelecionada.value;

        // Busca os carros e preenche a tabela
       buscarCarroPorCategoria(categoriaSelecionada.innerText,modelo); 
   }
  }

  // Adicionando ação ao botão de seleção de carros
  let categoriaDeCarro = document.getElementsByName("categoria")[0];
  categoriaDeCarro.onchange = function() {
     let indiceSelecionado = categoriaDeCarro.selectedIndex;
     let categoriaSelecionada = categoriaDeCarro[indiceSelecionado];
     trocaEstadoCategoriaECarro(categoriaSelecionada);
  }
                    
  // Evento JS
  document.getElementById('botao').addEventListener('click', () =>  { 
      let formularioValido = preencheFormulario();
      
      if (formularioValido) {
        dialogo.dialog("open");
      } else {
        alert('Esta reserva não pode ser alterada');
      }  
  });

  function fecharModal() {
      dialogo.dialog( "close" );
  }
  
  /**
   * 
   * Insere as informaçoes do campo selecionado pelo usuario no formulario para 
   * ediçao.
   * 
   * @returns {void}
   */
  function preencheFormulario() {
            
        let linhaSelecionada = buscaSelecionadoNasReservasDoUsuario();
        let objLinha = recuperaLinhaMatriz(linhaSelecionada);

        if (objLinha.status === "PENDENTE") {

            let idReserva = document.getElementById('idReserva');
            let dataInicialReserva = document.getElementsByName("dataInicial")[0];
            let dataFinalReserva = document.getElementsByName("dataFinal")[0];
            let valorTotal = document.getElementById("valorTotal");
            let lblValorHora = document.getElementById("lblValorHora");
     
            idReserva.innerText = objLinha.id;
            dataInicialReserva.value = new Date(objLinha.dataInicial).toJSON().split('T')[0];
            dataFinalReserva.value = new Date(objLinha.dataFinal).toJSON().split('T')[0];
            valorTotal.innerText = objLinha.valorTotal;
            lblValorHora.innerText = objLinha.valorCategoria;
            
            return true;
        } else {
            return false;
        }
  }

  dialogo = $( "#formularioDoDialogo" ).dialog({
    autoOpen: false,
    height: 400,
    width: 400,
    modal: true,
    buttons: {
      "Atualizar dados da reserva ": function() {
        enviarReservaDeCarro("Atualiza"); // Função do arquivo `dados.js`
      }, 
      "Cancelar": fecharModal
    }
  });
});