/**
 * Funções para a realização da manipulação de dados da página de reserva
 */

var carrosDisponiveis = [];
var categoriasDisponiveis = [];

/**
 * Função para recuperar todas as categorias disponíveis para reserva e seus
 * valores
 */
function recuperarCategorias() {
    const ajax = new XMLHttpRequest();

    ajax.open("GET", "/categoria/listar");
    ajax.send();

    let categorias = document.getElementsByName("categoria")[0];
    let categoriasInseridas = [];
    categoriasDisponiveis = [];
    ajax.onreadystatechange = function() {
        JSON.parse(ajax.responseText).forEach((elemento) => {
            let elementoSelect = document.createElement("option");
            
            // Verificador para evitar duplicatas no `select`
            if (!categoriasInseridas.includes(elemento.nome)) {
                categoriasDisponiveis.push(elemento);
                // Cria elemento e insere no `select` de categorias
                elementoSelect.innerHTML = elemento.nome;
                elementoSelect.id = elemento.idCategoria;
                elementoSelect.value = elemento.valorPorHora;

                categorias.appendChild(elementoSelect);
                categoriasInseridas.push(elemento.nome);
            }
        });
    }
}

/**
 * Função para buscar os carros de uma determinada categoria
 * e preencher o campo de carros disponíveis para determinada categoria
 * @param categoria {String}
 * @param carros {Select Element}
 */
function buscarCarroPorCategoria(categoria, carros) {
    $.get("/carro/listar/categoria", {
        "categoria": categoria
    }).done((dados) => {
        if (carros == undefined) {
            let carros = document.getElementsByName('modelo')[0];
        }
        if ('erro' in dados) {
            // Removendo todos os elementos atuais do select
            removeTodosOsOptions(carros.options);
            alert('Não há carros disponíveis para esta categoria');
        } else {
            carrosDisponiveis = dados;
            let carrosInseridos = [];

            // Removendo todos os elementos atuais do select
            removeTodosOsOptions(carros.options);
        
            dados.forEach((dado) => {
                let elementoSelect = document.createElement('option');
                
                // Verificador para evitar duplicatas no `select`
                if (!carrosInseridos.includes(dado.modelo)) {
                    // Cria elemento e insere no `select` de modelos
                    elementoSelect.id = dado.idCarro;
                    elementoSelect.innerHTML = dado.modelo;
    
                    carros.appendChild(elementoSelect);
                    carrosInseridos.push(elementoSelect.modelo);
                }
            });
        }
    });
}

/**
 * Função para realizar o envio dos dados da nova reserva
 */
function enviarReservaDeCarro(tipoDeEnvio) {
    
    // Realiza uma atualização automática dos valores
    try {
        calculaValorTotal();
    } catch(e) {
        alert('Todos os campos devem ser preenchidos corretamente!');
        return;
    }
    
    try {
        // Busca o ID da reserva
        let idReserva;
        if (tipoDeEnvio === "Atualiza") {
            idReserva = document.getElementById('idReserva').innerText;
        } else {
            idReserva = 0;
        }

        let dataInicialReserva = document.getElementsByName("dataInicial")[0].value;
        let dataFinalReserva = document.getElementsByName("dataFinal")[0].value;
        // Funções de recuperação disponíveis no script `manipulacao.js`
        let carroSelecionado = recuperaIDSelecionadoNosCarrosDisponiveis();
        let categoriaSelecionado = recuperaNomeSelecionadoNaCategoria();
        // Recuperando os valores
        let valorTotal = parseFloat(document.getElementById('valorTotal').innerHTML);

        // Objeto de dados
        let dadosObjeto = {
            idReserva: idReserva,
            dataInicial: dataInicialReserva,
            dataFinal: dataFinalReserva,
            carroSelecionado: carroSelecionado,
            categoriaSelecionado: categoriaSelecionado,
            valorTotal: valorTotal,
            tipoDeEnvio: tipoDeEnvio,
            status: "PENDENTE"
        };

        if (validaDadosDoFormulario(dadosObjeto)) {
            if (tipoDeEnvio !== 'Atualiza') {
                $.post('/reserva/carro', dadosObjeto, function() {
                    alert('Reserva criada com sucesso!');
                    location.reload();
                }).fail(function() {
                    alert('Verifique todos os campos! \n\
                            Eles precisam ser preenchidos corretamente!');
                });
            } else {
                $.ajax({
                    url: '/reserva/carro',
                    type: 'PUT',
                    data: $.param(dadosObjeto),
                    success: function () {
                        alert('Reserva atualizada com sucesso!');
                        location.reload();
                    }
                }).fail(function () {
                    alert('Verifique todos os campos! \n\
                            Eles precisam ser preenchidos corretamente!');
                });
            }
        } else {
            alert('Todos os campos devem ser preenchidos corretamente!');
        }
    } catch(e) {
        alert('Preencha todos os campos!');
        return;
    }
}

/**
 * Função para remover todos os options de um select
 */
function removeTodosOsOptions(options) {
    // Caso não seja vazio, realiza o processo 
    if (options.selectedIndex != -1) {
        Array.from(options).forEach((opt) => {
            if (opt.innerHTML != 'Selecione') {
                opt.remove();
            }
        });
    }
}


/**
 * Função para buscar categoria por ID, nas categorias disponíveis
 */
function buscaNasCategoriasDisponiveis(idCategoria) {
    let categoriaEscolhida;
    categoriasDisponiveis.forEach(function(categoria) {
        if (categoria.idCategoria == idCategoria) {
            categoriaEscolhida = categoria;
        }
    });
    return categoriaEscolhida;
}


/**
 * 
 * Recupera o fabricante do backend utilizando o id e insere o resultado
 * em um elemento da página
 * 
 * @param {Integer} idFabricante 
 */
function recuperarFabricantePorIDEInsereEmElemento(idFabricante, elemento) {
    $.get("/fabricante/lista/id", {
        "id": idFabricante
    }).done((fabricante) => {
        elemento.innerHTML = fabricante.nome;
        elemento.value = fabricante.idFabricante;
    });
}


/**
 * Função para validar os dados do formulário de criação de reserva
 * 
 * @param {Object} dados Objeto com todos os dados precisam ser validados
 * 
 */
function validaDadosDoFormulario(dados) {
    let dadosValidos = true;
    Object.keys(dados).forEach((dado) => {
        if (isNaN(dados[dado]) && !typeof(dados[dado]) === "string" || dados[dado] === "" 
                || dados[dado] === "Selecione") {
            dadosValidos = false;
        }
    });
    
    // Validando a data inserida pelo usuário
    let inicio = new Date(dados.dataInicial);
    let final = new Date(dados.dataFinal);
    if (inicio > final)
        dadosValidos = false;
    
    return dadosValidos;
}


/**
 * Função para a exclusão de reservas
 */

function excluiReservaDoUsuario(idReserva) {
    $.ajax({
        url: '/reserva/usuario' + '?' + $.param({
            idReserva: idReserva
        }),
        type: 'DELETE'
    }).done(function(result) {
        alert("Exclusão da reserva feita com sucesso");
        location.reload();
    });
}
