/**
 * Funções para a manipulação geral dos elementos da página de reserva
 * do usuário 
 */


/**
 * Função para recuperar o elemento selecionado no `select`
 * dos carros disponíveis
 */
function recuperaIDSelecionadoNosCarrosDisponiveis() {
    let selectCarrosDisponiveis = document.getElementsByName('modelo')[0];
    let elementoSelecionado = selectCarrosDisponiveis[selectCarrosDisponiveis.selectedIndex];

    return elementoSelecionado.id;
}

/**
 * Função para recuperar o elemento selecionado no `select`
 * das categorias
 */
function recuperaNomeSelecionadoNaCategoria() {
    let selectCategorias = document.getElementsByName('categoria')[0];
    let elementoSelecionado = selectCategorias[selectCategorias.selectedIndex];

    return elementoSelecionado.innerHTML;
}
