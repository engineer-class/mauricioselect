/**
 * Eventos gerais dos elementos presentes nas páginas de reserva
 * Criado utilizando clojures
 */

 /**
  * Função para aplicar comportamentos padrões a certos elementos
  * da página (Página de reserva do usuário)
  */
(function() {
    let carrosDisponiveis = document.getElementsByName('modelo')[0];
    let botaoInfoCarro = document.getElementById('verificaInfosCarro');

    // Disabilitando estes botões (Ao carregar o script)
    carrosDisponiveis.disabled = true;
    botaoInfoCarro.disabled = true;

    // Adicionado eventos aos botões
    let botaoCalculaValorTotal = document.getElementById('calculaValores');
    botaoCalculaValorTotal.addEventListener('click', function() {
        // Adicionando função presente no script `reserva-usuario/calculos.js`
        try {
            calculaValorTotal();
        } catch(erro) {
            alert('Ops! Antes de calcular os preços preencha todos os campos!');
        }
    });

    let botaoEnviaReserva = document.getElementsByName('confirmar')[0];
    botaoEnviaReserva.onclick = function() {
        enviarReservaDeCarro("Cria");
    };
})();


 /**
  * Função para adiconar os eventos do `select` nomeado `categoria` na página
  * de reserva do usuário
  */
(function() {
    let categorias = document.getElementsByName('categoria')[0];
    let carrosDisponiveis = document.getElementsByName('modelo')[0];

    // Onchange para a alteração dos valores de acordo com a categoria
    categorias.onchange = function() { 
        let indiceSelecionado = categorias.selectedIndex;
        let elementoSelecionado = categorias[indiceSelecionado];
        let labelValor = document.getElementById('lblValorHora');
        let modelo = document.getElementsByName("modelo")[0];

        if (elementoSelecionado.innerHTML != "Selecione") {
            // Recupera o valor e insere no campo correto
            let valor = elementoSelecionado.value;
            labelValor.innerHTML = valor;
            carrosDisponiveis.disabled = false;

            // Função chamada para preencher os campos com os carros 
            // disponíveis  na categoria selecionada pelo usuário
            // Função vinda do arquivo `dados.js`
            buscarCarroPorCategoria(elementoSelecionado.innerHTML, modelo);
        } else {
            labelValor.innerHTML = 0.00;
            carrosDisponiveis.selectedIndex = 0;
            carrosDisponiveis.disabled = true;
        }
    };
})();

/**
 * Função para adicionar eventos ao `select` nomeado de `modelo` na página
 * de reserva do usuário
 */
(function() {
    let modelo = document.getElementsByName('modelo')[0];
    let botaoInfoCarro = document.getElementById('verificaInfosCarro');

    modelo.onchange = function() {
        let indiceSelecionado = modelo.selectedIndex;
        let elementoSelecionado = modelo[indiceSelecionado];
        
        if (elementoSelecionado.innerHTML != "Selecione") {
            botaoInfoCarro.disabled = false;
        } else {
            botaoInfoCarro.disabled = true;
        }
    }   
})();


/**
 * Função para criação do modal (Adiciona também eventos as opções do modal)
 */
$(function() {
    var caixaDialogo;
    
    document.getElementById('verificaInfosCarro').onclick = function() { 
        limpaTodosOsCamposDoFormulario();
        preencheFormulario();
        caixaDialogo.dialog( "open" ); 
    };

    function preencheFormulario() {
        let modelo = document.getElementsByName('modelo')[0];
        let modeloSelecionado = modelo[modelo.selectedIndex].innerHTML;
        let formIDCarro = document.getElementById('formIDCarro');
        let formCategoria = document.getElementById('formCategoria');
        let formFabricante = document.getElementById('formFabricante');
        let formModelo = document.getElementById('formModelo');
        let formAnoCarro = document.getElementById('formAnoCarro');

        // `carrosDisponiveis` variável global do arquivo de script
        // `dados.js`
        carrosDisponiveis.forEach((carro) => {
            if (carro.modelo === modeloSelecionado) {
                // Busca nos dados globais do script `dados.js`
                let categoriaDoCarro = buscaNasCategoriasDisponiveis(carro.categoria.idCategoria);

                // Busca fabricante por id e insere em um elemento
                // Função disponível no script `dados.js`
                recuperarFabricantePorIDEInsereEmElemento(carro.fabricante.idFabricante, formFabricante);

                // Preenche os demais campos
                formAnoCarro.innerHTML = carro.ano;
                formIDCarro.innerHTML = carro.idCarro;
                formModelo.innerHTML = carro.modelo;
                formCategoria.innerHTML = categoriaDoCarro.nome;
            }
        });
    }

    /**
     * Função para limpar todos os campos do formulário utilizado no modal
     */
    function limpaTodosOsCamposDoFormulario() {
        formFabricante.innerHTML = "";
        formAnoCarro.innerHTML = 0;
        formIDCarro.innerHTML = 0;
        formModelo.innerHTML = "";
        formCategoria.innerHTML = "";
    }
        
    caixaDialogo = $( "#formularioModalInformacoesCarro" ).dialog({
      autoOpen: false,
      height: 400,
      width: 400,
      modal: true
    });
});
