/**
 * Funções com os cálculos realizados na página de reserva do usuário
 */


/**
 * Função para calcular o valor total da reserva do usuário
 */
function calculaValorTotal() {

    // Elementos
    let valorTotal = document.getElementById('valorTotal');

    // Informações
    let valorDaCategoria = parseFloat(document.getElementById('lblValorHora').innerHTML);
    let dataInicial = document.getElementsByName('dataInicial')[0].valueAsDate;
    let dataFinal = document.getElementsByName('dataFinal')[0].valueAsDate;
    
    // Calcula a quantidade de dias entre os dois dias selecionados pelo usuário
    // A função utilizada está dentro do arquivo `manipuladorDeData.js`
    let diasDeDistancia = distanciaEntreAsDatas(dataInicial, dataFinal);

    // Calcula o valor total utilizando as horas
    let valorTotalCalculado = (diasDeDistancia * 24) * valorDaCategoria;
    valorTotal.innerHTML = valorTotalCalculado;
}


/**
 * Função para adicionar eventos aos `inputs` de valores presentes na janela 
 * de reserva do usuário
 */
/*
(function() {
    const labelValorTotal = document.getElementById('valorTotal');
    const inputValorPagoAntecipado = document.getElementById('valorPagoAntecipado');
    const inputValorPagoNaDevolucao = document.getElementById('valorPagoNaDevolucao');
    
    inputValorPagoAntecipado.addEventListener('change', function() {
        let valorTotal = parseFloat(labelValorTotal.innerHTML);
        let valorPagoAntecipado = parseFloat(inputValorPagoAntecipado.value);
        let valorAlterado = valorTotal - valorPagoAntecipado;

        if (valorAlterado > valorTotal || valorAlterado < 0) {
            // Avisa o usuário e retorna ao padrão!
            alert('Valor inválido!');

            inputValorPagoAntecipado.value = valorTotal / 2;
            inputValorPagoNaDevolucao.value = valorTotal / 2;
        } else {
            inputValorPagoNaDevolucao.value = valorAlterado;
        }
    });   
    
    inputValorPagoNaDevolucao.addEventListener('change', function() {
        let valorTotal = parseFloat(labelValorTotal.innerHTML);
        let valorPagoNaDevolucao = parseFloat(inputValorPagoNaDevolucao.value);
        let valorAlterado = valorTotal - valorPagoNaDevolucao;

        if (valorAlterado > valorTotal || valorAlterado < 0) {
            // Avisa o usuário e retorna ao padrão!
            alert('Valor inválido!');

            inputValorPagoAntecipado.value = valorTotal / 2;
            inputValorPagoNaDevolucao.value = valorTotal / 2;
        } else {
            inputValorPagoAntecipado.value = valorAlterado;
        }
    }); 
})();
*/