/**
 * 
 * @param {type} url: Endereço para onde o post será enviado
 * @param {type} data: Dados que serão enviados no POST
 * @param {type} funcaoResposta: Função chamada para indicar que tudo deu certo
 * @returns {undefined}
 */
function ajaxPostDadosJQuery(url, data, funcaoResposta) {
    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        success: funcaoResposta
    });
}

/**
 * Função para realizar ajax implementado no JS puro
 * 
 * @returns {undefined}
 */
function ajaxPostDadosJSPuro() {
    
}