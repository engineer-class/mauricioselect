/***
 * Função para validar os parâmetros do carro
 * 
 * @return boolean
 */
function validaCarro(carro) {

    let retorno = true;
    // função disponível em js/reserva-usuario/dados.js
    if (validaDadosDoFormulario(carro)) {
        if (carro["anoDoCarro"] < 1990 || carro["anoDoCarro"] > 9999 ||
            carro["quilometragem"] < 0 ||
            carro["tanqueDoCarro"] < 0 || carro["tanqueDoCarro"] > 100)
            retorno = false;
    }
    return retorno;
}