// atualiza valor pago na devolução após o carregamento da página e desabilita o evento action do input valor pago antecipadamente
window.onload = () =>{
    let form = document.getElementsByTagName("form")[0];
    atualizaValorPagoNaDevolucao();
    form.action = 'javascript:void(0)';
};

function recuperaUsuario() {
    let usuario = document.getElementById("usuario");
    return {
        "idUsuario": usuario.children.idUsuario.innerText,
        "nome": usuario.children.nome.innerText,
        "sexo": usuario.children.sexo.innerText,
        "rg": usuario.children.rg.innerText,
        "email": usuario.children.email.innerText,
        "cpf": usuario.children.cpf.innerText,
        "apelido": usuario.children.apelido.innerText
    };
}

function recuperaCarro() {
    let carro = document.getElementById("carro");
    return {
        "idCarro": carro.children.idCarro.innerText,
        "modeloCarro": carro.children.modeloCarro.innerText,
        "quilometragem": carro.children.quilometragem.innerText,
        "placa": carro.children.placa.innerText,
        "estadoDeConservacao": carro.children.estadoDeConservacao.innerText,
        "cor": carro.children.cor.innerText,
        "tanque": carro.children.tanque.innerText,
        "ano": carro.children.ano.innerText,
        "fabricante": carro.children.fabricante.innerText,
        "idCategoria": carro.children.idCategoria.innerText,
        "valorPorHora": carro.children.valorPorHora.innerText
    };
}

function recuperaReserva() {
    let reserva = document.getElementById("reserva");
    return {
        "idReserva": reserva.children.idReserva.innerText,
        "dataInicio": reserva.children.dataInicio.innerText,
        "dataFinal": reserva.elements.dataFinal.value,
        "status": reserva.elements.status.value,
        "valorTotal": reserva.children.valorTotal.innerText
    };
}

/*
 * Função para recuperar dados relacionados ao contrato de locação (Aluguel)
 */
function pegaDadosAluguel() {
    let aluguel = document.getElementById("aluguel");
    return {
        "valorPagoAntecipado": aluguel.children.valorAntecipado.value,
        "valorPagoDevolucao": aluguel.children.valorPagoDevolucao.innerText
    };
}

var botao = document.getElementById("aprovarReserva");

botao.addEventListener("click", function () {

    let reserva = recuperaReserva();
    let aluguel = pegaDadosAluguel();

    if (reserva.valorTotal > parseInt(aluguel.valorPagoAntecipado)) {
        switch (reserva.status) {
            case "APROVADA":
                jqueryAjax("/reserva/aprova");
                break;
            case "REPROVADA":
                jqueryAjax("/reserva/reprova/pendente");
                break;
            case "PENDENTE":
                jqueryAjax("/reserva/reprova/pendente")
                break;
            default:
                alert("A reserva não está com os campos preenchidos corretamente!");
        }

    }
}
);

var valorAntecipado = document.getElementById("valorAntecipado");

// atualiza o vaor pago na devolução sempre que o valor antecipado for atualizado
valorAntecipado.addEventListener("change", () => {
    atualizaValorPagoNaDevolucao();
});

var dataFinal = document.getElementById("dataFinal");

// atualiza valor total e valor pago na devolução sempre que a data for alterada
dataFinal.addEventListener("change", function (){
    let reserva = recuperaReserva();
    let carro = recuperaCarro();
    let aluguel = pegaDadosAluguel();

    let valorTotal = document.getElementById("valorTotal");
    let valorPagoDevolucao = document.getElementById("valorPagoDevolucao");
    // Função disponível em js/manipuladorDeData.js
    let distanciaEntreDias = distanciaEntreAsDatas(new Date(reserva.dataInicio), dataFinal.valueAsDate);
    let total = (distanciaEntreDias * 24) * carro.valorPorHora;
    
    if(total - aluguel.valorPagoAntecipado < 0){
        alert("Data inválida");
        botao.disabled = true;
    }else{
        valorTotal.innerText = total;
        valorPagoDevolucao.innerText = total - aluguel.valorPagoAntecipado;
    }
});

/**
 * 
 * Função para atualizar o valor que será pago na devolução
 * 
 */

function atualizaValorPagoNaDevolucao(){
    let aluguel = document.getElementById("aluguel").children;
    let reserva = document.getElementById("reserva").children;
    let valorPagoDevolucao = reserva.valorTotal.innerText - aluguel.valorAntecipado.value;
    aluguel.valorPagoDevolucao.innerText = valorPagoDevolucao;
}

/**
 * Função para aprovar ou reprovar reservas
 * via método ajax da biblioteca jquery
 * 
 * @param {String} url 
 */
function jqueryAjax(url) {
    let usuario = recuperaUsuario();
    let carro = recuperaCarro();
    let reserva = recuperaReserva();
    let aluguel = pegaDadosAluguel();

    $.ajax({
        method: "PUT",
        url: url,
        data: {
            "usuario": usuario,
            "carro": carro,
            "reserva": reserva,
            "aluguel": aluguel
        }
    }).done(
        function (resposta) {
            alert(resposta['mensagem']);
            location.pathname = "/reserva/listar";
        });
}