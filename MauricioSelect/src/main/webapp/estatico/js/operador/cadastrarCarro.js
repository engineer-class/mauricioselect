recuperarValores("GET", "/categoria/listar", document.getElementsByName("categoria")[0]);
recuperarValores("GET", "/fabricante/listar", document.getElementsByName("fabricante")[0]);

var botao = document.getElementsByTagName("button")[0];
/***
 * Função para enviar requisição de cadastro de carro ao servidor
 * 
 */
botao.addEventListener('click', function () {

    let carro = stringToJson($("form").serializeArray());
    
    // Função disponível em js/validadores/validadorCarro.js
    if (validaCarro(carro)) {
        $.post("/carro/cadastrar",
            carro,
            function (resposta) {
                alert(resposta['mensagem']);
                location.reload();
            }
        );
    }else{
        alert("Preencha os campos corretamente para cadastrar o carro!");
    }
});

/***
 * Função para fazer o parse do método serialize() do ajax
 * 
 * @return JSONObject carro
 * 
 */
function stringToJson(form) {

    let jsonParametros = [];
    for (let i = 0; i < form.length; i++) {
        jsonParametros.push(form[i]["value"]);
    }

    return {
        "categoria": jsonParametros[0],
        "fabricante": jsonParametros[1],
        "conservacao": jsonParametros[2],
        "anoDoCarro": jsonParametros[3],
        "placaDoCarro": jsonParametros[4],
        "modeloDoCarro": jsonParametros[5],
        "corDoCarro": jsonParametros[6],
        "quilometragem": jsonParametros[7],
        "tanqueDoCarro": jsonParametros[8]
    };
}