/**
 * Script que utiliza JQuery para criar um modal na tela do usuario, este por sua
 * vez, e utilizado para a ediçao de reservas.
 * Ha funçoes fora do clojure que facilitam o processo de tratamento
 * @returns {undefined}
 */

$(function() {
    
    // Dados gerais
    let dialog;
    let objReserva;
    let idReserva = document.getElementById('idReserva');
    let apelido = document.getElementById('apelido');
    let categoria = document.getElementById('categoria');
    let modeloCarro = document.getElementById('modeloCarro');
    let dataInicial = document.getElementById('dataInicial');
    let dataFinal = document.getElementById('dataFinal');
    let status = document.getElementById('status');
    let valorAntecipado = document.getElementById('valorAntecipado');
    let valorTotalDaReserva = document.getElementById('valorTotal');
    let valorTotalAPagar = document.getElementById('totalPagar');

    // Dados de entrega
    let valorTotalDaReservaParaEntrega = document.getElementById('valorTotalDaReserva');
    let valorTotalAPagarParaEntrega = document.getElementById('valorTotalEntrega');
    
    const ESTADO_DE_CONSERVACAO = {
        'BOM': 0,
        'MEDIO': 15,
        'BAIXO': 30
    };
    const ESTADO_JA_ALTERADO = {"ULTIMO_VALOR": 0, "JA_ALTERADO": false};
    
    // Evento JS
    document.getElementById('atualizarAluguel').addEventListener('click', () =>  {
        let retorno = preencheFormulario();
        
        if (retorno) {
            dialog.dialog("open");
        } else {
            alert('Reservas finalizadas não podem ser editadas!');
        }
    });
    
    dataFinal.addEventListener("change", function() {
        let total = calculaValorTotalDoAluguel(stringEmData(dataFinal.value));
        
        if ((total - parseFloat(valorAntecipado.innerText)) < 0) {
            alert('É necessário que o consumo do valor antecipado seja feita. Devoluções não são aceitas!');
            dataFinal.value = objReserva.dataFinal;
            valorTotalDaReserva.innerText = objReserva.valorTotal;
            return;
        } else {
            valorTotalDaReserva.innerText = total;
            valorTotalAPagar.innerText = total - parseFloat(valorAntecipado.innerText);
        }
    });
    
    // Adição de eventos aos botões de observações para a finalização do carro
    let obsCarroQuebrado = document.getElementById('obsCarroQuebrado');
    let valorJaInserido = false;
    obsCarroQuebrado.addEventListener('click', function() {
        if (calculaValoresDaEntrega()) {                           
            let valorTotal = parseFloat(valorTotalDaReservaParaEntrega.innerText);
            let valorPagoAntecipado = parseFloat(valorAntecipado.innerText);
            
            if (valorJaInserido) {
                valorTotal -= 3000;
                valorJaInserido = false;
            }

            if (obsCarroQuebrado.checked) {
                valorTotal += 3000;
                valorJaInserido = true;
            } 

            valorTotalDaReservaParaEntrega.innerText = valorTotal;
            valorTotalAPagarParaEntrega.innerText = valorTotal - valorPagoAntecipado;
        }
    });
    
    let obsTanque = document.getElementById('obsTanque');
    let valorJaAlterado = false;
    let valorTotalOriginal = 0;
    obsTanque.addEventListener('change', function () {
        if (calculaValoresDaEntrega()) {  
            // Evento que atualiza os valores (Aplica multa) caso o tanque do carro
            // esteja com menos de 80% do valor total
            let valorDoTanque = parseInt(obsTanque.value);
            let valorTotal = parseFloat(valorTotalDaReservaParaEntrega.innerText);
            let valorPagoAntecipado = parseFloat(valorAntecipado.innerText);  
            
            if (valorDoTanque <= 0 || valorDoTanque > 100) {
                obsTanque.value = 0;
                alert('Valor do tanque inválido!');
                return;
            }
            
            if (valorDoTanque < 80) {
                // Caso o valor não tenha cido alterado
                if (!valorJaAlterado) { 
                    valorTotalOriginal = valorTotal;
                    valorTotal += 300;
                    
                    valorTotalDaReservaParaEntrega.innerText = valorTotal;
                    valorTotalAPagarParaEntrega.innerText = (valorTotal - valorPagoAntecipado);
                    valorJaAlterado = true;
                }
            } else {
                if (valorTotalOriginal === 0) {
                    valorTotalOriginal = parseFloat(valorTotalDaReservaParaEntrega.innerText);
                }
                
                valorTotalDaReservaParaEntrega.innerText = valorTotalOriginal;
                valorTotalAPagarParaEntrega.innerText = (valorTotalOriginal - valorPagoAntecipado);
                valorJaAlterado = false;
            }
        }
    });
    
    let obsQuilometragem = document.getElementById('obsQuilometragem');
    obsQuilometragem.addEventListener('change', function () {
        // Evento para realizar uma rápida verificação do valor inserido na 
        // quilometragem rodada
        let valorQuilometragem = parseFloat(obsQuilometragem.value);
        if (valorQuilometragem <= 0) {
            obsQuilometragem.value = 0;
            alert('Valor de quilometragem inserido é inválido!');
            return;
        }
    });
    
    let obsConservacao = document.getElementById('obsConservacao');
    obsConservacao.addEventListener('change', function() {
        if (calculaValoresDaEntrega()) {            
            let valorSelecionado = ESTADO_DE_CONSERVACAO[obsConservacao.value];
            let valorTotal = parseFloat(valorTotalDaReservaParaEntrega.innerText);
            let valorPagoAntecipado = parseFloat(valorAntecipado.innerText);  
            
            if (!ESTADO_JA_ALTERADO.JA_ALTERADO) {
                ESTADO_JA_ALTERADO.JA_ALTERADO = true;
                ESTADO_JA_ALTERADO.ULTIMO_VALOR = valorTotal;
            } else {
                valorTotal = ESTADO_JA_ALTERADO.ULTIMO_VALOR;
            }
            
            valorTotalDaReservaParaEntrega.innerText = valorTotal + valorSelecionado;
            valorTotalAPagarParaEntrega.innerText = ((valorTotal + valorSelecionado) 
                                                                    - valorPagoAntecipado);
        }
    });
    
   /**
    * Função para a exclusão do aluguel
    */
   function excluiAluguel() {   
       let ajax = new XMLHttpRequest();
       ajax.open('delete','/aluguel/deleta?id=' + objReserva.idAluguel, true);
       ajax.send();
       ajax.onload = () => {
           alert("Aluguel excluído com sucesso");
           location.reload();
       };
       dialog.dialog( "close" );
   }
   
   /**
    * Função para a finalização do aluguel
    * @returns
    */
   function receberAluguel() {
        if (calculaValoresDaEntrega()) {  
            let objModal = recuperaModal();
            let objObservacoes = recuperaObservacoesModal();
            let dadosDeEntrega = montaObjetoDeEntrega(objObservacoes, objModal);
            
            if (validaDadosDoFormulario(dadosDeEntrega)) {
                $.post("/aluguel/finaliza", dadosDeEntrega, function(retorno) {
                    alert(retorno.mensagem);
                    location.reload();
                });
                dialog.dialog( "close" );           
            } else {
                alert("Há campos que precisam ser preenchidos corretamente!");
            }
        }
   }
   
   /**
    * Função para atualizar o aluguel
    */
   function atualizaAluguel() {
       let objModal = recuperaModal();
       let dadosDeAtualizacao = montaObjetoDeAtualizacao(objModal);
       
       if (validaDadosDoFormulario(dadosDeAtualizacao)) {
            $.post("/aluguel/atualiza", dadosDeAtualizacao, function(retorno) {
                alert(retorno.mensagem);
                location.reload();
            });
            dialog.dialog( "close" );           
       } else {
           alert("Há campos que precisam ser preenchidos corretamente!");
       }
   }
    
    /**
     * 
     * Insere as informaçoes do campo selecionado pelo usuario no formulario para 
     * ediçao.
     * 
     * @returns {void}
     */
    function preencheFormulario() {
          let linhaSelecionada = buscaSelecionado();
          objReserva = recuperaLinhaMatriz(linhaSelecionada);
          
          // Pequena verificação para evitar qualquer mudança em alugueis 
          // finalizados
          if (objReserva.status === "FINALIZADO") {
              return false;
          }
          
          // Insere em cada elemento do formulário os dados da linha selecionada
          // na matriz de reservas
          idReserva.innerText = objReserva.idReserva;
          apelido.innerText = objReserva.apelido;
          modeloCarro.innerText = objReserva.modeloCarro;
          categoria.innerHTML = objReserva.categoria;
          dataInicial.innerText = new Date(objReserva.dataInicial).toJSON().split('T')[0];
          dataFinal.value = new Date(objReserva.dataFinal).toJSON().split('T')[0];
          status.innerText = objReserva.status;
          valorTotalDaReserva.innerText = objReserva.valorTotal;
          valorAntecipado.innerText = objReserva.valorPagoAntecipado;
          valorTotalAPagar.innerText = parseFloat(valorTotalDaReserva.innerText) 
                                                - parseFloat(valorAntecipado.innerText);
           
          return true;
    }
    
    /**
     * Função para recuperar os dados presentes no Modal
     */
    function recuperaModal(){
        return {
            'idAluguel' : objReserva.idAluguel,
            'idReserva': idReserva.innerText,
            'apelido': apelido.innerText,
            'idCategoria': objReserva.categoria.innerHTML,
            'modeloCarro': modeloCarro.innerText,
            'dataInicial': dataInicial.innerText,
            'dataFinal': dataFinal.value,
            'pagoAntecipado': valorAntecipado.value,
            'valorTotalDaReserva' : valorTotalDaReserva.innerText,
            'valorTotalAPagar': valorTotalAPagar.innerText,
            'status': status.innerText
       };
    }
    
    /**
     * Função para recuperar os parâmetros de observação para a devolução do carro
     */
    function recuperaObservacoesModal() {
        let obsCarroQuebradoBool = document.getElementById('obsCarroQuebrado').checked;
        
        let obsCarroQuebrado = 0;
        if (obsCarroQuebradoBool) {
            obsCarroQuebrado = 1;
        }
        
        let obsTanque = document.getElementById('obsTanque').value;
        let obsQuilometragem = document.getElementById('obsQuilometragem').value;
        let obsConservacao = document.getElementById('obsConservacao').value;
        
        return {
            'obsCarroQuebrado': obsCarroQuebrado,
            'obsTanque': obsTanque,
            'obsQuilometragem': obsQuilometragem,
            'obsConservacao': obsConservacao
        };
    }
  
    /**
     * Função para montar a estrutura que será enviada para a entrega do 
     * aluguel
     * 
     * @param {type} objObservacoes Objeto de observações
     * @param {type} objModal Objeto de dados do modal
     * @returns {undefined}
     */
    function montaObjetoDeEntrega(objObservacoes, objModal) { 
        let dataDeAgora = new Date().toJSON().split('T')[0];
        return {
            'carroEstaQuebrado': objObservacoes.obsCarroQuebrado,
            'carroTanque': objObservacoes.obsTanque,
            'carroQuilometragem': objObservacoes.obsQuilometragem,
            'idAluguel': objModal.idAluguel,
            'dataFinal': dataDeAgora, // Pega a data atual 
            'valorTotalDaReserva': valorTotalDaReservaParaEntrega.innerText,
            'status': objModal.status,
            'conservacao': objObservacoes.obsConservacao
        };        
    }
    
    /**
     * Função para montar o objeto a ser enviado para a atualização do aluguel
     * @param {type} objModal
     * @returns {undefined}
     */
    function montaObjetoDeAtualizacao(objModal) {
        return {
            'idReserva': objModal.idReserva,
            'idAluguel': objModal.idAluguel,
            'dataFinal': objModal.dataFinal,
            'valorTotalDaReserva': objModal.valorTotalDaReserva,
            'valorTotalAPagar': objModal.valorTotalAPagar,
            'status': objModal.status
        }; 
    }
    
    /**
     * Função para calcular o valor total do aluguel
     */
    function calculaValorTotalDoAluguel(dataFinal) {         
        let distanciaEntreDias = distanciaEntreAsDatas(stringEmData(dataInicial.innerText), 
                                                                        dataFinal);
        return (distanciaEntreDias * 24) * objReserva.valorDaCategoria;
    }

    /**
     * Função para calcular os valores da entrega
     */
    function calculaValoresDaEntrega() {
        if (valorTotalDaReservaParaEntrega.innerText === "0.00") {
            // Calcula o valor total da entrega para o dia atual
            let valorTotalDaEntrega = calculaValorTotalDoAluguel(stringEmData(
                new Date().toJSON().split('T')[0]
            ));
            // Gerando os valores para verificação
            let valorAPagar = valorTotalDaEntrega - parseFloat(valorAntecipado.innerText);
            
            if (valorAPagar < 0) {
                alert('Só é possível devolver o carro após consumir todo o valor pago antecipado');
                return false;
            }
            valorTotalDaReservaParaEntrega.innerText = valorTotalDaEntrega;
            valorTotalAPagarParaEntrega.innerText = valorAPagar;
        }
        return true;
    }
  
    dialog = $( "#formularioReservaDialogo" ).dialog({
      autoOpen: false,
      height: 465,
      width: 520,
      modal: true,
      buttons: {
        "Receber carro": receberAluguel,
        "Atualizar aluguel": atualizaAluguel,
        "Excluir aluguel": excluiAluguel
      }
    });
});

/**
 * Função para o tratamento dos eventos de listagem na página
 */
(function () {
    let visualizarDisponiveis = document.getElementById("visualizarDisponiveis");
    let visualizarIndisponiveis = document.getElementById("visualizarIndisponiveis");
    let visualizarTodos = document.getElementById("visualizarTodos");
    
    visualizarDisponiveis.onclick = () => {
        location.pathname = "/aluguel/listar/alugado";
    };
    visualizarIndisponiveis.onclick = () => {
        location.pathname = "/aluguel/listar/finalizado";
    };
    visualizarTodos.onclick = () => {
        location.pathname = "/aluguel/listar/todos";
    };
})();
  
/**
 * Funçao para realizar a busca do elemento selecionado
 * 
 * @returns {int}
 */
function buscaSelecionado() {
    let elements = document.getElementsByTagName("input");

    for (var i = 0; i < elements.length; i++) {
        if (elements[i].checked) {
            return i;
        }
    }
}

/**
 * Funçao para buscar a linha da matriz que esta sendo editada
 * 
 * @param {type} linhaSelecionada Linha selecionada pelo usuario para ediçao
 * @returns {recuperaLinhaMatriz.formularioAtualizaReservaAnonym$0}
 */
function recuperaLinhaMatriz(linhaSelecionada) { 
        
    let matriz = document.getElementById("tabelaAluguel");
    let linha;
    
    try {
        linha = matriz.rows[linhaSelecionada + 1].cells;
    } catch (e) {
        linha = matriz.rows[linhaSelecionada].cells;
    }
    
    return {
            'idAluguel': linha[0].innerText, 
            'idReserva': linha[1].innerText,
            'apelido': linha[2].innerText,
            'operador': linha[3].innerText,
            'modeloCarro': linha[4].innerText,
            'categoria': linha[5].innerText,
            'valorDaCategoria': linha[6].innerText,
            'dataInicial': linha[7].innerText,
            'dataFinal': linha[8].innerText,
            'valorPagoAntecipado': linha[9].innerText,
            'valorTotal' : linha[10].innerText,
            'status': linha[11].innerText  
    };
}

/**
 * Função para validar os dados do formulário de finalização/atualização
 * 
 * @param {type} dados
 * @returns {Boolean}
 */
function validaDadosDoFormulario(dados) {
    let dadosValidos = true;
    Object.keys(dados).forEach((dado) => {
        if (isNaN(dados[dado]) && !typeof(dados[dado]) === "string" || dados[dado] === "" 
                || dados[dado] === "Selecione") {
            dadosValidos = false;
        }
    });
    
    // Validando a data inserida pelo usuário
    let inicio = new Date(dados.dataInicial);
    let final = new Date(dados.dataFinal);
    if (inicio > final)
        dadosValidos = false;
    
    return dadosValidos;
}
