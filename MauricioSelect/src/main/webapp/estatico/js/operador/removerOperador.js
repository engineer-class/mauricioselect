/**
 * Função para serializar o formulário de remoção de operador e enviar requisição ajax para remoção do operador
 * 
 * @type String
 */

document.getElementById('botao-remove').addEventListener('click', (function () {
    // Função disponível no arquivo: js/operador/validaCadastroOperador.js
    if (validaAtualizacao()) {
        var requisicao = new XMLHttpRequest();
        let dados = {
            'nomeUsuario': document.getElementById('nomeUsuarioAtualiza').value
        };
        
        requisicao.open("POST", "/gerencia/operador", true);
        requisicao.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        // Seta paramêtros da requisição e envia a requisição
        requisicao.send($.param(dados));

        // Cria um evento para receber o retorno.
        requisicao.onreadystatechange = function() {
            alert(requisicao.responseText);
            location.reload();
        };
    }
}));