/***
 * Função para recuperar valores para o cadastro do carro disponíveis no banco de dados
 * @param {String} metodo
 * @param {String} url
 * @param {HTML Document} elemento
 * @returns {options fields}
 */
function recuperarValoresCategoria(metodo,url, elemento) {
    const ajax = new XMLHttpRequest();

    ajax.open(metodo, url);
    ajax.send();

    let elementos = elemento;
    let elementossInseridos = [];
    let elementosDisponiveis = [];
    ajax.onreadystatechange = function() {
        JSON.parse(ajax.responseText).forEach((elemento) => {
            elementosDisponiveis.push(elemento);
            let elementoSelect = document.createElement("option");

            // Verificador para evitar duplicatas no `select`
            if (!elementossInseridos.includes(elemento.nome)) {
                // Cria elemento e insere no `select` de categorias
                elementoSelect.innerHTML = elemento.nome;

                elementos.appendChild(elementoSelect);
                elementossInseridos.push(elemento.nome);
            }
        });
    };
}

function recuperarValoresCarro(metodo,url, elemento) {
    const ajax = new XMLHttpRequest();

    ajax.open(metodo, url);
    ajax.send();

    let elementos = elemento;
    let elementossInseridos = [];
    let elementosDisponiveis = [];
    ajax.onreadystatechange = function() {
        JSON.parse(ajax.responseText).forEach((elemento) => {
            elementosDisponiveis.push(elemento);
            let elementoSelect = document.createElement("option");

            // Verificador para evitar duplicatas no `select`
            if (!elementossInseridos.includes(elemento.modelo)) {
                // Cria elemento e insere no `select` de categorias
                elementoSelect.innerHTML = elemento.modelo;

                elementos.appendChild(elementoSelect);
                elementossInseridos.push(elemento.modelo);
            }
        });
    };
}

function recuperarValores(metodo,url, elemento) {
    const ajax = new XMLHttpRequest();

    ajax.open(metodo, url);
    ajax.send();

    let elementos = elemento;
    let elementossInseridos = [];
    let elementosDisponiveis = [];
    ajax.onreadystatechange = function() {
        JSON.parse(ajax.responseText).forEach((elemento) => {
            elementosDisponiveis.push(elemento);
            let elementoSelect = document.createElement("option");

            // Verificador para evitar duplicatas no `select`
            if (!elementossInseridos.includes(elemento.nome)) {
                // Cria elemento e insere no `select` de categorias
                elementoSelect.innerHTML = elemento.nome;

                elementos.appendChild(elementoSelect);
                elementossInseridos.push(elemento.nome);
            }
        });
    };
}