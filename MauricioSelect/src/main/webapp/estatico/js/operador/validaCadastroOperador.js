/**
 * Funções para a rápida validação dos dados inseridos antes do cadastro/exclusão
 * de um operador
 */

/**
 * Função para validar os dados antes da atualização
 */
function validaAtualizacao() {
    let nomeUsuarioAtualiza = document.getElementById('nomeUsuarioAtualiza').value;

    if (nomeUsuarioAtualiza.trim().length === 0) {
        alert('O campo de nome precisa ser preenchido corretamente para a exclusão!');
        return false;
    }
    return true;
}