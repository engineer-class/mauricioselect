function requisicaoAjax(reserva, url, mensagem){
    let ajax = new XMLHttpRequest();
    
    ajax.open('post',url+'?idUsuario='+reserva.apelido+
    "&idCategoria="+reserva.idCategoria+
    "&dataInicial="+reserva.dataInicial+
    "&dataFinal="+new Date(reserva.dataFinal).toJSON().split('T')[0]+
    "&status="+reserva.status+
    "&carro="+reserva.modeloCarro+
    "&valorTotal="+reserva.valorTotal+
    "&valorAntecipado="+parseFloat(reserva.valorAntecipado)+
    "&idReserva="+reserva.idReserva
    ,true);
    ajax.addEventListener("load", function(){
        alert(ajax.responseText);
        location.reload();
    }, false);
    ajax.send();
};
