$(function () {

    var idReserva = document.getElementById("idReserva");
    let idUsuario = document.getElementById("idUsuario");
    let dataInicial = document.getElementById("dataInicial");
    let idCategoria = document.getElementById("idCategoria");
    let dataFinal = document.getElementById("dataFinal");
    let modeloCarro = document.getElementById("carro");
    let valorAntecipado = document.getElementById("valorAntecipado");
    let valorTotal = document.getElementById("valorTotal");
    let divResposta = document.getElementById("respostaReserva");
    let status = document.getElementById("status");
   $( "#formularioReservaDialogo" ).dialog({
        autoOpen: false,
        height: 400,
        width: 400,
        modal: true,
        buttons: { 
            "Atualizar Reserva": atualizaReserva 
        }
   });
   
    function atualizaReserva(){
        let reserva = {
            'idReserva': idReserva.value,
            'idUsuario': idUsuario.value, 
            'idCategoria': idCategoria.value,
            'modeloCarro': modeloCarro.value,
            'dataInicial': dataInicial.value,
            'dataFinal': dataFinal.value,
            'valorAntecipado' : valorAntecipado.value,
            'valorTotal' : valorTotal.value,
            'status': status.value
       };
       requisicaoAjax(reserva);
       $( "#formularioReservaDialogo" ).dialog("close");
    }
   
   function preencheFormulario(){
       
        if(idReserva.value == ""){
            divResposta.innerHTML = "<div><p>O campo ID Reserva é obrigatório</p></div>";
        }else{
            divResposta.innerHTML = "";
            $.get("/reserva",
            {"reserva":idReserva.value},
            function(retorno){

                if(retorno == null){
                    divResposta.innerHTML = "<div><p>Reserva não encontrada</p></div>";
                }else{
                    
                    encontraCategoriaPorId(retorno['idCategoria']);
                    encontrarUsuarioPorId(retorno['idUsuario']);
                    encontrarCarroPorId(retorno['idCarro']);
                    
                    modeloCarro.value = carro;
                    idUsuario.value = usuario;
                    idCategoria.value = categoria;
                    valorAntecipado.value = retorno['valorAntecipado'];
                    valorTotal.value = retorno['valorTotal'];
                    status.value = retorno['status'];
                    dataFinal.value = new Date(retorno['diaFinal']).toJSON().split('T')[0];
                    dataInicial.value = new Date(retorno['diaInicio']).toJSON().split('T')[0];

                    $("#formularioReservaDialogo").dialog('open');
            }
        });
        }
    }

    $("#botao").click(function() {
        preencheFormulario();
    });

});

var categoria;
function encontraCategoriaPorId(id){
    $.get("/categoria/lista/id",
        {"id":id},
        function(data){
            categoria = data['nome'];
        });
}
var usuario;
function encontrarUsuarioPorId(id){
        $.get("/usuario/lista/id",
        {"id":id},
        function(data){
            usuario = data['apelido'];
        });
}
var carro;
function encontrarCarroPorId(id){
    $.get("/carro/lista/id",
        {"id":id},
        function(data){
            carro = data['modelo'];
        });
}