/** 
 * Função para adicionar a classe oculta menu para o menu ser responsivo
 * 
*/
function menuOpcao() {
    var barra = document.getElementsByClassName("barra-de-navegacao")[0];
    if (barra.className === "barra-de-navegacao") {
        barra.className += " oculta-menu";
    } else {
        barra.className = "barra-de-navegacao";
    }
}