/**
 * Função para definir a data máxima para os campo 'date' dos formulários
 * 
 * @returns {void}
 */
function dataMinimaDosFormularios() {
    let formulariosData = document.getElementsByTagName("input");

    for (let i in formulariosData) {
        if (formulariosData[i].type === 'date') {
            // Recupera a data atual e coloca como definição mínima
            formulariosData[i].min = new Date().toJSON().split('T')[0];
        }
    }        
} 

/**
 * Função para calcular a distância entre duas datas
 * 
 * @param {Date} dataUm 
 * @param {Date} dataDois 
 */
function distanciaEntreAsDatas(dataUm, dataDois) {
    const DIA_EM_MILISSEGUNDOS = 1000 * 60 * 60 * 24;

    let dataUmMLS = dataUm.getTime();
    let dataDoisMLS = dataDois.getTime();
    let distancia = Math.round((dataDoisMLS - dataUmMLS) / DIA_EM_MILISSEGUNDOS);

    // Caso a distância seja 0 (Mesmo dia) cobra um dia todo!
    if (distancia > 0) {
        return distancia;
    } else {
        return 1;
    }
}

/**
 * Transforma string (yyyy-MM-dd) em Data
 * 
 * @returns {Date}
 */
function stringEmData(dataEmString) {
    let informacoes = dataEmString.split('-');
    
    return new Date(informacoes[0], informacoes[1] - 1, informacoes[2]);
}
