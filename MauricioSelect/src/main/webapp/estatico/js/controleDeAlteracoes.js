/**
 * 
 * Função para realizar as mudanças realizadas no perfil do usuário, 
 * na página de perfil
 * 
 * @returns {void}
 */
function salvaAlteracoesPerfil() {   
    let apelidoUsuario = document.getElementById('apelidoUsuario').innerText;
    let emailUsuario = document.getElementById("emailUsuario").value;
    let rgUsuario = document.getElementById("rgUsuario").value;
    let cpfUsuario = document.getElementById("cpfUsuario").value;
    let nomeUsuario = document.getElementById("nomeUsuario").value;
    
    let senhaUm = document.getElementById('senhaUsuarioUm').value;
    let senhaDois = document.getElementById('senhaUsuarioDois').value;
    
    // Verifica se todos os campos foram preenchidos
    if (apelidoUsuario.trim() === '' || emailUsuario.trim() === ''
            || rgUsuario.trim() === '' || cpfUsuario.trim() === '' 
            || nomeUsuario.trim() === '' || !emailUsuario.includes('@')) {
        return false;
    }

    // Valida o novo cpf inserido
    if (!CPF.validate(cpfUsuario)) {
        return false;
    }
    
    if (senhaUm === senhaDois) {
       let objetoPadrao = {'email': emailUsuario, 'rg': rgUsuario, 
                'cpf': cpfUsuario, 'nome': nomeUsuario, 'apelido': apelidoUsuario};
       
       // Caso o usuário coloque uma senha, ela será trocada
       if (senhaUm !== "") {
           objetoPadrao.senha = senhaUm;
       } 
       
       return objetoPadrao;
    }
    return false;
}