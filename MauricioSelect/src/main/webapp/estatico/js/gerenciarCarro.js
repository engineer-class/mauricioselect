/**
 * Script que utiliza JQuery para criar um modal na tela do usuario, este por sua
 * vez, e utilizado para a ediçao de reservas.
 * Ha funçoes fora do clojure que facilitam o processo de tratamento
 * @returns {undefined}
 */

$(function () {

    let dialogo;
    let idCarro = document.getElementsByName("idCarro")[0];
    let idCategoria = document.getElementsByName("categoria")[0];
    let idFabricante = document.getElementsByName("fabricante")[0];
    let ano = document.getElementsByName("anoDoCarro")[0];
    let conservacao = document.getElementsByName("conservacao")[0];
    let cor = document.getElementsByName("corDoCarro")[0];
    let placa = document.getElementsByName("placaDoCarro")[0];
    let quilometragem = document.getElementsByName("quilometragem")[0];
    let modelo = document.getElementsByName("modeloDoCarro")[0];
    let tanque = document.getElementsByName("tanqueDoCarro")[0];
    
    document.getElementById('editarCarro').addEventListener('click', () => {
        preencheFormulario();
        dialogo.dialog('open');
    });
    document.getElementById('visualizarDisponiveis').addEventListener('click', () => {
        location.pathname = "/carro/listar/disponiveis";
    });

    document.getElementById('visualizarIndisponiveis').addEventListener('click', () => {
        location.pathname = "/carro/listar/indisponiveis";
    });

    document.getElementById('visualizarTodos').addEventListener('click', () => {
        location.pathname = "/carro/listar";
    });

    /**
   * 
   * Insere as informaçoes do campo selecionado pelo usuario no formulario para 
   * ediçao.
   * 
   * @returns {void}
   */
    function preencheFormulario() {

        let linhaSelecionada = buscaSelecionado();
        let objLinha = recuperaLinhaMatriz(linhaSelecionada);

        // Insere em cada elemento do formulário os dados da linha selecionada
        // na matriz de reservas
        idCarro.innerText = objLinha.idCarro;
        idCategoria.value = objLinha.idCategoria;
        idFabricante.value = objLinha.idFabricante;
        ano.value = objLinha.ano;
        conservacao.value = objLinha.conservacao;
        cor.value = objLinha.cor;
        placa.value = objLinha.placa;
        quilometragem.value = objLinha.quilometragem;
        modelo.value = objLinha.modelo;
        tanque.value = objLinha.tanque;

    }

    document.getElementById('excluirCarro').addEventListener('click', () => {
        excluiCarro();
    });

    function atualizarCarro() {
        let form = criaCarro();
        if (validaCarro(form)) {
            let ajax = $.post("/carro/atualizar", form);
            ajax.done(function (resposta) {
                alert(resposta['mensagem']);
                location.reload()
            });
        } else {
            alert("Preencha os campos corretamente para atualizar o carro!");
        }
    }

    function excluiCarro() {
        let opcao = confirm("Voce realmente deseja excluir este carro?");
        if (opcao == true) {

            let linhaSelecionada = buscaSelecionado();
            let idCarro = recuperaLinhaMatriz(linhaSelecionada).idCarro;

            var deleta = $.ajax({
                url: "/carro/deletar?idCarro=" + idCarro,
                method: "DELETE"
            });

            deleta.done(
                function (resposta) {
                    alert(resposta['mensagem']);
                    location.reload();
                }
            );
        }

    }

    function fecharModal() {
        dialogo.dialog('close');
    }

    dialogo = $("#formularioEdicao").dialog({
        autoOpen: false,
        height: 540,
        width: 400,
        modal: true,
        buttons: {
            "Atualizar carro ": atualizarCarro,
            "Cancelar": fecharModal
        }
    });
});

/**
 * Funçao para realizar a busca do elemento selecionado
 * 
 * @returns {int}
 */
function buscaSelecionado() {
    let elementos = document.getElementsByTagName("input");

    for (var i = 0; i < elementos.length; i++) {
        if (elementos[i].checked) {
            return i + 1;
        }
    }
}

/**
 * Funçao para buscar a linha da matriz que esta sendo editada
 * 
 * @param {type} linhaSelecionada Linha selecionada pelo usuario para ediçao
 * @returns {recuperaLinhaMatriz.formularioAtualizaReservaAnonym$0}
 */
function recuperaLinhaMatriz(linhaSelecionada) {
    let matriz = document.getElementById("tabelaCarro");
    let linha = matriz.rows[linhaSelecionada].cells;

    return {
        'idCarro': linha[0].innerText,
        'idCategoria': linha[1].innerText,
        'idFabricante': linha[2].innerText,
        'status': linha[3].innerText,
        'ano': linha[4].innerText,
        'conservacao': linha[5].innerText,
        'cor': linha[6].innerText,
        'placa': linha[7].innerText,
        'quilometragem': linha[8].innerText,
        'modelo': linha[9].innerText,
        'tanque': linha[10].innerText.split("%")[0].trim()
    };
}

/***
 * Função para fazer a criação do objeto carro
 * 
 * @return JSONObject carro
 * 
 */
function criaCarro() {

    let idCarro = document.getElementsByName("idCarro")[0];
    let idCategoria = document.getElementsByName("categoria")[0];
    let idFabricante = document.getElementsByName("fabricante")[0];
    let ano = document.getElementsByName("anoDoCarro")[0];
    let conservacao = document.getElementsByName("conservacao")[0];
    let cor = document.getElementsByName("corDoCarro")[0];
    let placa = document.getElementsByName("placaDoCarro")[0];
    let quilometragem = document.getElementsByName("quilometragem")[0];
    let modelo = document.getElementsByName("modeloDoCarro")[0];
    let tanque = document.getElementsByName("tanqueDoCarro")[0];
    let carroQuebrado = document.getElementsByName("carroQuebrado")[0];

    return {
        "idCarro": idCarro.innerText,
        "categoria": idCategoria.value,
        "fabricante": idFabricante.value,
        "anoDoCarro": ano.value,
        "conservacao": conservacao.value,
        "corDoCarro": cor.value,
        "placaDoCarro": placa.value,
        "quilometragem": quilometragem.value,
        "modeloDoCarro": modelo.value,
        "tanqueDoCarro": tanque.value,
        "carroQuebrado": carroQuebrado.value
    };
}