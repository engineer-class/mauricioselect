/**
 * Script que utiliza JQuery para criar um modal na tela do usuario, este por sua
 * vez, e utilizado para a ediçao de reservas.
 * Ha funçoes fora do clojure que facilitam o processo de tratamento
 * @returns {undefined}
 */

$(function () {
    let dialog;
    let idReserva = document.getElementById("idReserva");
    let apelido = document.getElementById("apelido");
    let idCategoria = document.getElementsByName("categoria")[0];
    let dataInicial = document.getElementById("dataInicial");
    let dataFinal = document.getElementById("dataFinal");
    let status = document.getElementById("status");
    let modeloCarro = document.getElementsByName("modeloCarro")[0];
    let valorTotal = document.getElementById("valorTotal");
    let objReserva;
    // Evento JS

    // Busca sempre que uma categoria é alterada
    idCategoria.addEventListener("change", () =>{
        buscarCarroPorCategoria(idCategoria.value,modeloCarro);
        let distanciaEntreDias = distanciaEntreAsDatas(new Date(dataInicial.innerText), dataFinal.valueAsDate);
        //
        $.get("/categoria/lista/nome",
        
        { nome:idCategoria.value} ,
        function(data){
            valorTotal.innerText = (distanciaEntreDias * 24) * data['valorPorHora'];
        });
    });

    document.getElementById('atualizarReserva').addEventListener('click', () => {
        preencheFormulario();
        dialog.dialog("open");
        
    });

    dataFinal.addEventListener("change", function () {
        let distanciaEntreDias = distanciaEntreAsDatas(new Date(dataInicial.innerText), dataFinal.valueAsDate);
        valorTotal.innerText = (distanciaEntreDias * 24) * objReserva.valorDaCategoria;

    });


    function excluiReserva() {

        let ajax = new XMLHttpRequest();
        ajax.open('delete', '/reserva/excluir?id=' + idReserva.innerText, true);
        ajax.send();
        ajax.onload = () => {
            alert(JSON.parse(ajax.responseText).mensagem);
            location.reload();
        };
        dialog.dialog("close");

    }

    function aprovarReserva() {
        let reserva = recuperaModal();
        var form = document.createElement("form");
        form.setAttribute("action", "/reserva/aprova/id?" + $.param(reserva) + status.innerText);
        form.setAttribute("method", "post");
        document.body.appendChild(form);
        form.submit();
    }

    function atualizaReserva() {
        let reserva = recuperaModal();
        if (reserva.dataFinal < reserva.dataInicial) {
            alert("Data inválida");
            dataFinal.value = objReserva.dataFinal;
            dataInicial.value = objReserva.dataInicial;
        } else {
            requisicaoAjax(reserva, "/reserva/atualiza", "Reserva atualizada com sucesso");
            dialog.dialog("close");
        }
    }

    /**
     * 
     * Insere as informaçoes do campo selecionado pelo usuario no formulario para 
     * ediçao.
     * 
     * @returns {void}
     */
    function preencheFormulario() {
        let linhaSelecionada = buscaSelecionado();
        let valorPadrao = document.getElementById("valorPadraoDoModelo");
        objReserva = recuperaLinhaMatriz(linhaSelecionada);

        // Insere em cada elemento do formulário os dados da linha selecionada
        // na matriz de reservas
        idReserva.innerText = objReserva.idReserva;
        apelido.innerText = objReserva.apelido;
        idCategoria.value = objReserva.idCategoria;
        valorPadrao.innerText = objReserva.modeloCarro;
        modeloCarro.value = valorPadrao.value;
        dataInicial.innerText = objReserva.dataInicial;
        dataFinal.value = new Date(objReserva.dataFinal).toJSON().split('T')[0];
        status.innerText = objReserva.status;
        valorTotal.innerText = objReserva.valorTotal;
    }

    function recuperaModal() {
        return {
            'idReserva': idReserva.innerText,
            'apelido': apelido.innerText,
            'idCategoria': idCategoria.value,
            'modeloCarro': modeloCarro.value,
            'dataInicial': dataInicial.innerText,
            'dataFinal': dataFinal.value,
            'valorTotal': valorTotal.innerText,
            'status': status.innerText
        };
    }

    dialog = $("#formularioReservaDialogo").dialog({
        autoOpen: false,
        height: 400,
        width: 400,
        modal: true,
        buttons: {
            "Aprovar reserva": aprovarReserva,
            "Atualizar reserva ": atualizaReserva,
            "Excluir reserva": excluiReserva
        }
    });
});

/**
 * Função para o modal de criação de reserva
 */

$(function () {

    let criaReservaModal;
    let apelido = document.getElementsByName("apelidoC")[0];
    let idCategoria = document.getElementsByName("categoriaC")[0];
    let dataInicial = document.getElementsByName("dataInicialC")[0];
    let dataFinal = document.getElementsByName("dataFinalC")[0];
    let modeloCarro = document.getElementsByName("modeloCarroC")[0];
    //let valorTotal = document.getElementsByName("valorTotalC")[0];

    // Busca sempre que uma categoria é alterada
    idCategoria.addEventListener("change", () =>{
        buscarCarroPorCategoria(idCategoria.value,modeloCarro);
    });

    document.getElementById("criaReserva").addEventListener("click",() =>{
        criaReservaModal.dialog("open");  
    })

    function recuperaModal() {
        return {
            'apelido': apelido.value,
            'idCategoria': idCategoria.value,
            'modeloCarro': modeloCarro.value,
            'dataInicial': dataInicial.value,
            'dataFinal': dataFinal.value
        };
    }

    function criarReserva(){
        $.post( "/reserva/cadastrar", recuperaModal() )
        .done(function (mensagem){
            alert(mensagem['mensagem']);
            location.reload();
        });
    }

    criaReservaModal = $("#criarReservaDialogo").dialog({
        autoOpen: false,
        height: 300,
        width: 400,
        modal: true,
        buttons: {
            "Criar Reserva": criarReserva
        }
    });
});

/**
 * Funçao para realizar a busca do elemento selecionado
 * 
 * @returns {int}
 */
function buscaSelecionado() {
    let elements = document.getElementsByTagName("input");

    for (var i = 0; i < elements.length; i++) {
        if (elements[i].checked) {
            return i + 1;
        }
    }
}

/**
 * Funçao para buscar a linha da matriz que esta sendo editada
 * 
 * @param {type} linhaSelecionada Linha selecionada pelo usuario para ediçao
 * @returns {recuperaLinhaMatriz.formularioAtualizaReservaAnonym$0}
 */
function recuperaLinhaMatriz(linhaSelecionada) {
    let matriz = document.getElementById("tabelaReservas");
    let linha = matriz.rows[linhaSelecionada].cells;
    return {
        'idReserva': linha[0].innerText,
        'apelido': linha[1].innerText,
        'idCategoria': linha[2].innerText,
        'valorDaCategoria': linha[3].innerText,
        'modeloCarro': linha[4].innerText,
        'dataInicial': linha[5].innerText,
        'dataFinal': linha[6].innerText,
        'valorTotal': linha[7].innerText.trim(),
        'status': linha[8].innerText
    };
}

(function eventosDosBotoes() {

    document.getElementById("reservasPendentes").addEventListener("click", () => {
        location.pathname = "/reserva/listar";
    });
    document.getElementById("reservasAprovadas").addEventListener("click", () => {
        location.pathname = "/reserva/listar/todas";
    });

})();

window.onload = function () {
    if (location.pathname == "/reserva/listar/todas") {
        document.getElementById('atualizarReserva').style.display = "none";
        let radio = document.getElementsByName("selecaoEdicao");
        for (let i = 0; i < radio.length; i++) {
            radio[i].style.display = "none";
        }
    }
};