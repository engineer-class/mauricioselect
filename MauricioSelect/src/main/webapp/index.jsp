<%-- Este é o arquivo que é carregado primeiro no site --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <%@include file="/jsp/templates/meta.jsp" %>
    <link rel="stylesheet" type="text/css" href="estatico/css/geral.css">
    <link rel="stylesheet" type="text/css" href="estatico/css/titulos.css">
    <link rel="stylesheet" type="text/css" href="estatico/css/fonte.css">
    <link rel="stylesheet" type="text/css" href="estatico/css/botao.css">
    <title>Início - Mauricio Select</title>
    
    <style>
        h1::after {
            content: "|";
        }
        
        section {
            max-width: 600px;
            margin: 80px auto;
            padding: 0 20px;
            text-align: justify;
        }
        
        p {
            font-size: 25px;
            font-family: sofia-light;
        }
        
        ul {
            font-size: 20px;
            font-family: sofia-light;
        }
        
        a#conecte-agora {
            background-color: #8C62FF;
            color: white;
            padding: 1em 1.5em;
            text-decoration: none;
            text-transform: uppercase;
        }
    </style>
</head>

<body>
    <ul class="barra-de-navegacao">
        <li><a href="/">Início</a></li>
        <li><a href="/login">Entrar</a></li>
        <li><a href="../html/sobre.html">Sobre</a></li>
        <a href="javascript:void(0);" class="icon" onclick="menuOpcao()">
            <i class="fa fa-bars"></i>
        </a>
    </ul>
</body>

    <h1 class="titulo-pagina">Os melhores carros de luxo do país. Aluguel pelos maiores preços 🏎️</h1>
    
    <section id="velocidade">
        <h2>Carros rápidos e caros!</h2>
        <p>
            A MauricioSelect é uma empresa com os melhores carros de luxo para você e seu negócio! Venha aproveitar os melhores e <b>maiores</b> preços que você pode imaginar !</p>
            <p>Venha alugar seu carro hoje mesmo! Seja para uma rápida viagem ou para diversão com os amigos, pode acreditar, se precisar a MauricioSelect vai estar! 🚀</p>
            <p>E lembre-se nosso serviços tiveram inspiração vinda direta do Japão, onde todos são ninjas
        </p>        
    </section>
    <section id="felicidade">
        <h2>Velocidade e felicidade!</h2>
        <p>Trabalhamos somente com as melhores marcas! Confira</p>
        <ul>
            <li>Ferrari</li>
            <li>BMW</li>
            <li>Ford</li>
        </ul>
        <p>Fazemos tudo sempre pensando em nossos clientes 💜 milionários.
            Nossa diversão é fazer vocẽ feliz 🎆. Começamos como uma simples empresa para a matéria de engenharia de software, e depois disto fomos apenas crescendo 💃</p>
        <p>Os testes dos carros são feitos pelos maiores pilotos da história de <b><i>Speed Racer</i></b></p>      
        
        <div style="text-align: center">
            <a id="conecte-agora" target="_blank" href="/cadastro">Conecte-se agora mesmo!</a>    
        </div>
    </section>
   
    
    
    <script> 
        // Função para criar uma animação legal!
        let elemento = document.querySelector('h1');
        (function (mensagem){
            elemento.innerHTML = "";
            // Separando todas as letras e iterando
            mensagem.split('').forEach(function (letra, posicao) {
                setTimeout(function () {
                   elemento.innerHTML += letra;
                   
                }, 76 * posicao);
            });
        })(elemento.innerHTML);
    </script>
</body>
</html>