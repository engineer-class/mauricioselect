<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <%@include file="../templates/meta.jsp" %>
    <link rel="stylesheet" type="text/css" href="../../estatico/css/geral.css">
    <link rel="stylesheet" type="text/css" href="../../estatico/css/formulario.css">
    <link rel="stylesheet" type="text/css" href="../../estatico/css/titulos.css">
    <link rel="stylesheet" type="text/css" href="../../estatico/css/botao.css">
    <link rel="stylesheet" type="text/css" href="../../estatico/css/extras.css">
    <script src="../../estatico/js/geral/pace.min.js"></script>
    <title>Gerenciamento de categorias</title>
</head>

<body>
    <%@include file="../templates/operador/cabecalho.jsp" %>
    <div class="caixa-formulario">
        <h3>Cadastro de categorias 🔮</h3>
        <div class="grade-formulario">
            <label>Nome da categoria</label>
            <input type="text" id="cadastroNomeCategoria">
            <label>Valor da categoria (Por hora)</label>
            <input type="number" id="cadastroValorCategoria" min="0" />
        </div>
        <button id="btnCadastra" class="botao-generico">Cadastrar categoria</button>

        <h3>Atualizar ou deletar 🔨</h3>
        <div class="grade-formulario">
            <label>Categoria</label>
            <select id="selectCategorias">
            </select>
            <label>Novo nome da categoria</label>
            <input type="text" id="atualizaNomeCategoria">
            <label>Novo valor por hora (R$):</label>
            <input type="number" id="atualizaHoraCategoria" min="0">
        </div>
        <button id="btnAtualizar" class="botao-generico">Atualizar categoria selecionada</button>
        <button id="btnDeleta" class="botao-excluir">Remover categoria selecionada</button>
    </div>
    <script src="../../estatico/js/geral/jquery-3.3.1.min.js"></script>
    <script src="../../estatico/js/gerencia-cat-fab/dados-categoria.js"></script>
    <script src="../../estatico/js/gerencia-cat-fab/eventosDeCategorias.js"></script>
</body>

</html>