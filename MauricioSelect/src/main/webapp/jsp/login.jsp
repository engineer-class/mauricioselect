<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <%@include file="/jsp/templates/meta.jsp" %>
    <link rel="stylesheet" type="text/css" href="../estatico/css/acesso.css">
    <link rel="stylesheet" type="text/css" href="../estatico/css/geral.css">
    <link rel="stylesheet" type="text/css" href="../estatico/css/formulario.css">
    <script src="../../estatico/js/geral/pace.min.js"></script>
    <title>Login - MauricioSelect</title>
</head>

<body>
    <ul class="barra-de-navegacao">
        <li><a href="/">Início</a></li>
        <li><a href="/login">Entrar</a></li>
        <li><a href="../html/sobre.html">Sobre</a></li>
        <a href="javascript:void(0);" class="icon" onclick="menuOpcao()">
            <i class="fa fa-bars"></i>
        </a>
    </ul>
    <div class="caixa-formulario">
        <form method="post" action="ValidaUsuario">
            <input id="inputUsuario" name="usuario" type="text" placeholder="Apelido" required>
            <input id="inputSenha" name="senha" type="password" placeholder="Senha" required>
            <button class="botao-entrar">Entrar</button>
            <p class="aviso-cadastro">Não está registrado ? <a href="/cadastro">Registre-se</a></p>
            <p class="aviso-cadastro">Esqueceu a senha ? <a href="/recuperarSenha">Recuperar agora</a></p>
        </form>
        <%
            if("sim".equals(request.getAttribute("usuarioInvalido"))){
                out.print("<p>Usuario ou senha inválidos.</p>");
            }
        %>
    </div>
</body>

</html>