<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="../estatico/js/CPF.js"></script>
    <script src="../estatico/js/validadores.js"></script>
    <link rel="stylesheet" type="text/css" href="../estatico/css/geral.css">
    <link rel="stylesheet" type="text/css" href="../estatico/css/acesso.css">
    <link rel="stylesheet" type="text/css" href="../estatico/css/formulario.css">
    <link rel="stylesheet" type="text/css" href="../estatico/css/titulos.css">
    <link rel="stylesheet" type="text/css" href="../estatico/css/extras.css">
    <script src="../../estatico/js/geral/pace.min.js"></script>
    <title>Cadastro - MauricioSelect</title>
</head>
<body>
    <ul class="barra-de-navegacao">
        <li><a href="/">Início</a></li>
        <li><a href="/login">Entrar</a></li>
        <li><a href="../html/sobre.html">Sobre</a></li>
    </ul>
    <h3 class="titulo-pagina">Cadastro no MauricioSelect</h3>
    <div class="caixa-formulario">
        <form id="formularioCadastro" method="post" action="cadastro"
              onsubmit="return validaCadastroUsuario('formularioCadastro')">
            <label>Nome completo</label>
            <input id="nome" name="nome" type="text" maxlength="150" placeholder="Mauricio Yassunaga" required>
            <label>Apelido</label>
            <input id="apelido" name="apelido" type="text" maxlength="50" placeholder="Mau Mau" required>
            <label>E-mail</label>
            <input id="email" name="email" type="email" maxlength="50" placeholder="mauricio.yassunaga@fatec.sp.gov.br" required>
            <label>CPF</label>
            <input id="cpf" name="cpf" type="text" maxlength="14" placeholder="123456789" required>
            <label>RG</label>
            <input id="rg" name="rg" type="text" maxlength="10" placeholder="123456789" required>
            <label>Sexo</label>
            <select name="sexo" id="select-sexo">
                <option value="M">Masculino</option>
                <option value="F">Feminino</option>
            </select>
            <label>Senha</label>
            <input id="senha" name="senha" type="password" maxlength="50" placeholder="Senha" required>
            <button class="botao-entrar" type="submit" value="Cadastrar">cadastrar</button>
            <p class="aviso-cadastro">Já é cadastrado ? <a href="/login">Fazer login</a></p>
            
            <%
                if(request.getAttribute("formularioPreenchido") != null) {
                    if ("sim".equals(request.getAttribute("dadosInvalidos"))) {
                        out.print("<p>" + request.getAttribute("mensagemDeErro")  + "</p>");
                    } else {
                        out.print("<p>Usuário cadastrado com sucesso</p>");
                        out.print("<p><a href=\"./login\">Voltar para a tela de login</a></p>");
                    }
                }
            %>
        </form>
    </div>
    <script>
        // Script apenas para adicionar um efeito legal na página =D
        (function() {
            const apelidosDoMauricio = ['Mau Mau', 'Olha o Maurício'];
            const apelidoEscolhido = apelidosDoMauricio[Math.floor(Math.random() * (1 - 0 + 1)) + 0];
            document.getElementById('apelido').placeholder = apelidoEscolhido; 
        })();
    </script>
</body>
</html>