<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../../estatico/css/geral.css">
    <link rel="stylesheet" type="text/css" href="../../estatico/css/formulario.css">
    <link rel="stylesheet" type="text/css" href="../../estatico/css/titulos.css">
    <link rel="stylesheet" type="text/css" href="../../estatico/css/botao.css">
    <link rel="stylesheet" type="text/css" href="../../estatico/css/extras.css">
    <script src="../../estatico/js/geral/pace.min.js"></script>
    <title>Gerenciamento de fabricantes</title>
</head>
<body>
    <%@include file="../templates/operador/cabecalho.jsp" %>

    <div class="caixa-formulario">
        <h3>Cadastro de fabricantes 👷</h3>
        <div class="grade-formulario">
            <label>Nome do fabricante</label>
            <input type="text" id="cadastroNomeFabricante">
        </div>
        <button id="btnCadastra" class="botao-generico">Cadastrar fabricante</button>
   
        <h3>Atualizar ou deletar 🔨</h3>
        <div class="grade-formulario">
            <label>Fabricante</label>
            <select  id="selectFabricantes">
            </select>
            <label>Novo nome do fabricante</label>
            <input type="text" id="atualizaNomeFabricante">
        </div>  
        <button id="btnAtualizar" class="botao-generico">Atualizar fabricante selecionado</button>
        <button id="btnDeleta" class="botao-excluir">Remover fabricante selecionado</button>
    </div>
    <script src="../../estatico/js/geral/jquery-3.3.1.min.js"></script>
    <script src="../../estatico/js/gerencia-cat-fab/dados-fabricante.js"></script>
    <script src="../../estatico/js/gerencia-cat-fab/eventosDeFabricante.js"></script>
</body>
</html>