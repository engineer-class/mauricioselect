<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Esqueci a senha - MauricioSelect</title>
    <link rel="stylesheet" type="text/css" href="../estatico/css/geral.css">
    <link rel="stylesheet" type="text/css" href="../estatico/css/titulos.css">
    <script src="../../estatico/js/geral/pace.min.js"></script>
    <style>
        #div-email {
            position: relative;
            width: 100px;
            height: 100px;
            margin: auto;
            top: 150px;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        #div-botao-recuperacao {
            display: flex;
            align-items: center;
            justify-content: center;
            margin-left: 10px;
        }

        #entrada-email {
            height: 30px;
        }
    </style>
</head>
<body>
    <ul class="barra-de-navegacao">
        <li><a href="/">Início</a></li>
        <li><a href="/login">Entrar</a></li>
        <li><a href="sobre.html">Sobre</a></li>
    </ul>
    <h3 class="titulo-pagina">Recuperação de senha</h3>
    <h3 class="titulo-pagina">💁🏽</h3>
    <div id="div-email">
        <form  method="POST" action="/recuperarSenha">
            <label>Insira seu e-mail: </label>
            <input id="entrada-email" name="emailUsuario" type="email" placeholder="cascão@turmadamonica.com.br">
            <div id="div-botao-recuperacao">
                <button>➡️</button>
            </div>
        </form>
        <%
            if ("sim".equals(request.getAttribute("formularioEnviado"))) {
                if("sim".equals(request.getAttribute("dadosInvalidos"))){
                    out.print("<p>" + request.getAttribute("mensagemDeErro") +"</p>");
                } else {
                    out.print("<p>O pedido foi enviado com sucesso! E chegará "
                            + "no seu email em instantes</p>");
                }
            }
        %>
    </div>
</body>
</html>
