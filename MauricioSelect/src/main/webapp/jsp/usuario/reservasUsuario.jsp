<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <%@include file="../templates/meta.jsp" %>
    <link rel="stylesheet" type="text/css" href="../../estatico/css/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="../../estatico/css/geral.css">
    <link rel="stylesheet" type="text/css" href="../../estatico/css/usuario.css">
    <link rel="stylesheet" type="text/css" href="../../estatico/css/tabela.css">
    <link rel="stylesheet" type="text/css" href="../../estatico/css/titulos.css">
    <link rel="stylesheet" type="text/css" href="../../estatico/css/botao.css">
    <link rel="stylesheet" type="text/css" href="../../estatico/css/formulario.css">
    <script src="../../estatico/js/geral/pace.min.js"></script>
    <title>Reservas - MauricioSelect</title>
</head>

<body>
    <%@include file="../templates/usuario/cabecalho.jsp" %>
    <div class="div-titulo-pagina">
        <h3>Suas reservas 🚩</h3>
        <button class="botao-generico" id="botao">Editar reserva selecionada</button>
    </div>

    <div class="div-tabela">
        <table id="tabelaReservas">
            <tr>
                <th>ID da reserva</th>
                <th>Categoria do carro</th>
                <th>Valor (Categoria)</th>
                <th>Modelo do carro</th>
                <th>Data de início</th>
                <th>Data de fim</th>
                <th>Valor total</th>
                <th>Status</th>
            </tr>

            <c:forEach items="${reservas}" var="reserva">
                <tr>
                    <td>${reserva.getIdReserva()}</td>
                    <td>${reserva.getCategoria().getIdCategoria()}</td>
                    <td>${reserva.getCategoria().getValorPorHora()}</td>
                    <td>${reserva.getCarro().getModelo()}</td>
                    <td>${reserva.getDiaInicio()}</td>
                    <td>${reserva.getDiaFinal()}</td>
                    <td>${reserva.getValorTotal()}</td>
                    <td>${reserva.getStatus()}</td>
                    <td><input id="radioSelecao" name="selecaoEdicao" value="${reserva.getIdReserva()}" type="radio"></td>
                </tr>
            </c:forEach>
        </table>
    </div>

    <div id="formularioDoDialogo">
        <fieldset class="formulario-modal">
            <label>Identificador da reserva
                <label id="idReserva"></label>
            </label>

            <label id="tituloCampoFormulario">Data de início</label>
            <input type="date" name="dataInicial">

            <label id="tituloCampoFormulario">Data final</label>
            <input type="date" name="dataFinal">

            <label id="tituloCampoFormulario">Categoria do carro</label>
            <select id="tituloCampoFormulario" name="categoria">
                <option>Selecione</option>
            </select>

            <label>Valor hora (R&dollar;):
                <label id="lblValorHora">

                </label>
            </label>

            <label id="tituloCampoFormulario">Modelo do carro</label>
            <select id="modeloDeCarro" name="modelo">
            </select>

            <label id="tituloCampoFormulario">Valor total (R&dollar;)</label>
            <label type="number" id="valorTotal"></label>

            <button id="btnRecalcula">Recalcular o valor da reserva</button>
            <button id="btnExcluir" onclick="verificacao();">Excluir reserva</button>
        </fieldset>
    </div>
    <script src="../../estatico/js/geral/jquery-3.3.1.min.js"></script>
    <script src="../../estatico/js/geral/jquery-ui.js"></script>
    <script src="../../estatico/js/manipuladorDeData.js"></script>
    <script src="../../estatico/js/reserva-usuario/manipulacao.js"></script>
    <script src="../../estatico/js/reserva-usuario/calculos.js"></script>
    <script src="../../estatico/js/reserva-usuario/dados.js"></script>
    <script src="../../estatico/js/reserva-usuario/formularioAtualizaReserva.js"></script>
    <script>
        // Função para inserção de datas mínimas nos formulários
        dataMinimaDosFormularios();
    </script>
</body>

</html>