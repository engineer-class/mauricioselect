<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <%@include file="../templates/meta.jsp" %>
    <link rel="stylesheet" href="../../estatico/css/jquery-ui.css">
    <link rel="stylesheet" href="../../estatico/css/geral.css">
    <link rel="stylesheet" href="../../estatico/css/operador.css">
    <link rel="stylesheet" href="../../estatico/css/formulario.css">
    <link rel="stylesheet" href="../../estatico/css/titulos.css">
    <link rel="stylesheet" href="../../estatico/css/extras.css">
    <link rel="stylesheet" href="../../estatico/css/botao.css">
    <script src="../../estatico/js/geral/jquery-3.3.1.min.js"></script>
    <script src="../../estatico/js/geral/jquery-ui.js"></script>
    <script src="../../estatico/js/geral/pace.min.js"></script>
    <title>Nova Reserva - MauricioSelect</title>
</head>

<body>
    <%@include file="../templates/usuario/cabecalho.jsp" %>
    <h3 class="titulo-pagina">Nova reserva 🗽</h3>

    <div class="caixa-formulario">
        <div class="grade-formulario" id="formularioCriaReserva">
            <label>Data inicial</label>
            <input type="date" name="dataInicial">
            <label>Data final</label>
            <input type="date" name="dataFinal">
            <label>Categoria</label>
            <select name="categoria" form="formularioCriaReserva">
                <option>Selecione</option>
            </select>
            <label>Carros disponíveis</label>
            <select name="modelo" form="formularioCriaReserva">
                <option>Selecione</option>
            </select>
            <label>Valor total (R&dollar;): <label id="lblValorHora">0.00</label></label>
            <label id="valorTotal">0.00</label>
            <button name="confirmar" value="Confirmar Reserva" id="botao-confirma-reserva" class="botao-generico">Confirmar
                reserva</button>
        </div>
        <button id="calculaValores" style="background: #8C99FF; color: white">Calcular valor da reserva</button>
        <button id="verificaInfosCarro">Informações do carro</button>
    </div>
    <div id="formularioModalInformacoesCarro">
        <form>
            <fieldset class="formulario-modal">
                <label>ID do carro: </label>
                <label id="formIDCarro"></label>
                <label>Categoria: </label>
                <label id="formCategoria"></label>
                <label>Fabricante: </label>
                <label id="formFabricante"></label>
                <label>Modelo: </label>
                <label id="formModelo"></label>
                <label>Ano: </label>
                <label id="formAnoCarro"></label>
            </fieldset>
        </form>
    </div>
    <script src="../../estatico/js/manipuladorDeData.js"></script>
    <script src="../../estatico/js/reserva-usuario/manipulacao.js"></script>
    <script src="../../estatico/js/reserva-usuario/dados.js"></script>
    <script src="../../estatico/js/reserva-usuario/calculos.js"></script>
    <script src="../../estatico/js/reserva-usuario/eventosDeElementos.js"></script>
    <script>
        dataMinimaDosFormularios();
        recuperarCategorias();
    </script>
</body>

</html>