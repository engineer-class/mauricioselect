<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="../templates/meta.jsp" %>
        <title>Pagina inicial</title>
    </head>
    <%@include file="../templates/usuario/cabecalho.jsp" %>
        <a href="/reserva/usuario">Minhas reservas</a>
        <a href="/reserva/carro">Nova reserva</a>
    <%@include file="../templates/usuario/rodape.jsp" %>
</html>
