<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="../templates/meta.jsp" %>
        <link rel="stylesheet" type="text/css" href="../../estatico/css/botao.css">
        <link rel="stylesheet" type="text/css" href="../../estatico/css/tabela.css">
        <link rel="stylesheet" type="text/css" href="../../estatico/css/titulos.css">
        <link rel="stylesheet" type="text/css" href="../../estatico/css/extras.css">
        <link rel="stylesheet" href="../../estatico/css/jquery-ui.css">
        <link rel="stylesheet" href="../../estatico/css/formulario.css">
        <script src="../../estatico/js/geral/jquery-3.3.1.min.js"></script>
        <script src="../../estatico/js/geral/jquery-ui.js"></script>
        <script src="../../estatico/js/manipuladorDeData.js"></script>
        <title>MauricioSelect - Gerenciamento de alugueis</title>
    </head>
    <%@include file="../templates/operador/cabecalho.jsp" %>
    
    <div class="div-titulo-pagina">
        <h3>Gerenciamento de Alugueis</h3>
    </div>

    <div class="botaoOpcoes">
        <button class="botao-generico" id="atualizarAluguel">Sobre o aluguel selecionado</button>
        <button class="botao-generico" id="visualizarDisponiveis">Visualizar alugueis em andamento</button>
        <button class="botao-generico" id="visualizarIndisponiveis">Visualizar alugueis finalizados</button>
        <button class="botao-generico" id="visualizarTodos">Visualizar todos os alugueis</button>        
    </div>
    
    <c:choose>
        <c:when test="${alugueis.isEmpty()}">
            <div class="listaVazia">Não há alugueis</div>
        </c:when>
        <c:otherwise>
                <table id="tabelaAluguel">
            
            <tr>
                <th>ID Aluguel</th>
                <th>ID Reserva</th>
                <th>Usuario</th>
                <th>Operador</th>
                <th>Modelo do Carro</th>
                <th>Categoria</th>
                <th>Valor da categoria</th>
                <th>Data de início</th>
                <th>Data final</th>
                <th>Valor pago antecipado</th>
                <th>Valor total</th>
                <th>Status</th>
            </tr>
                        
            <c:forEach items="${alugueis}" var="aluguel">
                <tr>
                    <td>${aluguel.getIdAluguel()}</td>
                    <td>${aluguel.getIdReserva()}</td>
                    <td>${aluguel.getUsuario().getNome()}</td>
                    <td>${aluguel.getOperador().getNome()}</td>
                    <td>${aluguel.getCarro().getModelo()}</td>
                    <td>${aluguel.getCategoria().getNome()}</td>
                    <td>${aluguel.getCategoria().getValorPorHora()}</td>
                    <td>${aluguel.getDataRetirada()}</td>
                    <td>${aluguel.getDataDevolucao()}</td>
                    <td>${aluguel.getValorPagoAntecipado()}</td>
                    <td>${aluguel.getValorTotal()}</td>
                    <td>${aluguel.getStatus()}</td>
                    <td><input id="radioSelecao" name="selecaoEdicao" value="${aluguel.getIdAluguel()}" type="radio"></td>
                </tr>
            </c:forEach>
        </table>

        <!-- Tabela que sera exibida no momento da exclusão -->
        <div id="formularioReservaDialogo">
            <form>
                <fieldset class="grade-formulario">
                    <label>Visão geral para atualização</label>
                    <hr>
                    <label id="tituloCampoFormulario">Id da reserva</label>
                    <text id="idReserva"></text>
                    <label id="tituloCampoFormulario">Usuario</label>
                    <text type="text" id="apelido"></text>
                    <label id="tituloCampoFormulario">Categoria</label>
                    <text id="categoria" name="categoria"></text>
                    <label>Carro</label>
                    <text type="text" id="modeloCarro"></text>
                    <label id="tituloCampoFormulario">Dia inicial</label>
                    <text id="dataInicial"></text>
                    <label id="tituloCampoFormulario">Dia final</label>
                    <input type="date" id="dataFinal">
                    <label id="tituloCampoFormulario">Status</label>
                    <label id="status"></label>
                    <label id="tituloCampoFormulario">Valor antecipado</label>
                    <text id="valorAntecipado"></text>
                    <label id="tituloCampoFormulario">Valor total do aluguel</label>
                    <text id="valorTotal"></text>
                    <label id="tituloCampoFormulario">Total a pagar</label>
                    <text type="number" id="totalPagar"></text>
                                   
                    <label>Observações para a devolução</label>
                    <hr>
                    
                    <label>Carro quebrado</label>
                    <input id="obsCarroQuebrado" type="checkbox">                
                    <label>Nível de combustível no carro</label>
                    <input id="obsTanque" type="number">
                    <label>Quilometragem rodada com o carro</label>
                    <input id="obsQuilometragem" type="number">
                    <label>Estado de conservação</label>
                    <select id="obsConservacao">
                        <option value="BOM">BOM</option>
                        <option value="MEDIO">MÉDIO</option>
                        <option value="BAIXO">BAIXO</option>
                    </select>
                    
                    <label>Valores para devolução</label>
                    <hr>
                    <label>Valor total a pagar (R&dollar;): 
                        <label id="valorTotalEntrega">
                            0.00
                        </label>
                    </label>
                    <label>valor total da reserva (R&dollar;): 
                        <label id="valorTotalDaReserva">
                            0.00
                        </label>
                    </label>
                </fieldset>
            </form>
        </div>
        </c:otherwise>   
    </c:choose>
        <script src="../../estatico/js/operador/gerenciaAluguel.js"></script>
        <script src="../../estatico/js/operador/reservaAjax.js"></script>
        <script>
            dataMinimaDosFormularios();           
        </script>
    </body>
</html>
