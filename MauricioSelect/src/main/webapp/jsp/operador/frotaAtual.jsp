<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="../templates/meta.jsp" %>
        <link rel="stylesheet" type="text/css" href="../../estatico/css/botao.css">
        <link rel="stylesheet" type="text/css" href="../../estatico/css/tabela.css">
        <link rel="stylesheet" type="text/css" href="../../estatico/css/titulos.css">
        <link rel="stylesheet" type="text/css" href="../../estatico/css/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="../../estatico/css/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="../../estatico/css/formulario.css">
        <script src="../../estatico/js/geral/jquery-3.3.1.min.js"></script>
        <script src="../../estatico/js/geral/jquery-ui.js"></script>
        <script src="../../estatico/js/gerenciarCarro.js"></script>
        <script src="../../estatico/js/reserva-usuario/dados.js"></script>
        <script src="../../estatico/js/validadores/validadorCarro.js"></script>
        <title>Frota atual</title>
    </head>
    <%@include file="../templates/operador/cabecalho.jsp" %>

        <div class="div-titulo-pagina">
            <h3>Frota Atual</h3>
        </div>        

        <table id='tabelaCarro'>
            <tr>
                <th>Id</th>
                <th>Categoria</th>
                <th>Fabricante</th>
                <th>Status</th>
                <th>Ano</th>
                <th>Conservacao</th>
                <th>Cor</th>
                <th>Placa</th>
                <th>Quilometragem</th>
                <th>Modelo</th>
                <th>Tanque</th>
                <th></th>
            </tr>
            
            <c:forEach items="${carros}" var="carro">
                <tr>
                    <td>${carro.getIdCarro()}</td>
                    <td>${carro.getCategoria().getNome()}</td>
                    <td>${carro.getFabricante().getNome()}</td>
                    <c:set var = "status" value = "Disponível"/>
                    <c:if test="${carro.getEstaQuebrado() == 1 || carro.getStatus() == 'INDISPONIVEL'}">
                        <c:set var = "status" value = "Indisponível"/>
                    </c:if>
                    <td><c:out value = "${status}"/></td>
                    <td>${carro.getAno()}</td>
                    <td>${carro.getEstadoConserva()}</td>
                    <td>${carro.getCor()}</td>
                    <td>${carro.getPlaca()}</td>
                    <td>${carro.getQuilometragem()}</td>
                    <td>${carro.getModelo()}</td>
                    <td>${carro.getTanque()} &percnt;</td>
                    <td><input name="opcao" type="radio"></td>
                </tr>
            </c:forEach>
          </table>
        <button id="visualizarDisponiveis" class="botao-generico">Visualizar Carros Disponíveis</button>
        <button id="visualizarIndisponiveis" class="botao-generico">Visualizar Carros Indisponíveis</button>
        <button id="visualizarTodos" class="botao-generico">Vistualizar Frota Completa</button>
        <button id="editarCarro" class="botao-generico">Editar carro selecionado</button>
        <button id="excluirCarro" class="botao-excluir">Excluir carro selecionado</button>
        
        <div id="formularioEdicao">
            <form id="formCarro">
                <fieldset class="grade-formulario">
                <label>ID Carro</label><br>
                <label form="formCarro" name="idCarro"></label><br>
                <label>Categoria</label><br>
                <select name="categoria">
                  <option>Selecione</option>
                </select>
                <label>Fabricante</label>
                <select name="fabricante">
                  <option>Selecione</option>
                </select>
                <label>Ano</label>
                <input name='anoDoCarro' type='number'>
                <label>Conservacao</label>
                <select name="conservacao">
                  <option>Selecione</option>
                  <option>BOM</option>
                  <option>MÉDIO</option>
                  <option>BAIXO</option>
                </select>
                <label>Cor</label>
                <select name="corDoCarro">
                  <option>Selecione</option>
                  <option>BRANCO</option>
                  <option>PRETO</option>
                  <option>CINZA</option>
                </select>
                <label>Placa</label>
                <input name='placaDoCarro' type='text'>
                <label>Quilometragem</label>
                <input name='quilometragem' type="number">
                <label>Modelo</label>
                <input name='modeloDoCarro' type='text'>
                <label>Tanque</label>
                <input name='tanqueDoCarro' type='number'>
                <label>Carro quebrado</label>
                <select name="carroQuebrado">
                  <option>Selecione</option>
                  <option>SIM</option>
                  <option>NÃO</option>
                </select>
                </fieldset>
            </form>
        </div>
        <script src="../../estatico/js/operador/recuperarValoresAjax.js"></script>
        <script>
            
            recuperarValores("GET","/categoria/listar",document.getElementsByName("categoria")[0]);
            recuperarValores("GET","/fabricante/listar", document.getElementsByName("fabricante")[0]);
        </script>
    </body>
</html>
