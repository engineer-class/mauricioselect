<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>

<head>
    <%@include file="../templates/meta.jsp" %>
    <link rel="stylesheet" type="text/css" href="../../estatico/css/geral.css">
    <link rel="stylesheet" type="text/css" href="../../estatico/css/formulario.css">
    <link rel="stylesheet" type="text/css" href="../../estatico/css/titulos.css">
    <link rel="stylesheet" type="text/css" href="../../estatico/css/botao.css">
    <link rel="stylesheet" type="text/css" href="../../estatico/css/extras.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="../../estatico/js/manipuladorDeData.js"></script>
    <title>Aprovar reserva</title>
</head>
<%@include file="../templates/operador/cabecalho.jsp" %>

<div id="formularioReservaDialogo">

    <form>
        <div class="caixa-formulario">
            <label>Dados do cliente</label>
            <fieldset id="usuario">
                <label id="tituloCampoFormulario">Id usuario</label>
                <text id="idUsuario">${usuario.getIdUsuario()}</text>
                <label>Nome</label>
                <text id="nome">${usuario.getNome()}</text>
                <label>Sexo</label>
                <text id="sexo">${usuario.getSexo()}</text>
                <label>RG</label>
                <text id="rg">${usuario.getRg()}</text>
                <label>E-mail</label>
                <text id="email">${usuario.getEmail()}</text>
                <label>CPF</label>
                <text id="cpf">${usuario.getCpf()}</text>
                <label>Apelido</label>
                <text id="apelido">${usuario.getApelido()}</text>
            </fieldset>
        </div>

        <div class="caixa-formulario">
            <label>Dados da reserva</label>

            <fieldset id="reserva">
                <label id="tituloCampoFormulario">Id da reserva</label>
                <text id="idReserva">${reserva.getIdReserva()}</text>
                <label id="tituloCampoFormulario">Dia inicial</label>
                <text id="dataInicio">${reserva.getDiaInicio()}</text>
                <label id="tituloCampoFormulario">Dia final</label>
                <input type="date" id="dataFinal" value="${reserva.getDiaFinal()}">
                <label id="tituloCampoFormulario">Status</label>
                <select id="status">
                    <option>Selecione</option>
                    <option>PENDENTE</option>
                    <option>APROVADA</option>
                    <option>REPROVADA</option>
                </select>
                <label id="tituloCampoFormulario">Valor total</label>
                <text id="valorTotal">${reserva.getValorTotal()}</text>
            </fieldset>
        </div>
        <div class="caixa-formulario">
            <label>Dados do aluguel (Contrato de locação)</label>

            <fieldset id="aluguel">
                <label>Valor pago antecipadamente</label>
                <input type="number" id="valorAntecipado" value="0">
                <label>Valor pago na devolução</label>
                <text id="valorPagoDevolucao">0</text>
            </fieldset>
        </div>
        <div class="caixa-formulario">
            <label>Dados do carro</label>


            <fieldset id="carro">
                <label>Id carro</label>
                <text id="idCarro">${carro.getIdCarro()}</text>
                <label>Modelo</label>
                <text type="text" id="modeloCarro">${carro.getModelo()}</text>
                <label>Quilometragem</label>
                <text type='number' id="quilometragem">${carro.getQuilometragem()}</text>
                <label>Placa</label>
                <text type="text" id="placa">${carro.getPlaca()}</text>
                <label>Estado de conservação</label>
                <text type='text' id="estadoDeConservacao">${carro.getEstadoConserva()}</text>
                <label>Cor</label>
                <text type="text" id="cor">${carro.getCor()}</text>
                <label>Tanque</label>
                <text type="text" id="tanque">${carro.getTanque()}</text>
                <label>Ano</label>
                <text type="text" id="ano">${carro.getAno()}</text>
                <label>Fabricante</label>
                <text id="fabricante">${carro.getFabricante().getNome()}</text>
                <label id="tituloCampoFormulario">Categoria</label>
                <text type="text" id="idCategoria">${carro.getCategoria().getNome()}</text>
                <label>Valor por hora</label>
                <text id="valorPorHora">${carro.getCategoria().getValorPorHora()}</text>
            </fieldset>
            <button class="botao-generico" id="aprovarReserva">Confirmar</button>
        </div>
    </form>


</div>
<script src="../../estatico/js/operador/criarAluguel.js"></script>
<script>dataMinimaDosFormularios()</script>
</body>

</html>