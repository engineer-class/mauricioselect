<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="../templates/meta.jsp" %>
        <link rel="stylesheet" type="text/css" href="../../estatico/css/botao.css">
        <link rel="stylesheet" type="text/css" href="../../estatico/css/formulario.css">
        <link rel="stylesheet" type="text/css" href="../../estatico/css/tabela.css">
        <link rel="stylesheet" type="text/css" href="../../estatico/css/titulos.css">
        <link rel="stylesheet" href="../../estatico/css/jquery-ui.css">
        <script src="../../estatico/js/reserva-usuario/dados.js"></script>
        <script src="../../estatico/js/geral/jquery-3.3.1.min.js"></script>
        <script src="../../estatico/js/geral/jquery-ui.js"></script>
        <script src="../../estatico/js/manipuladorDeData.js"></script>
        <title>Gerenciamento de reservas</title>
    </head>
    <%@include file="../templates/operador/cabecalho.jsp" %>
        <div class="div-titulo-pagina">
            <h3>Gerenciamento de Reservas</h3>
        </div>
        <div class="div-tabela">
            <table id="tabelaReservas">
                <tr>
                    <th>ID Reserva</th>
                    <th>Usuario</th>
                    <th>Categoria do carro</th>
                    <th>Valor por hora</th>
                    <th>Carro</th>
                    <th>Data de início</th>
                    <th>Data final</th>
                    <th>Valor total</th>
                    <th>Status</th>
                </tr> 
                
                <c:forEach items="${reservas}" var="reserva">
                    <tr>
                        <td>${reserva.getIdReserva()}</td>
                        <td>${reserva.getUsuario().getApelido()}</td>
                        <td>${reserva.getCategoria().getNome()}</td>
                        <td>${reserva.getCategoria().getValorPorHora()}</td>
                        <td>${reserva.getCarro().getModelo()}</td>
                        <td>${reserva.getDiaInicio()}</td>
                        <td>${reserva.getDiaFinal()}</td>
                        <td>${reserva.getValorTotal()}</td>
                        <td>${reserva.getStatus()}</td>
                        <td><input id="radioSelecao" name="selecaoEdicao" value="${reserva.getIdReserva()}" type="radio"></td>
                    </tr>
                </c:forEach>            
            </table>
        </div>

        <button id="criaReserva" class="botao-generico">Criar reserva</button>
        <button id="reservasPendentes" class="botao-generico">Visualizar reservas pendentes</button>
        <button id="reservasAprovadas" class="botao-generico">Visualizar todas as reservas</button>
        <button id="atualizarReserva" class="botao-generico">Atualizar reserva selecionada</button>

        <div id="respostaAjax"></div>
        <!-- modal de atualização de reserva -->
        <div id="formularioReservaDialogo">
            <form>
                <fieldset class="grade-formulario">
                    <label id="tituloCampoFormulario">Id da reserva</label>
                    <text id="idReserva"></text>
                    <label id="tituloCampoFormulario">Usuario</label>
                    <text id="apelido"></text>
                    <label id="tituloCampoFormulario">Categoria</label>
                    <select name="categoria">
                        <option>Selecione</option>
                    </select>
                    <label>Carro</label>
                    <select name="modeloCarro">
                        <option>Selecione</option>
                        <option id="valorPadraoDoModelo"></option>
                    </select>
                    <label id="tituloCampoFormulario">Dia inicial</label>
                    <text id="dataInicial"></text>
                    <label id="tituloCampoFormulario">Dia final</label>
                    <input type="date" id="dataFinal">
                    <label id="tituloCampoFormulario">Status</label>
                    <label id="status"></label>
                    <label id="tituloCampoFormulario">Valor total</label>
                    <text id="valorTotal"></text>
                </fieldset>
            </form>
        </div>

        <!-- modal de criação de reserva -->
        <div id="criarReservaDialogo">
            <form id="criaReservaForm">
                <fieldset class="grade-formulario">
                    <label id="tituloCampoFormulario">Usuario</label>
                    <input name="apelidoC">
                    <label id="tituloCampoFormulario">Categoria</label>
                    <select name="categoriaC">
                        <option>Selecione</option>
                    </select>
                    <label>Carro</label>
                    <select name="modeloCarroC">
                        <option>Selecione</option>
                    </select>
                    <label id="tituloCampoFormularioC">Dia inicial</label>
                    <input type="date" name="dataInicialC">
                    <label id="tituloCampoFormulario">Dia final</label>
                    <input type="date" name="dataFinalC">
                </fieldset>
            </form>
        </div>

        <script src="../../estatico/js/gerenciarReservas.js"></script>
        <script src="../../estatico/js/operador/reservaAjax.js"></script>
        <script src="../../estatico/js/operador/recuperarValoresAjax.js"></script>
        <script>
            /***
            * Script disponível em js/operador/recuperarValoresAjax.js 
            */
            recuperarValoresCategoria("GET","/categoria/listar",document.getElementsByName("categoria")[0]);
            recuperarValoresCategoria("GET","/categoria/listar",document.getElementsByName("categoriaC")[0]);
            
            dataMinimaDosFormularios();
        </script>
    </body>
</html>
