<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="../templates/meta.jsp" %>
        <title>Cadastrar carros</title>
        <link rel="stylesheet" type="text/css" href="../../estatico/css/botao.css">
        <link rel="stylesheet" type="text/css" href="../../estatico/css/formulario.css">
        <link rel="stylesheet" type="text/css" href="../../estatico/css/extras.css">
        <script src="../../estatico/js/geral/jquery-3.3.1.min.js"></script>
        <script src="../../estatico/js/reserva-usuario/dados.js"></script>
        <script src="../../estatico/js/validadores/validadorCarro.js"></script>
    </head>
    <%@include file="../templates/operador/cabecalho.jsp" %>
        <div class="caixa-formulario">
            <form id="formCarro" method="post" action="/cadastrar/carro">
                
                <div class="grade-formulario">
                    <label>Categoria</label>
                    <select name="categoria">
                        <option>Selecione</option>
                    </select>
                </div>
                <div class="grade-formulario">
                    <label>Fabricante</label>
                    <select name="fabricante">
                        <option>Selecione</option>
                    </select>
                </div>
                <div class="grade-formulario">
                <label>Conservação</label> 
                    <select name="conservacao">
                        <option>Selecione</option>
                        <option>BOM</option>
                        <option>MÉDIO</option>
                        <option>BAIXO</option>
                    </select>
                </div>
                <label>Ano</label>
                <input type="number" min="1990" max="9999" name="anoDoCarro">
                <label>Placa</label>
                <input type="text" maxlength="7" name="placaDoCarro">
                <label>Modelo</label>
                <input type="text" name="modeloDoCarro">
                <div class="grade-formulario">
                    <label>Cor</label>
                    <select name="corDoCarro">
                        <option>Selecione</option>
                        <option>PRETO</option>
                        <option>BRANCO</option>
                        <option>CINZA</option>
                    </select>
                </div>
                <label>Quilometragem</label>
                <input type="number" min="0" name="quilometragem">
                <label>Tanque &percnt;</label>
                <input type="number" min="0" max="100" name="tanqueDoCarro">
            </form>
            <button id="btnCadastra" class="botao-generico">cadastrar</button>
            
        </div>
         <%
            if("sim".equals(request.getAttribute("carroCadastrado"))){
                out.print("<p>Carro cadastrado com sucesso.</p>");
            } else if ("nao".equals(request.getAttribute("operadorCadastrado"))) {
                out.print("<p>Algo deu errado ao cadastrar o operador. Consulte"
                        + "o seu sysadmin. </p>");
            }
        %>
        <script src="../../estatico/js/operador/recuperarValoresAjax.js"></script>
        <script src="../../estatico/js/operador/cadastrarCarro.js"> </script>
    </body>
</html>
