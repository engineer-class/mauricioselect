<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="../templates/meta.jsp" %>
        <title>Confirmação de reserva</title>
        <script src="../../estatico/js/jquery-3.3.1.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    </head>
    <%@include file="../templates/operador/cabecalho.jsp" %>
        <label>Id reserva</label>
        <input id="idReserva" type="number" min="0" required>
        <button id="botao" name="botao">Alugar</button>
 
        <div id="respostaReserva"></div>
        
        <div id="formularioReservaDialogo" >
            <form >
                <fieldset>
                    <label id="tituloCampoFormulario">Usuario</label><br>
                    <input type="text" id="idUsuario"><br>
                    <label id="tituloCampoFormulario">Categoria</label><br>
                    <input type="text" id="idCategoria"><br>
                    <label>Carro</label><br>
                    <input type="text" id="carro"><br>
                    <label id="tituloCampoFormulario">Dia inicial</label><br>
                    <input type="date" id="dataInicial"><br>
                    <label id="tituloCampoFormulario">Dia final</label><br>
                    <input type="date" id="dataFinal"><br>
                    <label id="tituloCampoFormulario">Status</label><br>
                    <input type="text" id="status"><br>
                    <label id="tituloCampoFormulario">Valor antecipado</label><br>
                    <input type="number" id="valorAntecipado"><br>
                    <label id="tituloCampoFormulario">Valor total</label><br>
                    <input type="number" id="valorTotal"><br>
                    
                </fieldset>
            </form>        
        </div>
        <script src="../../estatico/js/operador/aprovaReserva.js"></script>
        <script src="../../estatico/js/operador/aprovaReservaAjax.js"></script>
    </body>
</html>
