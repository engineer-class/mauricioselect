<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="../templates/meta.jsp" %>
        <title>Página inicial</title>
    </head>
    
    <%@include file="../templates/operador/cabecalho.jsp" %>
        <a href="/gerencia/operador">Gerenciar operador</a>
        <a href="/reserva/listar">Gerenciar reservas</a>
        <a href="/aluguel/listar">Gerenciar alugueis</a>
        <a href="/carro/listar">Visualizar situação da frota</a>
        <a href="/carro/cadastrar">Cadastrar carro</a>
        <a href="/fabricante">Gerenciar fabricantes</a>
        <a href="/categoria">Gerenciar categorias</a>
    </body>
</html>
