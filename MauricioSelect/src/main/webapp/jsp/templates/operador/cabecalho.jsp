<%-- Cabeçalho das páginas --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<body>

    <ul class="barra-de-navegacao">
        <li><a href="../../../html/operador-principal.html">Início</a></li>
        <li><a href="/perfil">Perfil</a></li>

        <li class="menu-abaixa">
            <a href="javascript:void(0)" class="selecionado">Operadores</a>
            <div class="menu-abaixa-conteudo">
                <a href="/gerencia/operador">Gerenciar operadores</a>
            </div>
        </li>

        <li class="menu-abaixa">
            <a href="javascript:void(0)" class="selecionado">Reservas</a>
            <div class="menu-abaixa-conteudo">
                <a href="/reserva/listar">Gerenciar reservas</a>
            </div>
        </li>

        <li class="menu-abaixa">
            <a href="javascript:void(0)" class="selecionado">Alugueis</a>
            <div class="menu-abaixa-conteudo">
                <a href="/aluguel/listar/todos">Gerenciar alugueis</a>
            </div>
        </li>

        <li class="menu-abaixa">
            <a href="javascript:void(0)" class="selecionado">Carros</a>
            <div class="menu-abaixa-conteudo">
                <a href="/carro/listar">Visualizar situação atual da frota</a>
                <a href="/carro/cadastrar">Cadastrar novo carro</a>
                <a href="/fabricante">Gerenciar fabricantes</a>
                <a href="/categoria">Gerenciar categorias</a>
            </div>
        <li><a href="/sair">Sair</a></li>
        <a href="javascript:void(0);" class="icon" onclick="menuOpcao()">
            <i class="fa fa-bars"></i>
        </a>
    </ul>

    