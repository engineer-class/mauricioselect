<%-- Arquivo de configuração da página, com informações sobre os dados que serão renderizados --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="stylesheet" type="text/css" href="../../estatico/css/geral.css">
<link rel="stylesheet" href="../../estatico/css/font-awesome.min.css">
<script src="../../estatico/js/geral/alteraMenu.js"></script>
<script src="../../estatico/js/geral/pace.min.js"></script>
<link rel="stylesheet" type="text/css" href="../../estatico/css/geral.css">
