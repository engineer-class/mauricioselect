<%@page import="com.fatec.mauricioselect.api.modelo.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <%@include file="/jsp/templates/meta.jsp"%>
    <title>Perfil - MauricioSelect</title>
    <script src="../../estatico/js/geral/pace.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../estatico/css/geral.css">
    <link rel="stylesheet" type="text/css" href="../../estatico/css/extras.css">
    <link rel="stylesheet" type="text/css" href="../../estatico/css/imagem.css">
    <link rel="stylesheet" type="text/css" href="../../estatico/css/formulario.css">
    <link rel="stylesheet" type="text/css" href="../../estatico/css/titulos.css">
    <link rel="stylesheet" type="text/css" href="../../estatico/css/botao.css">
    <script src="../../estatico/js/geral/jquery-3.3.1.min.js"></script>
    <script src="../../estatico/js/CPF.js"></script>
    <script src="../../estatico/js/controleDeAlteracoes.js"></script>
    <script src="../../estatico/js/ajax.js"></script>
    <script>   
        /**
         * Função para pegar os dados do usuário na tela e enviar para o servidor
         * 
         * @returns {void}
         */
        function servicoPost() {
            let data = salvaAlteracoesPerfil();

            if (data) {
                ajaxPostDadosJQuery('/perfil', data, function (data){
                     alert('Usuário atualizado com sucesso!');
                }); 
            } else {
                alert('Preencha todos os campos corretamente');
            }
        }                           
    </script>
</head>
<body>
    <%
        String tipo = (String) session.getAttribute("tipoUsuario");
        if("usuario".equals(tipo)){%>
            <jsp:include page="../templates/usuario/cabecalho.jsp" flush="true" />
        <%} else {%>
            <jsp:include page="../templates/operador/cabecalho.jsp" flush="true" />
    <%}%>
    
    <h3 class="titulo-pagina">Seu perfil</h3>
    <h3 class="titulo-pagina"><img class="imagem-pequena" src="../../estatico/recursos/usuario.png"></h3>

    <div class="caixa-formulario">
        <div class="grade-formulario">
    <%
        Usuario usuario = (Usuario) request.getAttribute("usuario");        
        if (usuario != null) {
            out.print("<label>Apelido do usuário</label>");
            out.print("<label id=\"apelidoUsuario\">" + "\"" + usuario.getApelido() + "\"" + "</label>");

            out.print("<label>Nome do usuário</label>");
            out.print("<input id=\"nomeUsuario\" value=" + "\"" + usuario.getNome() + "\"" + ">");

            out.print("<label>Email</label>");
            out.print("<input id=\"emailUsuario\" value=" + usuario.getEmail() + ">");

            out.print("<label>RG</label>");
            out.print("<input id=\"rgUsuario\" value=" + usuario.getRg() + ">");

            out.print("<label>CPF</label>");
            out.print("<input id=\"cpfUsuario\" value=" + usuario.getCpf() + ">");   

            out.print("<label>Senha</label>");
            out.print("<input id=\"senhaUsuarioUm\" type=\"password\">"); 
            out.print("<label></label>");
            out.print("<input id=\"senhaUsuarioDois\" type=\"password\">");
        } else {
            out.print("<label>Usuário não encontrado</label>");
        }
    %>
        </div>
        <button id="botaoMudaPerfil" class="botao-generico" onclick="servicoPost();">Salvar alterações</button>
    </div>
</body>
</html>