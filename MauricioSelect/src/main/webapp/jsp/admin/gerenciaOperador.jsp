<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <%@include file="../templates/meta.jsp" %>
    <link rel="stylesheet" type="text/css" href="../../estatico/css/operador.css">
    <link rel="stylesheet" type="text/css" href="../../estatico/css/botao.css">
    <link rel="stylesheet" type="text/css" href="../../estatico/css/formulario.css">
    <link rel="stylesheet" type="text/css" href="../../estatico/css/titulos.css">
    <script type="text/javascript" src="../../estatico/js/jquery-3.3.1.js"></script>
    <title>Cadastro de operador</title>
</head>
<%@include file="../templates/operador/cabecalho.jsp" %>

    <div class="div-titulo-pagina">
        <h3>Gerenciamento de operadores</h3>
    </div>

    <form class="caixa-formulario" method="post" action="/cadastro/operador">
        <div class="grade-formulario">
            <label>Nome do usuário:</label>
            <input id="nomeUsuario" name="nomeUsuario" type="text" required>
            <label>Cargo: </label>
            <input id="cargoUsuario" name="cargoUsuario" type="text" required>
        </div>
        <input style="height : 30px;" type="submit" class="botao-generico" value="cadastrar">
    </form>
    <div class="caixa-formulario" id="removerOperador">
        <div class="grade-formulario">
            <label>Nome do usuário:</label>
            <input id="nomeUsuarioAtualiza" name="nomeUsuario" type="text">
        </div>
        <input id="botao-remove" style="height : 30px;" type="button" class="botao-excluir" value="remover operador">
    </div>

<%
        if("sim".equals(request.getAttribute("operadorCadastrado"))){
            %>
            <script>
                window.onload = () => {
                    alert("Operador cadastrado com sucesso")
                }
            </script>
            <%
            
        } else if ("nao".equals(request.getAttribute("operadorCadastrado"))) {
            %>
            <script>
                window.onload = () => {
                    alert("Algo deu errado ao cadastrar o operador. Consulte o seu sysadmin")
                }
            </script>
            <%
        }
    %>
    <script type="text/javascript" src="../../estatico/js/operador/validaCadastroOperador.js"></script> 
    <script type="text/javascript" src="../../estatico/js/operador/removerOperador.js"></script>
</body>

</html>