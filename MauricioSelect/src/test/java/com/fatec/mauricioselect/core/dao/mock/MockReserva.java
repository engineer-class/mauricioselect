/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fatec.mauricioselect.core.dao.mock;

import com.fatec.mauricioselect.api.modelo.Reserva;
import com.fatec.mauricioselect.api.modelo.ReservaBuilder;
import java.sql.Date;
import java.util.Calendar;

public class MockReserva {
    public static Reserva getMockReserva(){
        Date data = new Date(Calendar.getInstance().getTime().getTime());
        return ReservaBuilder.getBuilder()            
            .adicionaDiaFinal(data)
            .adicionaDiaInicio(data)
            .adicionaStatus("APROVADA")
            .adicionaValorTotal(1.1)
            .build();
    }
}
