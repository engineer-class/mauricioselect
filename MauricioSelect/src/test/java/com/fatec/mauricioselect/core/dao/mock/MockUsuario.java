/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fatec.mauricioselect.core.dao.mock;

import com.fatec.mauricioselect.api.modelo.Usuario;
import com.fatec.mauricioselect.api.modelo.UsuarioBuilder;

public class MockUsuario {
    
    public static Usuario getMockUsuario(){
        return UsuarioBuilder.getBuilder()
               .adicionaApelido("usuarioTeste")
               .adicionaSenha("123456")
               .adicionaCPF("43319455885")
               .adicionaRG("123456789")
               .adicionaNome("usuario de teste")
               .adicionaEmail("teste@teste.com")
               .adicionaSexo("M").build();
    }
}
