package com.fatec.mauricioselect.core.dao;

import com.fatec.mauricioselect.core.dao.mock.MockUsuario;
import com.fatec.mauricioselect.api.dao.UsuarioDAO;
import com.fatec.mauricioselect.api.modelo.Usuario;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UsuarioDAOTeste {
    
    Usuario u;
    
    @Before
    public void setUp(){
        u = MockUsuario.getMockUsuario();
    }
    
    @Test
    public void encontrarPorApelidoTeste1(){
        UsuarioDAO usuarioDAO = new UsuarioDAOMariaDB();
        usuarioDAO.inserir(u);
        assertNotNull(usuarioDAO.encontrarPorApelido("usuarioTeste"));
        assertEquals(u.getApelido(),usuarioDAO.encontrarPorApelido("usuarioTeste").getApelido());
    }
    
    @Test
    public void remocaoUsuarioTeste2(){
        UsuarioDAO usuarioDAO = new UsuarioDAOMariaDB();
        Usuario u = usuarioDAO.encontrarPorApelido("usuarioTeste");
        usuarioDAO.deletar(u.getIdUsuario());
        assertNull(usuarioDAO.encontrarPorId(u.getIdUsuario()));
    }
    
    @Test
    public void retornoNuloAposNaoEncontrarUsuarioTeste3(){
        UsuarioDAO usuarioDAO = new UsuarioDAOMariaDB();
        assertNull(usuarioDAO.encontrarPorApelido("gasparzinho"));
    }
    
}
