package com.fatec.mauricioselect.core.dao;

import com.fatec.mauricioselect.api.modelo.Categoria;
import java.util.List;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;


/**
 *
 * @author felipe
 */
public class CategoriaDAOMariaDBTeste {
    
    private CategoriaDAOMariaDB categoriaDAOMariaDB;
    
    @Before
    public void setUp() {
        categoriaDAOMariaDB = new CategoriaDAOMariaDB();
    }
    
    @After
    public void tearDown() {
        categoriaDAOMariaDB = null;
    }

    /**
     * Método para realizar o teste de inserir e deletar a categoria 
     * do banco de dados
     */
    @Test
    public void testeInserirEDeletarCategoria() {
        Categoria categoria = new Categoria("categoriaDeTeste", 1.1234); 
        Categoria categoriaInserida = categoriaDAOMariaDB.inserir(categoria);
        assertNotNull(categoriaInserida);
        assertTrue(categoriaDAOMariaDB.deletar(categoriaInserida.getIdCategoria()));
    }
    
    /**
     * Método para realizar o teste da atualização de uma categoria do banco de dados
     */
    @Test
    public void testeAtualizacaoCategoria() {
        Categoria categoriaAntiga = new Categoria("categoriaAntiga", 1.1234);
        Categoria categoriaNova = new Categoria("categoriaNova", 1.1234);
        
        Categoria categoriaNovaInserida = categoriaDAOMariaDB.inserir(categoriaAntiga);
        assertNotNull(categoriaDAOMariaDB.atualizar(categoriaAntiga, categoriaNovaInserida));
        categoriaDAOMariaDB.deletar(categoriaNovaInserida.getIdCategoria());
    }
    
    /**
     * Método para verificar a recuperação de todos os itens do banco de dados
     */
    @Test
    public void testeRecuperaTudo() {
        Categoria categoriaUm = new Categoria("catUm", 1.123124);
        Categoria categoriaDois = new Categoria("catDois", 7.34234);
        
        Categoria categoriaUmInserida = categoriaDAOMariaDB.inserir(categoriaUm);
        Categoria categoriaDoisInserida = categoriaDAOMariaDB.inserir(categoriaDois);
        
        List<Categoria> categorias = categoriaDAOMariaDB.encontrarTudo();
        assertFalse(categorias.isEmpty());
        
        categoriaDAOMariaDB.deletar(categoriaUmInserida.getIdCategoria());
        categoriaDAOMariaDB.deletar(categoriaDoisInserida.getIdCategoria());
    }   
}
