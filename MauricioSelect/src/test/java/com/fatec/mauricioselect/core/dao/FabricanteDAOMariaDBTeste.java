package com.fatec.mauricioselect.core.dao;

import com.fatec.mauricioselect.api.modelo.Fabricante;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author felipe
 */
public class FabricanteDAOMariaDBTeste {
    
    private FabricanteDAOMariaDB fabricanteDAOMariaDB; 
        
    @Before
    public void setUp() {
        fabricanteDAOMariaDB = new FabricanteDAOMariaDB();
    }
    
    @After
    public void tearDown() {
        fabricanteDAOMariaDB = null;
    }
    
    /**
     * Método de teste para verificar se a inserção e exclusão de fabricantes está sendo
     * feita corretamente
     */
    @Test
    public void testeInsereEDeletaFabricante() {
        Fabricante fabricante = new Fabricante("fabricaTeste");
        Fabricante fabricanteInserido = fabricanteDAOMariaDB.inserir(fabricante);
        
        assertNotNull(fabricanteInserido);
        assertTrue(fabricanteDAOMariaDB.deletar(fabricanteInserido.getIdFabricante()));
    }
    
    /**
     * Método para testar a atualização de modelos no banco de dados
     */
    @Test
    public void testeAtualizacaoDeFabricante() {
        Fabricante fabricanteAntigo = new Fabricante("antigo");
        Fabricante fabricanteAtual = new Fabricante("novo");
        
        Fabricante fabricanteInserido = fabricanteDAOMariaDB.inserir(fabricanteAntigo);
        assertNotNull(fabricanteDAOMariaDB.atualizar(fabricanteAntigo, fabricanteInserido));
        fabricanteDAOMariaDB.deletar(fabricanteInserido.getIdFabricante());
    }
    
    /**
     * Método para retornar todos os objetos presentes no banco de dados
     */
    @Test
    public void testeEncontraTudo() {
        Fabricante fabricanteUm = new Fabricante("fabricUm");
        Fabricante fabricanteDois = new Fabricante("fabricDois");
        
        Fabricante fabricanteUmInserido = fabricanteDAOMariaDB.inserir(fabricanteUm);
        Fabricante fabricanteDoisInserido = fabricanteDAOMariaDB.inserir(fabricanteDois);
        
        List<Fabricante> fabricantes = fabricanteDAOMariaDB.encontrarTudo();
        assertFalse(fabricantes.isEmpty());
    
        fabricanteDAOMariaDB.deletar(fabricanteUmInserido.getIdFabricante());
        fabricanteDAOMariaDB.deletar(fabricanteDoisInserido.getIdFabricante());
    }
}
