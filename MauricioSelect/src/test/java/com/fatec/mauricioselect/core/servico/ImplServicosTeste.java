package com.fatec.mauricioselect.core.servico;

import com.fatec.mauricioselect.api.modelo.Operador;
import com.fatec.mauricioselect.api.modelo.OperadorBuilder;
import com.fatec.mauricioselect.api.modelo.Usuario;
import com.fatec.mauricioselect.api.modelo.UsuarioBuilder;
import com.fatec.mauricioselect.core.dao.mock.MockUsuario;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ImplServicosTeste {
    
    Operador operador;
    Usuario usuario;
    ImplOperadorServico operadorServico;
    ImplUsuarioServico usuarioServico;
    
    @Before
    public void setUp() {
        operadorServico = new ImplOperadorServico();
        usuarioServico = new ImplUsuarioServico();
        
        usuario = MockUsuario.getMockUsuario();
        operador = OperadorBuilder.getBuilder()
                    .consomeObjetoUsuario(usuario)    
                    .adicionaCargo("tester").build();
    }
    
    @After
    public void tearDown() {
        operadorServico = null;
        usuarioServico = null;
    }

    @Test
    public void buscaPorApelidoTeste1() {
        
        usuarioServico.inserir(usuario);
        operadorServico.inserir(operador);
        
        Operador operadorTeste = operadorServico.encontrarPorApelido(usuario.getApelido());
        assertNotNull(operadorTeste);
    }
    
    @Test
    public void deletaUsuarioEOperadorTeste2() {
        
        usuario = usuarioServico.encontrarPorApelido(usuario.getApelido());
        operador = OperadorBuilder.getBuilder().consomeObjetoUsuario(usuario)
                                                .adicionaCargo("tester").build();
        
        operadorServico.deleta(operador);
        usuarioServico.deleta(usuario);
    }   
}
