package com.fatec.mauricioselect.core.dao;

import com.fatec.mauricioselect.api.modelo.Carro;
import com.fatec.mauricioselect.api.modelo.CarroBuilder;
import com.fatec.mauricioselect.api.modelo.Categoria;
import com.fatec.mauricioselect.api.modelo.Fabricante;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author felipe
 */
public class CarroDAOMariaDBTeste {
    
    private CarroDAOMariaDB carroDAOMariaDB;
    private CategoriaDAOMariaDB categoriaDAOMariaDB;
    private FabricanteDAOMariaDB fabricanteDAOMariaDB;
    private Carro carroUmInserido; 
    private Carro carroDoisInserido;
    private Fabricante fabricante;
    private Categoria categoria;
    
    @Before
    public void setUp() {
        carroDAOMariaDB = new CarroDAOMariaDB();
        categoriaDAOMariaDB = new CategoriaDAOMariaDB();
        fabricanteDAOMariaDB = new FabricanteDAOMariaDB();
       
        categoriaDAOMariaDB.inserir(new Categoria("categoriaTeste", 2.0));
        fabricanteDAOMariaDB.inserir(new Fabricante("fabricanteTeste"));

        categoria = categoriaDAOMariaDB.encontrarPorNome("categoriaTeste");        
        fabricante = fabricanteDAOMariaDB.encontrarPorNome("fabricanteTeste");

        Carro carroUm = CarroBuilder.getBuilder()
                        .adicionaIDFabricante(fabricante.getIdFabricante())
                        .adicionaCategoria(categoria.getIdCategoria())
                        .adicionaAno(2016)
                        .adicionaCor("PRETO")
                        .adicionaModelo("Corporativo")
                        .adicionaEstadoConserva("MÉDIO")
                        .adicionaPlaca("a1s2d3")
                        .adicionaQuilometragem(1.1234)
                        .adicionaTanque(123)
                        .build();
        
        Carro carroDois = CarroBuilder.getBuilder()
                        .adicionaIDFabricante(fabricante.getIdFabricante())
                        .adicionaCategoria(categoria.getIdCategoria())
                        .adicionaAno(2016)
                        .adicionaCor("CINZA")
                        .adicionaModelo("Econômico")
                        .adicionaEstadoConserva("MÉDIO")
                        .adicionaPlaca("a1s2d5")
                        .adicionaQuilometragem(1.1234)
                        .adicionaTanque(123).build();
        
        carroUm.setEstaQuebrado(0);
        carroDois.setEstaQuebrado(0);
        
        carroUmInserido = carroDAOMariaDB.inserir(carroUm);
        carroDoisInserido = carroDAOMariaDB.inserir(carroDois);  
    }
    
    @After
    public void tearDown() {
        
        carroDAOMariaDB.deletar(carroUmInserido.getIdCarro());
        carroDAOMariaDB.deletar(carroDoisInserido.getIdCarro());
        
        categoriaDAOMariaDB.deletar(categoria.getIdCategoria());
        fabricanteDAOMariaDB.deletar(fabricante.getIdFabricante());
                
        carroDAOMariaDB = null;
        categoriaDAOMariaDB = null;
        fabricanteDAOMariaDB = null;

        carroUmInserido = null;
        carroDoisInserido = null;
        fabricante = null;
        categoria = null;
    }
    
    @Test
    public void testeInserirCarro() {
        assertNotNull(carroUmInserido);
        assertNotNull(carroDoisInserido);
    } 
    
    @Test
    public void testeBuscaPorPlaca() {
        Carro carroProcurado = carroDAOMariaDB.encontrarPorPlaca(carroUmInserido.getPlaca());
        assertNotNull(carroProcurado);
    }
    
    @Test
    public void testeBuscaPorID() {
        Carro carroProcurado = carroDAOMariaDB.encontrarPorId(carroUmInserido.getIdCarro());
        assertNotNull(carroProcurado);
    }
    
    @Test
    public void testeBuscaPorModelo() {
        List<Carro> carros = carroDAOMariaDB.encontrarPorModelo("Econômico");
        assertFalse(carros.isEmpty());
    }
    
    @Test
    public void testeBuscaPorAno() {         
        List<Carro> carros = carroDAOMariaDB.encontrarPorAno(2016);
        assertFalse(carros.isEmpty());
    }
    
    @Test
    public void testeBuscaPorFabricante() {
        List<Carro> carros = carroDAOMariaDB.encontrarPorFabricante(fabricante.getIdFabricante());
        assertFalse(carros.isEmpty());     
    }
    
    @Test
    public void testBuscaPorCategoria() {
        List<Carro> carros = carroDAOMariaDB.encontrarPorCategoria(categoria.getIdCategoria());
        assertFalse(carros.isEmpty());
    }
    
    @Test
    public void testeRecuperaTudo() {
        List<Carro> carros = carroDAOMariaDB.encontrarTudo();
        assertFalse(carros.isEmpty());
    }
    
    @Test
    public void testeAtualizacao() {
        carroDoisInserido.setPlaca("01010101");
        Carro carroAtualizado = carroDAOMariaDB.atualizar(carroUmInserido, carroDoisInserido);
        assertEquals(carroDoisInserido.getPlaca(), carroAtualizado.getPlaca());
    }
    
}
