package com.fatec.mauricioselect.core.dao;

import com.fatec.mauricioselect.core.dao.mock.MockReserva;
import com.fatec.mauricioselect.core.dao.mock.MockUsuario;
import com.fatec.mauricioselect.api.dao.AluguelDAO;
import com.fatec.mauricioselect.api.dao.CarroDAO;
import com.fatec.mauricioselect.api.dao.CategoriaDAO;
import com.fatec.mauricioselect.api.dao.FabricanteDAO;
import com.fatec.mauricioselect.api.dao.OperadorDAO;
import com.fatec.mauricioselect.api.dao.ReservaDAO;
import com.fatec.mauricioselect.api.dao.UsuarioDAO;
import com.fatec.mauricioselect.api.modelo.Aluguel;
import com.fatec.mauricioselect.api.modelo.AluguelBuilder;
import com.fatec.mauricioselect.api.modelo.Carro;
import com.fatec.mauricioselect.api.modelo.Categoria;
import com.fatec.mauricioselect.api.modelo.Fabricante;
import com.fatec.mauricioselect.api.modelo.Operador;
import com.fatec.mauricioselect.api.modelo.OperadorBuilder;
import com.fatec.mauricioselect.api.modelo.Reserva;
import com.fatec.mauricioselect.api.modelo.Usuario;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AluguelDAOTeste {

    private static Usuario usuarioUm, usuarioDois;
    private static Operador operador;
    private static Aluguel aluguel;
    private static Date data;
    private static Categoria categoria;
    private static Carro carro;
    private static Reserva reserva, reservaNova;
    private static Fabricante fabricante;

    private CategoriaDAO categoriaDAO;
    private OperadorDAO operadorDAO;
    private ReservaDAO reservaDAO;
    private AluguelDAO aluguelDAO;
    private UsuarioDAO usuarioDAO;
    private CarroDAO carroDAO;
    private FabricanteDAO fabricanteDAO;

    @BeforeClass
    public static void insereDependencias() {

        FabricanteDAO fabricanteDAO = new FabricanteDAOMariaDB();
        CategoriaDAO categoriaDAO = new CategoriaDAOMariaDB();
        OperadorDAO operadorDAO = new OperadorDAOMariaDB();
        UsuarioDAO usuarioDAO = new UsuarioDAOMariaDB();
        CarroDAO carroDAO = new CarroDAOMariaDB();

        categoria = categoriaDAO.inserir(new Categoria("teste", 1.0));

        fabricante = new Fabricante("teste");
        Fabricante f = fabricanteDAO.inserir(fabricante);
        fabricante.setIdFabricante(f.getIdFabricante());

        data = new Date(Calendar.getInstance().getTime().getTime());

        usuarioUm = MockUsuario.getMockUsuario();
        Usuario usuario = usuarioDAO.inserir(usuarioUm);
        usuarioUm.setIdUsuario(usuario.getIdUsuario());

        operador = operadorDAO.inserir(OperadorBuilder.getBuilder()
                .consomeObjetoUsuario(usuarioUm)
                .adicionaCargo("tester")
                .build());

        carro = new Carro(
                categoria.getIdCategoria(),
                fabricante.getIdFabricante(),
                1990,
                "BOM",
                "PRETO",
                "Teste123",
                1.0,
                100,
                "modelo-test",
                0);

        Carro c = carroDAO.inserir(carro);
        carro.setIdCarro(c.getIdCarro());

        Reserva reservaInserida = MockReserva.getMockReserva();
        reservaInserida.setIdCategoria(categoria.getIdCategoria());
        reservaInserida.setIdUsuario(usuarioUm.getIdUsuario());
        reservaInserida.setIdCarro(carro.getIdCarro());
        ReservaDAO reservaDAO = new ReservaDAOMariaDB();
        reserva = reservaDAO.inserir(reservaInserida);

        aluguel = AluguelBuilder.getBuilder()
                .adicionaIdOperador(operador.getIdUsuario())
                .adicionaIdUsuario(usuarioUm.getIdUsuario())
                .adicionaIdReserva(reserva.getIdReserva())
                .adicionaValorTotal(2.0)
                .adicionaValorPagoAntecipado(1.0)
                .adicionaIdCarro(carro.getIdCarro())
                .adicionaDataDevolucao(data)
                .adicionaDataRetirada(data)
                .build();
    }

    @Before
    public void setUp() {

        fabricanteDAO = new FabricanteDAOMariaDB();
        aluguelDAO = new AluguelDAOMariaDB();
        categoriaDAO = new CategoriaDAOMariaDB();
        operadorDAO = new OperadorDAOMariaDB();
        reservaDAO = new ReservaDAOMariaDB();
        usuarioDAO = new UsuarioDAOMariaDB();
        carroDAO = new CarroDAOMariaDB();
    }

    @After
    public void tearDown() {

        fabricanteDAO = null;
        aluguelDAO = null;
        categoriaDAO = null;
        operadorDAO = null;
        reservaDAO = null;
        usuarioDAO = null;
        carroDAO = null;
    }

    @Test
    public void teste1Inserir() {
        
        Aluguel aluguelRetorno = aluguelDAO.inserir(aluguel);
        aluguel.setIdAluguel(aluguelRetorno.getIdAluguel());
        assertNotNull(aluguelRetorno.getIdAluguel());
        
    }

    @Test
    public void teste2EncontrarAluguel() {
        
        Aluguel aluguelRetorno = aluguelDAO.encontrarAluguel(aluguel);
        assertNotNull(aluguelRetorno);
        assertEquals(aluguel.getIdUsuario(), aluguelRetorno.getIdUsuario());
        assertEquals(aluguel.getIdOperador(), aluguelRetorno.getIdOperador());
        assertEquals(aluguel.getIdReserva(), aluguelRetorno.getIdReserva());
        assertNotNull(aluguelRetorno.getIdAluguel());

    }

    @Test
    public void teste3EncontrarPorId() {
        
        Aluguel aluguelEncontrado = aluguelDAO.encontrarPorId(aluguel.getIdAluguel());
        aluguel.setIdAluguel(aluguelEncontrado.getIdAluguel());
        assertNotNull(aluguelEncontrado);
        assertEquals(aluguel.getIdUsuario(), aluguelEncontrado.getIdUsuario());
        assertEquals(aluguel.getIdOperador(), aluguelEncontrado.getIdOperador());
        assertEquals(aluguel.getIdReserva(), aluguelEncontrado.getIdReserva());
        assertNotNull(aluguelEncontrado.getIdAluguel());
        
    }

    @Test
    public void teste4EncontrarTudo() {
        
        List<Aluguel> alugueis = null;
        alugueis = aluguelDAO.encontrarTudo();
        assertNotNull(alugueis);
        assertTrue(alugueis.size() >= 1);
        
    }

    @Test
    public void teste5Atualizar() {

        usuarioDois = MockUsuario.getMockUsuario();
        usuarioDois.setApelido("usuarioTesteDois");
        Usuario usuario = usuarioDAO.inserir(usuarioDois);
        usuarioDois.setIdUsuario(usuario.getIdUsuario());

        Reserva reservaInserida = MockReserva.getMockReserva();
        reservaInserida.setIdCategoria(categoria.getIdCategoria());
        reservaInserida.setIdUsuario(usuarioDois.getIdUsuario());
        reservaInserida.setIdCarro(carro.getIdCarro());
        reservaNova = reservaDAO.inserir(reservaInserida);

        Aluguel aluguelAtual = AluguelBuilder.getBuilder()
                .adicionaIdOperador(operador.getIdUsuario())
                .adicionaIdUsuario(usuarioUm.getIdUsuario())
                .adicionaIdReserva(reservaNova.getIdReserva())
                .adicionaValorTotal(2.0)
                .adicionaValorPagoAntecipado(1.0)
                .adicionaIdCarro(carro.getIdCarro())
                .adicionaDataDevolucao(data)
                .adicionaDataRetirada(data)
                .build();
        Aluguel aluguelAtualizado = aluguelDAO.atualizar(aluguel, aluguelAtual);

        assertNotNull(aluguelAtualizado);
        
    }

    @Test
    public void teste6Deletar() {

        assertTrue(aluguelDAO.deletar(aluguel.getIdAluguel()));
        reservaDAO.deletar(reserva.getIdReserva());
        reservaDAO.deletar(reservaNova.getIdReserva());
        operadorDAO = new OperadorDAOMariaDB();
        operadorDAO.deletar(operador.getIdUsuario());
        usuarioDAO.deletar(usuarioUm.getIdUsuario());
        usuarioDAO.deletar(usuarioDois.getIdUsuario());
        carroDAO.deletar(carro.getIdCarro());
        fabricanteDAO.deletar(fabricante.getIdFabricante());
        categoriaDAO.deletar(categoria.getIdCategoria());
        
    }

//    @Test
//    public void teste7EncontrarAlugueisDisponiveis(){
//        List<Aluguel> alugueis = aluguelDAO.encontrarAlugueisDisponiveis();
//        assertEquals(2,alugueis.size());
//    }
}
