package com.fatec.mauricioselect.core.dao;

import com.fatec.mauricioselect.core.dao.mock.MockReserva;
import com.fatec.mauricioselect.core.dao.mock.MockUsuario;
import com.fatec.mauricioselect.api.dao.CarroDAO;
import com.fatec.mauricioselect.api.dao.CategoriaDAO;
import com.fatec.mauricioselect.api.dao.FabricanteDAO;
import com.fatec.mauricioselect.api.dao.ReservaDAO;
import com.fatec.mauricioselect.api.dao.UsuarioDAO;
import com.fatec.mauricioselect.api.modelo.Carro;
import com.fatec.mauricioselect.api.modelo.Categoria;
import com.fatec.mauricioselect.api.modelo.Fabricante;
import com.fatec.mauricioselect.api.modelo.Reserva;
import com.fatec.mauricioselect.api.modelo.Usuario;
import java.util.List;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ReservaDAOTeste {

    private static Usuario usuarioUm, usuarioDois;
    private static Reserva r;
    private static Categoria categoria;
    private static Fabricante fabricante;
    private static Carro carro;
    private ReservaDAO reservaDAO;
    private UsuarioDAO usuarioDAO;
    private Reserva reservaNova;
    private CarroDAO carroDAO;
    private FabricanteDAO fabricanteDAO;
    private CategoriaDAO categoriaDAO;

    @BeforeClass
    public static void inserirCategoria() {

        CategoriaDAO categoriaDAO = new CategoriaDAOMariaDB();
        UsuarioDAO usuarioDAO = new UsuarioDAOMariaDB();
        FabricanteDAO fabricanteDAO = new FabricanteDAOMariaDB();
        CarroDAO carroDAO = new CarroDAOMariaDB();

        categoria = categoriaDAO.inserir(new Categoria("teste", 1.0));
        
        fabricante = new Fabricante("teste");
        Fabricante f = fabricanteDAO.inserir(fabricante);
        fabricante.setIdFabricante(f.getIdFabricante());

        carro = new Carro(
                categoria.getIdCategoria(),
                fabricante.getIdFabricante(),
                1990,
                "BOM",
                "PRETO",
                "Teste123",
                1.0,
                100,
                "modelo-test",
                0);

        Carro c = carroDAO.inserir(carro);
        carro.setIdCarro(c.getIdCarro());

        usuarioUm = MockUsuario.getMockUsuario();
        Usuario usuario = usuarioDAO.inserir(usuarioUm);
        usuarioUm.setIdUsuario(usuario.getIdUsuario());

        r = MockReserva.getMockReserva();
        r.setIdCategoria(categoria.getIdCategoria());
        r.setIdUsuario(usuarioUm.getIdUsuario());
        r.setIdCarro(carro.getIdCarro());

    }

    @Before
    public void setUp() {

        categoriaDAO = new CategoriaDAOMariaDB();
        reservaDAO = new ReservaDAOMariaDB();
        usuarioDAO = new UsuarioDAOMariaDB();
        carroDAO = new CarroDAOMariaDB();
        fabricanteDAO = new FabricanteDAOMariaDB();

    }

    @After
    public void tearDown() {

        categoriaDAO = null;
        reservaDAO = null;
        usuarioDAO = null;
        carroDAO = null;
        fabricanteDAO = null;

    }

    @Test
    public void teste1EncontrarPorId() {
        
        Reserva reserva = reservaDAO.inserir(r);
        r.setIdReserva(reserva.getIdReserva());
        assertNotNull(reservaDAO.encontrarPorId(r.getIdReserva()));
    
    }

    @Test
    public void teste2EncontrarReserva() {
        assertNotNull(reservaDAO.encontrarPorIdUsuario(r.getIdUsuario()));
    }

    @Test
    public void teste3EncontrarTudo() {
        List<Reserva> reservas = reservaDAO.encontrarTudo();
        assertNotNull(reservas);
        assertTrue(reservas.size() >= 1);
    }

    @Test
    public void teste4AtualizarReserva() {
        usuarioDois = MockUsuario.getMockUsuario();
        usuarioDois.setApelido("usuarioTesteDois");

        Usuario usuario = usuarioDAO.inserir(usuarioDois);
        usuarioDois.setIdUsuario(usuario.getIdUsuario());

        Reserva reservaDois = MockReserva.getMockReserva();
        reservaDois.setIdUsuario(usuarioDois.getIdUsuario());
        reservaDois.setIdCarro(carro.getIdCarro());
        reservaDois.setIdCategoria(categoria.getIdCategoria());
        reservaNova = reservaDAO.atualizar(r, reservaDois);

        assertNotNull(reservaNova);
        assertNotEquals(r.getIdUsuario(), reservaDAO.encontrarPorId(reservaNova.getIdReserva()).getIdUsuario());
        usuarioDAO.deletar(usuarioUm.getIdUsuario());
    }

    @Test
    public void teste5RemocaoReserva() {

        reservaDAO.deletar(r.getIdReserva());
        
        assertNull(reservaDAO.encontrarPorId(r.getIdReserva()));
        
        carroDAO.deletar(carro.getIdCarro());
        
        categoriaDAO.deletar(categoria.getIdCategoria());
        usuarioDAO.deletar(usuarioDois.getIdUsuario());
        fabricanteDAO.deletar(fabricante.getIdFabricante());
        
    }

}
