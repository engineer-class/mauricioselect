package com.fatec.mauricioselect.web.servlet.utilitarios;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * Classe para testar os métodos do manipulador de datas
 */
public class TesteManipuladorDeData {   
    /**
     * Testa se a data do sistema está igual a do NTP
     */
    @Test
    public void testaDataDoSistema() {
        assertTrue(ManipuladorDeData.dataEstaIgualANTP());
    }
}
