package com.fatec.mauricioselect.web.servlet.utilitarios;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Teste do cliente NTP
 */
public class TesteClienteNTP {
    /**
     * Teste para verificar a conexão com o servidor NTP
     */
    @Test
    public void testeConexaoComServidorNTP() {
        assertNotNull(ClienteNTP.recuperaDataAtual());
    }
}
